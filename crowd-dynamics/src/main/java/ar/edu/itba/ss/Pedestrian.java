package ar.edu.itba.ss;

public class Pedestrian extends Particle {
	private static double L = Configuration.getInstance().getL();
	
	public Pedestrian(double radius) {
		super(radius);
	}
	
	@Override
	public void updatePosition(double x, double y) {
		super.updatePosition(x, periodicY(y));
	}
	
	private double periodicY(double y) {
		return (y + L) % L;
	}

	@Override
	public String getColor() {
		return super.getColor(1,0,1);
	}
}