package ar.edu.itba.ss;

import java.awt.geom.Point2D;
import java.util.List;
import java.util.PriorityQueue;

public abstract class Heuristic {
    protected abstract double getAngle(Particle movingPedestrian, List<Particle> particles, PriorityQueue<Collision> cQueue);


    public static Heuristic getHeuristic1() {
        return new Heuristic() {
            @Override
            protected double getAngle(Particle movingPedestrian, List<Particle> particles, PriorityQueue<Collision> cQueue) {

                // double value = Math.PI / 2.0;
                double value = Configuration.getInstance().getHeuristicParameter();
                return MyRandom.getInstance().nextDouble(value, -1 * value);
            }
        };
    }

    public static Heuristic getHeuristic2() {
        return new Heuristic() {
            @Override
            protected double getAngle(Particle movingPedestrian, List<Particle> particles, PriorityQueue<Collision> cQueue) {
            	Collision nextCollision = cQueue.peek();
            	double currentAngle = movingPedestrian.getAngle();
            	double sign = addSign(currentAngle,nextCollision.getParticle().getAngle());
            	double A = Configuration.getInstance().getHeuristicParameter();
                return currentAngle + sign*(A / Math.exp(nextCollision.getTime()));
            }
        };
    }
    
    public static Heuristic getHeuristic3() {
        return new Heuristic() {
            @Override
            protected double getAngle(Particle movingPedestrian, List<Particle> particles, PriorityQueue<Collision> cQueue) {
                Collision next = cQueue.poll();
                Point2D futurePositionP1 = movingPedestrian.predictFuturePosition(next.getTime());
                Particle toCrash = next.getParticle();
                Point2D futurePositionP2 = toCrash.predictFuturePosition(next.getTime());
                int velocitySign = toCrash.getVelY() > 0 ? 1 : (toCrash.getVelY() == 0? 0 : -1);
                double delta = Configuration.getInstance().getDt1();
                double heuristicParameter = Configuration.getInstance().getHeuristicParameter();


                // Calculo las dos posiciones en el mismo eje y tal que no overlapea
                double cte = Math.sqrt(Math.pow(movingPedestrian.getRadius() * heuristicParameter + toCrash.getRadius(), 2) - Math.pow(futurePositionP1.getX() - futurePositionP2.getX(),2));
                Point2D posiblePos1 = new Point2D.Double(futurePositionP1.getX(), futurePositionP2.getY() + cte);
                Point2D posiblePos2 = new Point2D.Double(futurePositionP1.getX(), futurePositionP2.getY() - cte);

                Point2D futurePositionP2dt = toCrash.predictFuturePosition(next.getTime() + delta);

                //Verifico si en un dt1 ambas posiciones siguen overlapeando
                double overlapPos1 = Particle.getOvelap(posiblePos1, movingPedestrian.getRadius(), futurePositionP2dt, toCrash.getRadius());
                Point2D toCompare;
                double overlapPos2 = Particle.getOvelap(posiblePos2, movingPedestrian.getRadius(), futurePositionP2dt, toCrash.getRadius());

                // Me quedo con la posicion que tenga menor overlap en un dt mas en la misma posicion
                if (velocitySign > 0) {
                    toCompare = posiblePos2;
                } else {
                    toCompare = posiblePos1;
                }
                /*
                if (overlapPos1 < overlapPos2){
                    toCompare = posiblePos1;
                } else if (overlapPos2 < overlapPos1){
                    toCompare = posiblePos2;
                } else {
                    toCompare = movingPedestrian.getPosition().distance(posiblePos1) <= movingPedestrian.getPosition().distance(posiblePos2)? posiblePos1 : posiblePos2;
                }

                 */


                double ans = movingPedestrian.angleBetween(toCompare);
                // si una de las dos no overlapea en dt1 + delta, me quedo con esa. Sino busco la que tenga menor tiempo de overlap.
                // si ninguna de las dos resulta valida, me quedo con la de distancia mas corta


                // Tengo la posicion futura
                return ans;
            }
        };
    }

    public static Heuristic getHeuristic4() {
        return new Heuristic() {
            @Override
            protected double getAngle(Particle movingPedestrian, List<Particle> particles, PriorityQueue<Collision> cQueue) {
                Collision next = cQueue.poll();
                Point2D futurePositionP1 = movingPedestrian.predictFuturePosition(next.getTime());
                Particle toCrash = next.getParticle();
                Point2D futurePositionP2 = toCrash.predictFuturePosition(next.getTime());
                int velocitySign = toCrash.getVelY() > 0 ? 1 : (toCrash.getVelY() == 0? 0 : -1);

                double parameter = Configuration.getInstance().getHeuristicParameter();

                // Me quedo con la posicion que tenga menor overlap en un dt mas en la misma posicion
                double L = Configuration.getInstance().getL();

                double toSum = movingPedestrian.getRadius() + toCrash.getRadius() + parameter;
                Point2D posibleP1 = new Point2D.Double(futurePositionP1.getX(), futurePositionP2.getY() - toSum);
                double posibleAngleP1 = movingPedestrian.angleBetween(posibleP1);

                Point2D posibleP2 = new Point2D.Double(futurePositionP1.getX(), futurePositionP2.getY() + toSum);
                double posibleAngleP2 = movingPedestrian.angleBetween(posibleP2);
                double ans;

                if (velocitySign > 0 || (velocitySign < 0 && movingPedestrian.getVelocityModule() * Math.sin(posibleAngleP1) < toCrash.getVelY())) {
                    ans = posibleAngleP1;
                } else {
                    ans = posibleAngleP2;
                }

                // Tengo la posicion futura
                return ans;
            }
        };
    }

    public static Heuristic getHeuristic5() {
        return new Heuristic() {
            @Override
            protected double getAngle(Particle movingPedestrian, List<Particle> particles, PriorityQueue<Collision> cQueue) {
                Collision nextCollision = cQueue.peek();
                double currentAngle = movingPedestrian.getAngle();
                double sign = addSign(currentAngle,nextCollision.getParticle().getAngle());
                double A = Configuration.getInstance().getHeuristicParameter();
                double time = nextCollision.getTime();
                double e = 0.2*time*time + 0.3*time;
                return currentAngle + sign*(A / Math.exp(e));
            }
        };
    }

    private static double addSign(double vel1, double vel2) {
    	if (sameDirection(vel1,vel2)) {
    		return -1 * sign(vel1);
    	}
    	return sign(vel1);
    }

    private static double sign(double value){
        return Math.signum(value)>=0?1.0:-1.0;
    }
    
    private static boolean sameDirection(double angle1, double angle2) {
    	return (angle1 > 0 && angle2 > 0) || (angle1 < 0 && angle2 < 0);
    }
}