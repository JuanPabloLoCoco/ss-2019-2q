package ar.edu.itba.ss;

import java.awt.geom.Point2D;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class ParticleFactory {
    private static ParticleFactory instance;

    public static ParticleFactory getInstance(){
        if (instance == null){
            instance = new ParticleFactory();
        }
        return instance;
    }

    private Particle generateMovingPedestrian(){
        double minRadius = Configuration.getInstance().getPedestrianMinRadius();
        Particle newParticle = new MovingPedestrian(minRadius);

        Point2D initialPosition = Configuration.getInstance().getInitialPosition();
        newParticle.setProperties(initialPosition.getX(), initialPosition.getY(), 0,0);

        newParticle.setId();
        double mass = Configuration.getInstance().getPedestrianMass();
        newParticle.setMass(mass);

        double velocityModule = Configuration.getInstance().getPedestrianVelocity();
        newParticle.setVelocityModule(velocityModule);
        return newParticle;
    }

    private List<Particle> generateObstacles(){
        List<Particle> particleList = new ArrayList<>();

        double L = Configuration.getInstance().getL();
        double W = Configuration.getInstance().getW();

        int obstacleCount = Configuration.getInstance().getObstacleCount();
        double obstacleRadius = Configuration.getInstance().getObstacleRadius();
        double obstacleVelocity = Configuration.getInstance().getObstacleVelocity();

        double minWidth = Configuration.getInstance().getPedestrianMinRadius() * 2;
        double maxWidth = W - minWidth;
        double steps = (maxWidth - minWidth) / obstacleCount;

        for (int i = 0; i < obstacleCount; i++) {
            Particle particle = new Pedestrian(obstacleRadius);
            double y = MyRandom.getInstance().nextDouble(0, L);
            double x = steps * i + minWidth;
            particle.setProperties(x, y, 0, 0);

            double random = MyRandom.getInstance().nextDouble(0,1);
            double multiply = 1.0;
            if (random <= 0.5) {
                multiply = -1.0;
            }

            double minVelocity = Configuration.getInstance().getObstacleMinVelocity();
            double maxVelocity = Configuration.getInstance().getObstacleMaxVelocity();
            particle.setVelocityModule(MyRandom.getInstance().nextDouble(minVelocity, maxVelocity));
            particle.setAngle(multiply * (Math.PI / 2.0));
            particle.setId();
            particleList.add(particle);
        }

        return particleList;
    }

    public List<Particle> generateParticles (){
        List<Particle> particleList = new ArrayList<>();
        particleList.add(generateMovingPedestrian());
        particleList.addAll(generateObstacles());
        return particleList;
    }

    /*
    private Particle tryParticle (List<Particle> particleList){
        Particle particle;
        int n = 0;
        int particleTries = Configuration.getInstance().getParticleTries();
        double length = Configuration.getInstance().getL();
        double width = Configuration.getInstance().getW();

        do {

            double minRadius = Configuration.getInstance().getMinRadius();
            double maxRadius = Configuration.getInstance().getMaxRadius();
            double radius = MyRandom.getInstance().nextDouble(minRadius, maxRadius);
            particle = Particle.generateRandom(width, length, radius);
            n++;
        } while (!Particle.isValidParticle(particle, particleList) && n <= particleTries);
        if (n <= particleTries){
            return particle;
        }
        return null;
    }
    */

    /*
	private List<Particle> generateWallParticles () {
        List<Particle> particleList = new ArrayList<>();
        double heigth = Configuration.getInstance().getL();
        double hole = Configuration.getInstance().getD();
        double width = Configuration.getInstance().getW();
        double wallRadio =  Configuration.getInstance().getMinRadius()/2;
        double radius = Configuration.getInstance().getMinRadius();

        int count = (int)((width - hole)/radius);

        for (int i = 0; i <= count; i++){
            Particle particle = new Particle(wallRadio);
            particle.setProperties(i * wallRadio,0 , 0,0 );
            particle.setId();
            particleList.add(particle);
        }
        for (int j = 0; j <= count; j++){
            Particle particle = new Particle(wallRadio);
            particle.setProperties(j * wallRadio + (hole + width)/2, 0, 0,0 );
            particle.setId();
            particleList.add(particle);
        }

        int heigthCount = (int)(heigth/radius) * 2;
        for (int i = 0; i < heigthCount; i++) {
            Particle p1 = new Particle(wallRadio);
            Particle p2 = new Particle(wallRadio);
            p1.setProperties(0, i * wallRadio, 0, 0);
            p2.setProperties(width, i * wallRadio, 0, 0);
            p1.setId();
            p2.setId();
            particleList.add(p1);
            particleList.add(p2);
        }

        /** Horizontal Border Particles
        int widthCount = (int)(width/radius) * 2;
        for (int i = 0; i < widthCount; i++) {
            Particle p1 = new Particle(wallRadio);
            p1.setProperties(i * wallRadio, heigth, 0, 0);
            p1.setId();
            particleList.add(p1);
        }

        return particleList;
    }
    */

    private String generateParticlesWallData(List<Particle> particleList){
        StringBuilder builder = new StringBuilder();
        for(Particle current: particleList){
            builder.append(current.getId())
                    .append(" ")
                    .append(current.getX())
                    .append(" ")
                    .append(current.getY())
                    .append(" ")
                    .append(current.getRadius())
                    .append("\r\n");
        }
        return builder.toString();
    }

    public void generateParticleFile (List<Particle> particleList, String fileName){
        String dataToPrint = generateParticlesWallData(particleList);
        StringBuilder builder = new StringBuilder()
                .append(Configuration.outputDir)
                .append(fileName)
                .append(".txt");
        String filePath = builder.toString();
        Path path = Paths.get(filePath);

        StringBuilder timeBuilder = new StringBuilder()
                .append(particleList.size())
                .append("\r\n\r\n")
                .append(dataToPrint);
        try {
            if (!Files.exists(path)){
                Files.write(path, timeBuilder.toString().getBytes(), StandardOpenOption.CREATE);
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public void generateLJWall() {
        List<Particle> particleList = generateParticles();
        //List<Particle> particleList = generateWallParticles();

        String dataToPrint = generateParticlesWallData(particleList);
        StringBuilder builder = new StringBuilder()
                .append(Configuration.outputDir)
                .append("Wall_")
                //.append(Configuration.getInstance().getD())
                .append(".txt");
        String filePath =  builder.toString();
        Path path = Paths.get(filePath);

        StringBuilder timeBuilder = new StringBuilder()
                .append(particleList.size())
                .append("\r\n\r\n")
                .append(dataToPrint);
        try {
            if (!Files.exists(path)){
                Files.write(path, timeBuilder.toString().getBytes(), StandardOpenOption.CREATE);
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }

}
