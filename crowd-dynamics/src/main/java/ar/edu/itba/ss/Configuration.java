package ar.edu.itba.ss;

import javax.swing.*;
import java.awt.geom.Point2D;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

class Configuration {
	private final static String projectDir = System.getProperty("user.dir");
	final static String staticInfoDir = projectDir + "/resources/staticInfo.txt";
	final static String dynamicInfoDir = projectDir + "/resources/dynamicInfo.txt";
	final static String outputDir = projectDir + "/results/";
	final static String analyticsFileName = "analyticsFile.csv";

	private Properties properties;
	private InputStream inStream = null;

	private static Configuration instance;

	/**
	 * Seed
	 */
	private long seed;

	private double dt1;

	private double dt2;

	private double W;

	private double L;

	private Point2D goal;

	private Point2D initialPosition;

	private double pedestrianMass;

	private double pedestrianMinRadius;

	private double pedestrianVelocity;

	private double obstacleRadius;

	private double obstacleVelocity;

	private double obstacleMinVelocity;

	private double obstacleMaxVelocity;

	private int obstacleCount;

	private int heuristic;

	private int pedestrianTries;

	private double heuristicParameter;

	private Configuration(){
		properties = new Properties();
		try {
			String configInfoDir = projectDir + "/resources/config.properties";
			inStream = new FileInputStream(configInfoDir);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		loadConfiguration();
	}

	static Configuration getInstance(){
		if (instance == null){
			instance = new Configuration();
		}
		return instance;
	}

	private void loadConfiguration() {

		try {
			properties.load(inStream);
		} catch (IOException e){
			e.printStackTrace();
		}

		seed = Long.parseLong(properties.getProperty("seed"));

		dt2 = Double.parseDouble(properties.getProperty("dt2"));
		dt1 = Double.parseDouble(properties.getProperty("dt1"));
		W = Double.parseDouble(properties.getProperty("W"));
		L = Double.parseDouble(properties.getProperty("L"));

		double goalX = Double.parseDouble(properties.getProperty("goal.x"));
		double goalY = Double.parseDouble(properties.getProperty("goal.y"));
		goal = new Point2D.Double(goalX, goalY);

		double pedestrianX = Double.parseDouble(properties.getProperty("pedestrian.x"));
		double pedestrianY = Double.parseDouble(properties.getProperty("pedestrian.y"));
		initialPosition = new Point2D.Double(pedestrianX, pedestrianY);

		pedestrianMass = Double.parseDouble(properties.getProperty("pedestrian.mass"));
		pedestrianMinRadius = Double.parseDouble(properties.getProperty("pedestrian.minRadius"));
		pedestrianVelocity = Double.parseDouble(properties.getProperty("pedestrian.velocity"));
		pedestrianTries = Integer.parseInt(properties.getProperty("pedestrian.tries"));

		obstacleRadius = Double.parseDouble(properties.getProperty("obstacle.radius"));
		obstacleVelocity = Double.parseDouble(properties.getProperty("obstacle.velocity"));
		obstacleCount = Integer.parseInt(properties.getProperty("obstacle.count"));
		obstacleMinVelocity = Double.parseDouble(properties.getProperty("obstacle.minVelocity"));
		obstacleMaxVelocity = Double.parseDouble(properties.getProperty("obstacle.maxVelocity"));

		heuristic = Integer.parseInt(properties.getProperty("heuristic"));
		heuristicParameter = Double.parseDouble(properties.getProperty("heuristic.parameter"));
	}

	public long getSeed() {
		return seed;
	}

	public double getDt1() {
		return dt1;
	}

	public double getDt2() {
		return dt2;
	}


	public double getW() {
		return W;
	}

	public double getL() {
		return L;
	}

	public Point2D getGoal() {
		return goal;
	}

	public Point2D getInitialPosition() {
		return initialPosition;
	}

	public double getPedestrianMass() {
		return pedestrianMass;
	}

	public double getPedestrianMinRadius() {
		return pedestrianMinRadius;
	}

	public double getPedestrianVelocity() {
		return pedestrianVelocity;
	}

	public int getPedestrianTries() {
		return pedestrianTries;
	}

	public double getObstacleRadius() {
		return obstacleRadius;
	}

	public double getObstacleVelocity() {
		return obstacleVelocity;
	}

	public int getObstacleCount() {
		return obstacleCount;
	}

	public int getHeuristic() {
		return heuristic;
	}

	public double getObstacleMinVelocity() {
		return obstacleMinVelocity;
	}

	public double getObstacleMaxVelocity() {
		return obstacleMaxVelocity;
	}

	public double getHeuristicParameter() {
		return heuristicParameter;
	}


	@Override
	public String toString() {
		return "Configuration{" +
				"seed=" + seed +
				", dt1=" + dt1 +
				", dt2=" + dt2 +
				", W=" + W +
				", L=" + L +
				", goal=" + goal +
				", initialPosition=" + initialPosition +
				", pedestrianMass=" + pedestrianMass +
				", pedestrianMinRadius=" + pedestrianMinRadius +
				", pedestrianVelocity=" + pedestrianVelocity +
				", obstacleRadius=" + obstacleRadius +
				", obstacleVelocity=" + obstacleVelocity +
				", obstacleCount=" + obstacleCount +
				", heuristic=" + heuristic +
				'}';
	}
}