package ar.edu.itba.ss;

public class Collision implements Comparable<Collision> {
	/** Collision time */
	private double time;
	private Particle particle2;
	
	/** Pedestrian against particle */
	public Collision(double time, Particle particle2) {
		this.time = time;
		this.particle2 = particle2;
	}
	
	public double getTime() {
		return time;
	}

	public Particle getParticle() {
		return particle2;
	}

	@Override
	public int compareTo(Collision o) {
		return Double.compare(getTime(),o.getTime());
	}
	
	public boolean isActive() {
		return time < 0;
	}

	@Override
	public String toString() {
		return "Collision{" +
				"t = " + time + ", Particle = " +
				particle2.toString() +
				'}';
	}
}