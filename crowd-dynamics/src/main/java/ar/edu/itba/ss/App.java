package ar.edu.itba.ss;

import java.io.IOException;
import java.util.List;

public class App {
    public static void main( String[] args ) throws IOException {
        System.out.println("It´s running crowd dynamics");
        System.out.println(Configuration.getInstance().toString());

        Long seed = Configuration.getInstance().getSeed();
        MyRandom.setSeed(seed);

        List<Particle> particleList = ParticleFactory.getInstance().generateParticles();
        Algorithm algorithm = new Algorithm(particleList);

        algorithm.run();
    }
}