package ar.edu.itba.ss;

import java.awt.geom.Point2D;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.PriorityQueue;

public class Algorithm {
	public final static Heuristic HEURISTIC_1 = Heuristic.getHeuristic1();
	private Heuristic heuristicSelected;
	private final static Point2D GOAL = Configuration.getInstance().getGoal();
	/**
	 * For no collision between particles, a big enough time is used meaning it will not affect the rest of the logic.
	 */
	private final static double NO_COLLISION = Double.MAX_VALUE;
	private double totalTime = 0;
	private int totalTry = 0;
	// private Particle movingPedestrian;
	private List<Particle> particles;
	private Particle target;
	private double deltaT;
	private double dt2;
	private double totalOverlapping = 0;

	private PriorityQueue<Collision> cQueue = new PriorityQueue<Collision>();

	public Algorithm(List<Particle> particles) {
		this.particles = particles;
		target = new Particle(Configuration.getInstance().getPedestrianMinRadius());
		target.setProperties(GOAL.getX(), GOAL.getY(), 0, 0);

		int heuristicType = Configuration.getInstance().getHeuristic();

		if (heuristicType == 1) {
			heuristicSelected = Heuristic.getHeuristic1();
		} else if (heuristicType == 2){
			heuristicSelected = Heuristic.getHeuristic2();
		} else if (heuristicType == 3){
			heuristicSelected = Heuristic.getHeuristic3();
		} else if (heuristicType == 4) {
			heuristicSelected = Heuristic.getHeuristic4();
		} else if (heuristicType == 5) {
			heuristicSelected = Heuristic.getHeuristic5();
		}
	}

	public List<Particle> getObstacles() {
		return particles.subList(1, particles.size());
	}

	public void setParticles(List<Particle> particles) {
		this.particles = particles;
	}

	private void dt2Routine() {
		String particleSimulation = generateParticlesSimulationData(particles);
		generateParticleSimulationOutput(particleSimulation, totalTime);
	}

	private void printToFile() {
		String particlesData = generateParticleData(particles.get(0), totalTime);
		generateParticlesOutput(particlesData);
		String particleSimulation = generateParticlesSimulationData(particles);
		generateParticleSimulationOutput(particleSimulation, totalTime);
		String particleSimulationVelocity = generateParticlesSimulationDataWithVelocity(particles);
		generateParticleSimulationOutputWithVelocity(particleSimulationVelocity, totalTime);
	}

    private void dt1Routine(){
		MovingPedestrian pedestrian = (MovingPedestrian) particles.get(0);
		totalOverlapping = totalOverlapping + pedestrian.calculateOverlapping(particles) * deltaT;
		pedestrian.updateToGoal();

		generateCollisionTimeQueue();

		while (!cQueue.isEmpty() && cQueue.peek().isActive()){
    		cQueue.poll();
    	}
		if (!cQueue.isEmpty()){
			pedestrian.updateAngle(getObstacles() ,cQueue, heuristicSelected);
		}
    	
        for (Particle p1 : particles){
        	p1.advance(deltaT);
        }
        printToFile();
	}

    private boolean notInTarget (){
		Particle particle = particles.get(0);
		double distance = Particle.getDistance(GOAL.getX(), GOAL.getY(), particle.getX(), particle.getY());
		return (distance > particle.getRadius()/10.0);

	}

    public void run() {
        double dt1 = Configuration.getInstance().getDt1();
		deltaT = dt1;
		dt2 = Configuration.getInstance().getDt2();

		int tries = Configuration.getInstance().getPedestrianTries();

		for (; totalTry < tries; totalTry++){

			totalOverlapping = 0;
			totalTime = 0;
			Particle moving = particles.get(0);
			Point2D initialPosition = Configuration.getInstance().getInitialPosition();
			moving.setProperties(initialPosition.getX(), initialPosition.getY(), 0, 0);
			printToFile();

			while(notInTarget()){
				totalTime += dt1;
				System.out.println("Total Try = " + totalTry + "- X= " + particles.get(0).getX() + " - Y= " + particles.get(0).getY() + " - Overlap = " + totalOverlapping);
				dt1Routine();
			}
		}


	}

	/** Prints to File */
    private void generateParticleSimulationOutput(String particleData, double time) {
        String filePath = Configuration.outputDir +
                "Simu_N" +
				(particles.size() + 1)+
                "_Seed" +
                Configuration.getInstance().getSeed() +
				"_Heu" +
				Configuration.getInstance().getHeuristic() +
				"_par" +
				Configuration.getInstance().getHeuristicParameter() +
				".txt";
		Path path = Paths.get(filePath);

        String timeBuilder = (particles.size() + 1) +
                "\r\n//" +
                time +
                "_time,id,x,y,radio,r,g,b" +
                "\r\n";
        StringBuilder toPrint = new StringBuilder()
				.append(timeBuilder)
				.append(particleData);

		try {
			if (!Files.exists(path)){
				Files.write(path, toPrint.toString().getBytes(), StandardOpenOption.CREATE);
			} else {
				Files.write(path, toPrint.toString().getBytes(), StandardOpenOption.APPEND);
			}
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	private void generateParticleSimulationOutputWithVelocity(String particleData, double time) {
		String filePath = Configuration.outputDir +
				"SimuVel_N" +
				(particles.size() + 1)+
				"_Seed" +
				Configuration.getInstance().getSeed() +
				"_Heu" +
				Configuration.getInstance().getHeuristic() +
				"_par" +
				Configuration.getInstance().getHeuristicParameter() +
				".txt";
		Path path = Paths.get(filePath);

		String timeBuilder = (particles.size() + 1) +
				"\r\n//" +
				time +
				"_time,id,x,y,radio,r,g,b" +
				"\r\n";
		StringBuilder toPrint = new StringBuilder()
				.append(timeBuilder)
				.append(particleData);

		try {
			if (!Files.exists(path)){
				Files.write(path, toPrint.toString().getBytes(), StandardOpenOption.CREATE);
			} else {
				Files.write(path, toPrint.toString().getBytes(), StandardOpenOption.APPEND);
			}
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	private void generateParticlesOutput(String particleData) {
	String filePath = Configuration.outputDir +
                "Data_N" +
                particles.size() +
                "_Seed" +
                Configuration.getInstance().getSeed() +
				"_Heu" +
				Configuration.getInstance().getHeuristic() +
				"_tries" +
				Configuration.getInstance().getPedestrianTries() +
				"_par" +
				Configuration.getInstance().getHeuristicParameter() +
				".csv";
		Path path = Paths.get(filePath);

		try {
			if (!Files.exists(path)){
				Files.write(path, getParticleHeader().getBytes(), StandardOpenOption.CREATE);
			}
			Files.write(path, particleData.getBytes(), StandardOpenOption.APPEND);
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	/** Headers */
	private String getParticleHeader () {
		return "try,time,x,y,overlapping\r\n";
	}
	
	/** Particles Simulation Data */
	private String generateParticlesSimulationData(List<Particle> particleList){
		StringBuilder builder = new StringBuilder();
		int lastId = 0;
		for (Particle current: particleList){
			builder.append(current.getId())
					.append(" ")
					.append(current.getX())
					.append(" ")
					.append(current.getY())
					.append(" ")
					.append(current.getRadius())
					.append(" ");

			if (current.getId() == 1) {
				builder.append(((MovingPedestrian)current).getColor());
			} else {
				builder.append(((Pedestrian)current).getColor());
			}
					builder.append("\r\n");
			lastId = current.getId();
		}


		builder.append(lastId + 1)
				.append(" ")
				.append(target.getX())
				.append(" ")
				.append(target.getY())
				.append(" ")
				.append(target.getRadius())
				.append(" ")
				.append(target.getColor(0,1,0))
				.append(" ");
		builder.append("\r\n");
		return builder.toString();
	}

	private String generateParticlesSimulationDataWithVelocity(List<Particle> particleList){
		StringBuilder builder = new StringBuilder();
		int lastId = 0;
		for (Particle current: particleList){
			builder.append(current.getId())
					.append(" ")
					.append(current.getX())
					.append(" ")
					.append(current.getY())
					.append(" ")
					.append(current.getVelX())
					.append(" ")
					.append(current.getVelY())
					.append(" ")
					.append(current.getRadius())
					.append(" ");

			if (current.getId() == 1) {
				builder.append(((MovingPedestrian)current).getColor());
			} else {
				builder.append(((Pedestrian)current).getColor());
			}
			builder.append("\r\n");
			lastId = current.getId();
		}


		builder.append(lastId + 1)
				.append(" ")
				.append(target.getX())
				.append(" ")
				.append(target.getY())
				.append(" ")
				.append(target.getVelX())
				.append(" ")
				.append(target.getVelY())
				.append(" ")
				.append(target.getRadius())
				.append(" ")
				.append(target.getColor(0,1,0))
				.append(" ");
		builder.append("\r\n");
		return builder.toString();
	}
	/**  Particles Data */
	private String generateParticleData(Particle particle, double time){
		StringBuilder builder = new StringBuilder();
		builder.append(totalTry)
				.append(",")
				.append(time)
				.append(",")
				.append(particle.getX())
				.append(",")
				.append(particle.getY())
				.append(",")
				.append(totalOverlapping)
				.append("\r\n");
		return builder.toString();
	}

	public void generateCollisionTimeQueue() {
		cQueue.clear();
		double L = Configuration.getInstance().getL();
		for (Particle other : getObstacles()) {
			generateCollisionTimeQueue(other, other.getY());
			generateCollisionTimeQueue(other, other.getY() - L); // Top periodic condition
			generateCollisionTimeQueue(other, other.getY() + L); // Bottom periodic condition
		}
	}

	private void generateCollisionTimeQueue(Particle particle2, double particle2Ypos) {
		double currentTime = tCollisionParticles(particle2, particle2Ypos);
		
		if (currentTime != NO_COLLISION) {
			cQueue.add(new Collision(currentTime,particle2));
		}
	}
	
	public double tCollisionParticles(Particle particle2, double particle2Ypos) {
		Particle movingPedestrian = particles.get(0);

		double deltaRx  = particle2.getX()    - movingPedestrian.getX();
		double deltaRy  = particle2Ypos       - movingPedestrian.getY();
		double deltaVx  = particle2.getVelX() - movingPedestrian.getVelX();
		double deltaVy  = particle2.getVelY() - movingPedestrian.getVelY();
		/* dv.dr = (dvx)(dx) + (dvy)(dy) */
		double deltaVmR = deltaVx*deltaRx     + deltaVy*deltaRy;

		if ( deltaVmR >= 0 ) {
			return NO_COLLISION;
		}

		/* sigma = ri + rj */
		double sigma  = movingPedestrian.getRadius() + particle2.getRadius();
		double deltaRmR = deltaRx*deltaRx     + deltaRy*deltaRy;
		double deltaVmV = deltaVx*deltaVx     + deltaVy*deltaVy;
		/* d =( dv.dr)^2 - (dv.dv)(dr.dr-sigma^2) */
		double d = deltaVmR*deltaVmR - deltaVmV*(deltaRmR - sigma*sigma);
		
		if (d < 0) {
			return NO_COLLISION;
		}

		/* tc = - dv . dr + sqrt(d) / dv . dv */
		double t = - ((deltaVmR + Math.sqrt(d)) / deltaVmV);
		return t;
	}
}