package ar.edu.itba.ss;

import java.awt.geom.Point2D;
import java.util.List;

public class Particle {
	private double velocityModule = 0;
	private double velocityAngle = 0;

	private static int NEW_ID = 1;
	private static int RADIUS_ID = 0;
	private static int COLOR_ID = 1;
	private static int X_ID = 0, Y_ID = 1, VEL_X_ID = 2, VEL_Y_ID = 3;
	private static String DEFAULT_COLOR = "#FFF";
	private int id = 0;
	private Point2D position = new Point2D.Double();
	private Point2D velocity = new Point2D.Double();
	private double radius;
	private String color;
	private double mass = 0;


	public Particle(String[] details) {
		this(Double.parseDouble(details[RADIUS_ID]), details[COLOR_ID]);
	}

	public Particle(double radius) {
		this(radius, DEFAULT_COLOR);
	}

	public Particle(double radius, String color) {
		this.radius = radius;
		this.color = color;
		// this.velocityAngle = MyRandom.getInstance().nextDouble(0, Math.PI * 2);
	}
	
	void setId() {
		if (id == 0) {
			this.id = NEW_ID;
			NEW_ID++;
		}
	}

	/**
	 * public static void setPeriodic(boolean periodic){
	 * isPeriodic = periodic;
	 * }
	 */

	int getId() {
		return id;
	}

	double getRadius() {
		return radius;
	}

	public Point2D getPosition() {
		return position;
	}

	double getY() {
		return position.getY();
	}

	double getX() {
		return position.getX();
	}

	public double getAngle() {
		return velocityAngle;
	}

	void setAngle(double velocityAngle) {
		this.velocityAngle = velocityAngle;
	}

	/**
	 * @param cl cell length
	 * @return y normalized index
	 */
	int getYIndex(double cl) {
		return (int) (getY() / cl) % (int) cl;
	}

	/**
	 * @param cl cell length
	 * @return x normalized index
	 */
	int getXIndex(double cl) {
		return (int) (getX() / cl) % (int) cl;
	}

	public Particle setProperties(String[] properties) {
		return setProperties(Double.parseDouble(properties[X_ID]), Double.parseDouble(properties[Y_ID]),
				Double.parseDouble(properties[VEL_X_ID]), Double.parseDouble(properties[VEL_Y_ID]));
	}

	Particle setProperties(double x, double y, double velX, double velY) {
		updatePosition(x,y);
		velocity.setLocation(velX, velY);
		
		return this;
	}

	public void updatePosition(double x, double y){
		position.setLocation(x,y);
	}

	public static double getDistance(double x1, double y1, double x2, double y2){
		double x = x1 - x2;
		double y = y1 - y2;
		return Math.sqrt(x * x + y * y);
	}

	public double getDistance(Particle other) {
		/** Distance between centers minus the radius of the particles */
		return position.distance(other.position);
	}

	public void advance(double time) {
		/** %l accounts for periodic condition */
		updatePosition(position.getX() + getVelX()*time,position.getY() + getVelY()*time);
	}

	@Override
	public String toString() {
		return "Id: " + id + " (Radius: " + radius + ", Position x: " + position.getX() + ", y: " + position.getY() + ", Velocity x: " + velocity.getX() + ", Velocity y: " + velocity.getY() + ")";
	}
	
	public String getStaticRepresentation() {
		return radius + " " + color;
	}

	public String getDynamicRepresentation() {
		return position.getX() + " " + position.getY() + " " + getVelX() + " " + getVelY();
	}
	
	public double getVelX() {
		// return velocity.getX();
		return velocityModule * Math.cos(velocityAngle);
	}
	
	public double getVelY() {
		// return velocity.getY();
		return velocityModule * Math.sin(velocityAngle);
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public double getMass() {
		return mass;
	}

	public void setMass(double mass) {
		this.mass = mass;
	}

	public static boolean isValidParticle(Particle newParticle, List<Particle> currentParticles) {
		for (Particle currentParticle : currentParticles) {

			if (currentParticle.overlaps(newParticle) && !newParticle.equals(currentParticle)) {
				return false;
			}
		}
		return true;
	}
	
	public static Particle generateRandom(double width, double length, double radius) {
		double xPos, yPos;

		xPos = MyRandom.getInstance().nextDouble(radius, width - radius);
		yPos = MyRandom.getInstance().nextDouble(radius, length - radius);
		// xPos = Math.round(xPos*100)/(double)100;
		// yPos = Math.round(yPos*100)/(double)100;

		Particle particle = new Particle(radius);
		particle.setProperties(xPos, yPos, 0, 0);

		return particle;
	}

	public String getColor(){
		return getColor(0,0,0);
	}

	public String getColor(double red, double blue, double green){
		StringBuilder builder = new StringBuilder()
				.append(red)
				.append(" ")
				.append(green)
				.append(" ")
				.append(blue);
		return builder.toString();
	}
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Particle particle = (Particle) o;
		return id == particle.id;
	}

	@Override
	public int hashCode() {
		return id;
	}

	public double getVelocityModule(){
		final double velocityModule;
		velocityModule = Math.sqrt(velocity.getX() * velocity.getX() + velocity.getY() * velocity.getY());
		return velocityModule;
	}

	public Point2D.Double getVelocity() {
		return new Point2D.Double(getVelX(),getVelY());
	}

	public double getVersorX(Particle other) {
		return (other.getX() - this.getX()) / getDistance(other);
	}

	public double getVersorY(Particle other) {
		return (other.getY() - this.getY()) / getDistance(other);
	}

	public Point2D.Double getVersorN(Particle other) {
		return new Point2D.Double(getVersorX(other),getVersorY(other));
	}

	public Point2D.Double getVersorT(Particle other) {
		return new Point2D.Double(-getVersorY(other),getVersorX(other));
	}

	public static double getKineticEnergy(List <Particle> list){
		double kineticEnergy = 0;
		for (Particle particle : list){
			kineticEnergy = kineticEnergy +  particle.getMass() * particle.getVelocityModule() * particle.getVelocityModule();
		}
		return (kineticEnergy/2)/list.size();
	}

	public double getOverlap(Particle other) {
		double overlap = this.radius + other.radius - getDistance(other);
		if (overlap <= 0) return 0;
		return overlap;
	}

	public Point2D.Double getRelativeVelocity(Particle other) {
		return new Point2D.Double(this.getVelX() - other.getVelX(), this.getVelY() - other.getVelY());
	}

	public static double getOvelap (Point2D p1, double r1, Point2D p2, double r2) {
		double distance  = r1 + r2 - getDistance(p1.getX(), p1.getY(), p2.getX(), p2.getY());
		return distance < 0 ? 0 : distance;
	}
	public static boolean overlaps(Point2D p1, double r1, Point2D p2, double r2){
		return getOvelap(p1, r1, p2, r2) > 0 ? true : false;
	}

	public boolean overlaps(Particle other) {
		return getOverlap(other) > 0;
	}

	public void setId(int id){
		this.id = id;
	} 

	public void setVelocityModule(double velocityModule) {
		this.velocityModule = velocityModule;
	}

	public double getVelocityAngle() {
		return velocityAngle;
	}

	public void setVelocityAngle(double velocityAngle) {
		this.velocityAngle = velocityAngle;
	}
	
	public double angleBetween(Particle other) {
		return angleBetween(other.getPosition());
	}

	public static double angleBetween(Point2D p1, Point2D p2) {
		return Math.atan2(p2.getY() - p1.getY(), p2.getX() - p1.getX());
	}

	public double angleBetween(Point2D other) {
		return Math.atan2(other.getY() - getPosition().getY(), other.getX() - getPosition().getX());
	}

	public Point2D predictFuturePosition (double time){
		Point2D futurePosition = new Point2D.Double(getX() + time * getVelX(), getY() + time * getVelY());
		return futurePosition;
	}

	public double calculateOverlapping(List<Particle> particleList){
		double ans = 0;
		for (Particle particle: particleList){
			if (particle.getId() != id){
				ans = ans + getOverlap(particle);
			}
		}
		return ans;
	}
}