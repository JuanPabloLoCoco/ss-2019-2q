package ar.edu.itba.ss;

import java.awt.geom.Point2D;
import java.util.List;
import java.util.PriorityQueue;

public class MovingPedestrian extends Pedestrian {
	private static Point2D GOAL = Configuration.getInstance().getGoal();

	public MovingPedestrian(double radius) {
		super(radius);
	}

	public void updateToGoal() {
		setAngle(angleBetween(GOAL));
	}
	
	public void updateAngleCollision(Particle other) {
		setAngle(reverseAngle(angleBetween(other)));
	}
	
	/** Reverse angle in -pi : pi range. */
	public double reverseAngle(double angle) {
		if (angle > 0) return angle - Math.PI;
		return angle + Math.PI;
	}
	
	public void updateAngle(List<Particle> particles, PriorityQueue<Collision> cQueue, Heuristic heuristic) {
		setAngle(heuristic.getAngle(this,particles,cQueue));
	}

	@Override
	public String getColor() {
		return super.getColor(1,1,1);
	}
}