function [tryData, dataRows, dataCols] = getTry(fileName, trie)
  # fileName = '.\results\Data_N11_Seed1000_Heu4_tries2_par1.0.csv';
  A = importdata(fileName,',',1);
  datos = A.data;
  dataRows = size(datos)(1);
  dataCols = size(datos)(2);
  timeStep = datos(2,2);
  index = 1;
  
  for i = 1: dataRows
    if (datos(i,1) == trie)
      for j = 1:dataCols
        tryData(index,j) = datos(i,j);
      endfor
      if (index == 1) 
        tryData(index, dataCols + 1) = 0;
        tryData(index, dataCols + 2) = 0;
        tryData(index, dataCols + 3) = 0;
      else 
        tryData(index, dataCols + 1) = sqrt((datos(i,3)- datos(i-1,3))^2 + (datos(i,4) - datos(i - 1,4))^2)/timeStep;
        tryData(index, dataCols + 2) = (datos(i,3) - datos(i - 1, 3))/timeStep;
        tryData(index, dataCols + 3) = (datos(i,4) - datos(i - 1, 4))/timeStep;
      endif
      index = index + 1;  
    endif
  endfor
  dataRows = index - 1;
end