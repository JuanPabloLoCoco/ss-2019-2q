clear;

% fileName = '.\results\Data_N11_Seed1000_Heu4_tries2_par1.0.csv';
% fileName = '.\results\Data_N11_Seed1000_Heu4_tries2_par1.0.csv';

# Condicion 1.


heu1Data = heuristic1();

heu2Data = heuristic2();

heu3Data = heuristic5();

heu4Data = heuristic4();

#heu5Data = heuristic5();


minOverlap(1,1) = min(heu1Data(:,10));
minOverlap(2,1) = min(heu2Data(:,10));
minOverlap(3,1) = min(heu3Data(:,10));
minOverlap(4,1) = min(heu4Data(:,10));

for ( i = 1 : size(heu1Data)(1,1))
  if (heu1Data(i,10) == minOverlap(1,1))
    minOverlap(1,2) = i;
    minOverlap(1,3) = heu1Data(i,10);
    minOverlap(1,4) = heu1Data(i,2);
   endif 
endfor

for ( i = 1 : size(heu2Data)(1,1))
  if (heu2Data(i,10) == minOverlap(2,1))
    minOverlap(2,2) = i;
    minOverlap(2,3) = heu2Data(i,10)
    minOverlap(2,4) = heu2Data(i,2);
   endif 
endfor

for ( i = 1 : size(heu3Data)(1,1))
  if (heu3Data(i,10) == minOverlap(3,1))
    minOverlap(3,2) = i;
    minOverlap(3,3) = heu3Data(i,10);
    minOverlap(3,4) = heu3Data(i,2);
   endif 
endfor

for ( i = 1 : size(heu4Data)(1,1))
  if (heu4Data(i,10) == minOverlap(4,1))
    minOverlap(4,2) = i;
    minOverlap(4,3) = heu4Data(i,10);
    minOverlap(4,4) = heu4Data(i,2);
   endif 
endfor



figure(1)
plot(minOverlap(1,4),minOverlap(1,3),'o','Color','b','LineWidth',2);
hold on

#p1 = errorbar(heu1Data(:,8), heu1Data(:,2),heu1Data(:,9));
#set(p1, "linestyle", "none");

plot(minOverlap(2,4),minOverlap(2,3),'o','Color','k','LineWidth',2);
#p2 = errorbar(heu2Data(:,1), heu2Data(:,8),heu2Data(:,9));
#set(p2, "linestyle", "none");

plot(minOverlap(3,4),minOverlap(3,3),'o','Color','r','LineWidth',2);
#p3 = errorbar(heu4Data(:,1), heu4Data(:,8),heu4Data(:,9));
#set(p3, "linestyle", "none");

plot(minOverlap(4,4),minOverlap(4,3),'o','Color','c','LineWidth',2);
#p4 = errorbar(heu5Data(:,1), heu5Data(:,9),heu5Data(:,9));
#set(p4, "linestyle", "none");

hold off
#}
#{
figure(2)
plot(heu4Data(:,1),heu4Data(:,4),'o','Color','b','LineWidth',2);
hold on
p1 = errorbar(heu4Data(:,1), heu4Data(:,4),heu4Data(:,5));
set(p1, "linestyle", "none");
hold off

figure(3)
plot(heu4Data(:,1),heu4Data(:,6),'o','Color','b','LineWidth',2);
hold on
p1 = errorbar(heu4Data(:,1), heu4Data(:,6),heu4Data(:,7));
set(p1, "linestyle", "none");
hold off

figure(4)
plot(heu4Data(:,1),heu4Data(:,8),'o','Color','b','LineWidth',2);
hold on
p1 = errorbar(heu4Data(:,1), heu4Data(:,8),heu4Data(:,9));
set(p1, "linestyle", "none");
hold off


figure(5)
plot(heu4Data(:,1),heu4Data(:,10),'o','Color','b','LineWidth',2);
hold on
p1 = errorbar(heu4Data(:,1), heu4Data(:,10),heu4Data(:,11));
set(p1, "linestyle", "none");
hold off
#}

#{
figure(6)
plot(heu4Data(:,1),heu4Data(:,12),'o','Color','b','LineWidth',2);
hold on
p1 = errorbar(heu4Data(:,1), heu4Data(:,12),heu4Data(:,13));
set(p1, "linestyle", "none");
hold off


figure(7)
plot(heu4Data(:,1),heu4Data(:,14),'o','Color','b','LineWidth',2);
hold on
p1 = errorbar(heu4Data(:,1), heu4Data(:,14),heu4Data(:,15));
set(p1, "linestyle", "none");
hold off
#}

headers = ['param'; 'timeave'; 'timestd'; 'distAv';'DistStd'; '10m/distaAve'; '10m/distStd'; 'overlAve'; 'overlapStd'; 'overlap/distaAve'; 'overlap/distStd'; 'VxAve'; 'VxStd'; 'VyAve'; 'VyStd'];


# -- Corrida vieja --
#{
%[datos1, dataRows, dataCols] = getTry(fileName, 0);

#plot(datos1(:,3), datos1(:,4))
#plot(datos1(:,6), datos1(:,2))

timeStep = 0.05;
display('Tiempo de Transito')
transitTime = datos1(dataRows,2)
display('Longitud de Recorrido')
longitud = sum(datos1(:,6)* timeStep)
display('Velocidad media en x')
velMedX = mean(datos1(:,7))
velMedY = mean(datos1(:,8))
#}

# A = importdata(fileName,',',1);
# datos = A.data;

#A = importdata(fileName,',',1);
#datos = A.data;
#dataRows = size(datos)(1);
#dataCols = size(datos)(2);
#index = 1;
  
#for i = 1:dataRows
#  if (datos(i,1) == 1)
#    for j = 1:dataCols
#      tryData(index,j) = datos(i,j);
#    endfor
#    index = index + 1;  
#  endif
#endfor
#dataRows = index - 1;
