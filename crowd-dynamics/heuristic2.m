function [heuData] = heuristic2()
  directory = '.\results';
  heu = '\heu2\';
  problem = 'Data_N11_Seed1000_';
  config = 'Heu2_tries5_par0.0.csv';
  fileName = strcat(directory, heu, problem, config); 

  ## Distancia Prom = tiempo Promedio, overlap/tiempo,  
  ## try, timeTotal, DistanceTotal, 10m/timeTotal, overlap, overlap/tiempo, Vx, Vy, 
  timeStep = 0.05;

  # ------------------------------------------------------
  h = 1;
  config = 'Heu2_tries5_par0.0.csv';
  fileName = strcat(directory, heu, problem, config); 
  for q = 1:5
    [datos1, dataRows, dataCols] = getTry(fileName, q - 1);
    configData(q,1) = q - 1;
    configData(q,2) = transitTime = datos1(dataRows,2);
    configData(q,3) = sum(datos1(:,6)* timeStep);
    configData(q,4) = 10/configData(q,2);
    configData(q,5) = datos1(dataRows,5);
    configData(q,6) = configData(q,5)/(configData(q,2) * timeStep);
    configData(q,7) = mean(datos1(:,7));
    configData(q,8) = mean(datos1(:,8));
  end

  heuData(h,1) = 0;
  heuData(h,2) = mean(configData(:,2));
  heuData(h,3) = std(configData(:,2));
  heuData(h,4) = mean(configData(:,3));
  heuData(h,5) = std(configData(:,3));
  heuData(h,6) = mean(configData(:,4));
  heuData(h,7) = std(configData(:,4));
  heuData(h,8) = mean(configData(:,5));
  heuData(h,9) = std(configData(:,5));
  heuData(h,10) = mean(configData(:,6));
  heuData(h,11) = std(configData(:,6));
  heuData(h,12) = mean(configData(:,7));
  heuData(h,13) = std(configData(:,7));
  heuData(h,14) = mean(configData(:,8));
  heuData(h,15) = std(configData(:,8));

  #--------------------------------------------------------
  h = 2;
  config = 'Heu2_tries5_par0.785.csv';
  fileName = strcat(directory, heu, problem, config); 
  for q = 1:5
    [datos1, dataRows, dataCols] = getTry(fileName, q - 1);
    configData(q,1) = q - 1;
    configData(q,2) = transitTime = datos1(dataRows,2);
    configData(q,3) = sum(datos1(:,6)* timeStep);
    configData(q,4) = 10/configData(q,2);
    configData(q,5) = datos1(dataRows,5);
    configData(q,6) = configData(q,5)/(configData(q,2) * timeStep);
    configData(q,7) = mean(datos1(:,7));
    configData(q,8) = mean(datos1(:,8));
  end

  heuData(h,1) = 0.785;
  heuData(h,2) = mean(configData(:,2));
  heuData(h,3) = std(configData(:,2));
  heuData(h,4) = mean(configData(:,3));
  heuData(h,5) = std(configData(:,3));
  heuData(h,6) = mean(configData(:,4));
  heuData(h,7) = std(configData(:,4));
  heuData(h,8) = mean(configData(:,5));
  heuData(h,9) = std(configData(:,5));
  heuData(h,10) = mean(configData(:,6));
  heuData(h,11) = std(configData(:,6));
  heuData(h,12) = mean(configData(:,7));
  heuData(h,13) = std(configData(:,7));
  heuData(h,14) = mean(configData(:,8));
  heuData(h,15) = std(configData(:,8));

  #--------------------------------------------------------
  h = 3;
  config = 'Heu2_tries5_par1.571.csv';
  fileName = strcat(directory, heu, problem, config); 
  for q = 1:5
    [datos1, dataRows, dataCols] = getTry(fileName, q - 1);
    configData(q,1) = q - 1;
    configData(q,2) = transitTime = datos1(dataRows,2);
    configData(q,3) = sum(datos1(:,6)* timeStep);
    configData(q,4) = 10/configData(q,2);
    configData(q,5) = datos1(dataRows,5);
    configData(q,6) = configData(q,5)/(configData(q,2) * timeStep);
    configData(q,7) = mean(datos1(:,7));
    configData(q,8) = mean(datos1(:,8));
  end

  heuData(h,1) = 1.571;
  heuData(h,2) = mean(configData(:,2));
  heuData(h,3) = std(configData(:,2));
  heuData(h,4) = mean(configData(:,3));
  heuData(h,5) = std(configData(:,3));
  heuData(h,6) = mean(configData(:,4));
  heuData(h,7) = std(configData(:,4));
  heuData(h,8) = mean(configData(:,5));
  heuData(h,9) = std(configData(:,5));
  heuData(h,10) = mean(configData(:,6));
  heuData(h,11) = std(configData(:,6));
  heuData(h,12) = mean(configData(:,7));
  heuData(h,13) = std(configData(:,7));
  heuData(h,14) = mean(configData(:,8));
  heuData(h,15) = std(configData(:,8));

  #--------------------------------------------------------
  h = 4;
  config = 'Heu2_tries5_par2.352.csv';
  fileName = strcat(directory, heu, problem, config); 
  for q = 1:5
    [datos1, dataRows, dataCols] = getTry(fileName, q - 1);
    configData(q,1) = q - 1;
    configData(q,2) = transitTime = datos1(dataRows,2);
    configData(q,3) = sum(datos1(:,6)* timeStep);
    configData(q,4) = 10/configData(q,2);
    configData(q,5) = datos1(dataRows,5);
    configData(q,6) = configData(q,5)/(configData(q,2) * timeStep);
    configData(q,7) = mean(datos1(:,7));
    configData(q,8) = mean(datos1(:,8));
  end

  heuData(h,1) = 2.352;
  heuData(h,2) = mean(configData(:,2));
  heuData(h,3) = std(configData(:,2));
  heuData(h,4) = mean(configData(:,3));
  heuData(h,5) = std(configData(:,3));
  heuData(h,6) = mean(configData(:,4));
  heuData(h,7) = std(configData(:,4));
  heuData(h,8) = mean(configData(:,5));
  heuData(h,9) = std(configData(:,5));
  heuData(h,10) = mean(configData(:,6));
  heuData(h,11) = std(configData(:,6));
  heuData(h,12) = mean(configData(:,7));
  heuData(h,13) = std(configData(:,7));
  heuData(h,14) = mean(configData(:,8));
  heuData(h,15) = std(configData(:,8));

  #--------------------------------------------------------
  
end