lineaFinal = zeros(11,3);
punto = zeros(5,1);
counter = 1;
for eta = 1:11
 lineaFinal(eta,1) = Datos((eta - 1) * 5 + 1,4);
 for seed = 1:5
   % Anoto el eta
  puntos(seed,1) = Datos((eta - 1) * 5 + seed, 5); 
 endfor
  lineaFinal(eta,2) = mean(puntos);
  lineaFinal(eta,3) = std(puntos);
 endfor
 
for eta = 1:11
 lineaFinal2(eta,1) = Datos((eta - 1) * 5 + 1 + 11 * 5, 4);
 for seed = 1:5
   % Anoto el eta
  puntos(seed,1) = Datos((eta - 1) * 5 + seed + 11 * 5, 5); 
 endfor
  lineaFinal2(eta,2) = mean(puntos);
  lineaFinal2(eta,3) = std(puntos);
 endfor 
  
  
for eta = 1:11
 lineaFinal3(eta,1) = Datos((eta - 1) * 5 + 1 + 22 * 5, 4);
 for seed = 1:5
   % Anoto el eta
  puntos(seed,1) = Datos((eta - 1) * 5 + seed + 22 * 5, 5); 
 endfor
  lineaFinal3(eta,2) = mean(puntos);
  lineaFinal3(eta,3) = std(puntos);
endfor 


%plot(lineaFinal(:,1),lineaFinal(:,2),'Color','r','LineWidth',2)
plot(lineaFinal(:,1),lineaFinal(:,2),'o','Color','r','LineWidth',2);
hold on
errorbar(lineaFinal(:,1), lineaFinal(:,2),lineaFinal(:,3));
%errorbar(lineaFinal(:,1), lineaFinal(:,3));

title('Va en funcion de eta', 'FontSize',20);
%legend('N = 40','Location', 'NorthEast');
xlabel('\eta','FontSize',20);
ylabel('Va','FontSize',20);

%plot(lineaFinal(:,1),lineaFinal(:,2),'Color','r','LineWidth',2)
plot(lineaFinal2(:,1),lineaFinal2(:,2),'o','Color','g','LineWidth',2);
errorbar(lineaFinal2(:,1), lineaFinal2(:,2),lineaFinal2(:,3));
title('Va en funcion de eta', 'FontSize',20);
%legend('N = 100','Location', 'NorthEast');


%plot(lineaFinal(:,1),lineaFinal(:,2),'Color','r','LineWidth',2)
plot(lineaFinal3(:,1),lineaFinal3(:,2),'o','Color','b','LineWidth',2);
errorbar(lineaFinal3(:,1), lineaFinal3(:,2),lineaFinal3(:,3));
title('Va en funcion de eta', 'FontSize',20);
%legend('Location', 'NorthEast');
hold off