package ar.edu.itba.ss;

import java.util.List;

public class Board {
    private Cell[][] board;
    /** Board dimension */
    private int m;
    /** Cell length */
    private double cl;
    private List<Particle> particles;
    
    public Board(int m, List<Particle> particles, double l, boolean periodic) {
        this.m = m;
        cl = l/m;
        this.particles = particles;
        
        board = new Cell[m][];
        
        for (int y = 0; y < m; y++){
            board[y] = new Cell[m];
            for (int x = 0; x < m; x++){
                board[y][x] = new Cell();
            }
        }
        
        if (periodic) {
            for (int y = 0; y < m; y++){
                for (int x = 0; x < m; x++){
                    board[y][x].initNeighborsPeriodic(y,x,m,board);
                }
            }
        } else {
            for (int y = 0; y < m; y++){
                for (int x = 0; x < m; x++){
                    board[y][x].initNeighborsNonPeriodic(y,x,m,board);
                }
            }
        }
        
        resetParticlesInfo();
    }
    
    public Cell[][] getBoard() {
    	return board;
    }
    
    public int getM() {
    	return m;
    }
    
    public double getCL() {
    	return cl;
    }
    
    public Cell getCell(int y, int x) {
    	return board[y][x];
    }
    
    public void resetParticlesInfo() {
        for (Particle particle : particles) {
            board[particle.getYIndex(cl)][particle.getXIndex(cl)].addElement(particle);
        }
    }
    
    public List<Cell> getNeighborCells(int y, int x) {
    	return getCell(y,x).getNeighborCells();
    }
    
    public List<Cell> getNeighborCells(Cell cell) {
    	return cell.getNeighborCells();
    }
    
    public void reset() {
    	for (Cell[] row : board) {
    		for (Cell cell : row) {
    			cell.reset();
    		}
    	}
    	resetParticlesInfo();
    }
}