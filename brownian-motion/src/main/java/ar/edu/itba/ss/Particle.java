package ar.edu.itba.ss;

import java.awt.geom.Point2D;
import java.util.List;

public class Particle {

	// private double velocityModule = 0;
	private double velocityAngle = 0;

	private static int NEW_ID = 1;
	private static int RADIUS_ID = 0;
	private static int COLOR_ID = 1;
	private static int X_ID = 0, Y_ID = 1, VEL_X_ID = 2, VEL_Y_ID = 3;
	private static String DEFAULT_COLOR = "#FFF";
	private int id = 0;
	private Point2D position = new Point2D.Double();
	private Point2D velocity = new Point2D.Double();
	private double radius;
	private String color;
	private double mass = 0;

	public Particle(String[] details) {
		this(Double.parseDouble(details[RADIUS_ID]), details[COLOR_ID]);
	}

	public Particle(double radius) {
		this(radius, DEFAULT_COLOR);
	}

	public Particle(double radius, String color) {
		this.radius = radius;
		this.color = color;
		// this.velocityAngle = MyRandom.getInstance().nextDouble(0, Math.PI * 2);
	}

	void setId() {
		if (id == 0) {
			this.id = NEW_ID;
			NEW_ID++;
		}
	}

	/**
	 * public static void setPeriodic(boolean periodic){
	 * isPeriodic = periodic;
	 * }
	 */

	int getId() {
		return id;
	}

	double getRadius() {
		return radius;
	}

	public Point2D getPosition() {
		return position;
	}

	double getY() {
		return position.getY();
	}

	double getX() {
		return position.getX();
	}

	public double getAngle() {
		return velocityAngle;
	}

	void setAngle(double velocityAngle) {
		this.velocityAngle = velocityAngle;
	}

	/**
	 * @param cl cell length
	 * @return y normalized index
	 */
	int getYIndex(double cl) {
		return (int) (getY() / cl) % (int) cl;
	}

	/**
	 * @param cl cell length
	 * @return x normalized index
	 */
	int getXIndex(double cl) {
		return (int) (getX() / cl) % (int) cl;
	}

	public Particle setProperties(String[] properties) {
		return setProperties(Double.parseDouble(properties[X_ID]), Double.parseDouble(properties[Y_ID]),
				Double.parseDouble(properties[VEL_X_ID]), Double.parseDouble(properties[VEL_Y_ID]));
	}

	Particle setProperties(double x, double y, double velX, double velY) {
		position.setLocation(x,y);
		velocity.setLocation(velX, velY);
		return this;
	}

	public double getDistance(double x1, double y1, double x2, double y2){
		double x = x1 - x2;
		double y = y1 - y2;
		return Math.sqrt(x * x + y * y);
	}

	public double getDistance(Particle other) {
		/** Distance between centers minus the radius of the particles */
		return position.distance(other.position) - radius - other.radius;
	}

	void advance(double time) {
		/** %l accounts for periodic condition */
		position.setLocation(position.getX() + getVelX()*time, position.getY() + getVelY()*time);
	}

	@Override
	public String toString() {
		return "Id: " + id + " (Radius: " + radius + ", Position x: " + position.getX() + ", y: " + position.getY() + ", Velocity x: " + velocity.getX() + ", Velocity y: " + velocity.getY() + ")";
	}
	
	public String getStaticRepresentation() {
		return radius + " " + color;
	}

	public String getDynamicRepresentation() {
		return position.getX() + " " + position.getY() + " " + getVelX() + " " + getVelY();
	}
	
	double getVelX() {
		return velocity.getX();
		// return velocity * Math.cos(velocityAngle);
	}
	
	double getVelY() {
		return velocity.getY();
		// return velocity * Math.sin(velocityAngle);
	}

	void setRadius(double radius) {
		this.radius = radius;
	}

	double getMass() {
		return mass;
	}

	void setMass(double mass) {
		this.mass = mass;
	}

	static boolean isValidParticle(Particle newParticle, List<Particle> currentParticles) {
		for (Particle currentParticle : currentParticles) {
			if (currentParticle.getDistance(newParticle) < 0) {
				return false;
			}
		}
		return true;
	}
	
	static Particle generateRandom(double l, double r) {
		double xPos, yPos;

		xPos = MyRandom.getInstance().nextDouble(r, l - r);
		yPos = MyRandom.getInstance().nextDouble(r, l - r);

		xPos = Math.round(xPos*100)/(double)100;
		yPos = Math.round(yPos*100)/(double)100;

		Particle particle = new Particle(r);

		double velocityAngle = MyRandom.getInstance().nextDouble(0, Math.PI * 2);
		particle.setAngle(velocityAngle);

		double minVelocity = Configuration.getInstance().getMinVelocity();
		double maxVelocity = Configuration.getInstance().getMaxVelocity();
		double velocityModule = MyRandom.getInstance().nextDouble(minVelocity, maxVelocity);

		particle.setProperties(xPos,yPos,velocityModule * Math.cos(velocityAngle),velocityModule * Math.sin(velocityAngle));

		return particle;
	}

	String getColor(){
		double red = 0, green = 0, blue = 0;
		if (mass == Configuration.getInstance().getBigParticleMass()){
			red = 1;
		} else {
			blue = 1;
		}
		StringBuilder builder = new StringBuilder()
				.append(red)
				.append(" ")
				.append(green)
				.append(" ")
				.append(blue);
		return builder.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Particle particle = (Particle) o;
		return id == particle.id;
	}

	@Override
	public int hashCode() {
		return id;
	}

	private double getVelocity(){
		final double velocityModule;
		velocityModule = Math.sqrt(velocity.getX() * velocity.getX() + velocity.getY() * velocity.getY());
		return velocityModule;
	}

	static double getkineticEnergy(List <Particle> list){
		double kineticEnergy = 0;
		for (Particle particle : list){
			kineticEnergy = kineticEnergy +  particle.getMass() * particle.getVelocity() * particle.getVelocity();
		}
		return (kineticEnergy/2)/list.size();
	}
}