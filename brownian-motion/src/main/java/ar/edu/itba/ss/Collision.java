package ar.edu.itba.ss;

import java.util.PriorityQueue;

public class Collision implements Comparable<Collision> {
	// TODO Ver toda clase - Oliver
	/** Collision time */
	private double time;
	private BaseCollision collision;
	
	/** Particle against particle */
	public Collision(double time, Particle particle1, Particle particle2) {
		this.time = time;
		collision = new ParticlesCollision(particle1,particle2);
	}
	
	/** Particle against wall */
	private Collision(double time, BaseCollision collision) {
		this.time = time;
		this.collision = collision;
	}
	
	public static Collision ParticleHorizontalWallCollision(double time, Particle particle) {
		return new Collision(time, new ParticleHorizontalWallCollision(particle));
	}
	
	public static Collision ParticleVerticalWallCollision(double time, Particle particle) {
		return new Collision(time, new ParticleVerticalWallCollision(particle));
	}
	
	public double getTime() {
		return time;
	}

	public void decreaseTime(double t) {
		time -= t;
	}
	
	/**
	 * Do collision action
	 * @return Particles that collied
	 */
	public Particle[] collide() {
		return collision.collide();
	}

	public static Collision previous(Particle particle, PriorityQueue<Collision> cQueue) {
		for (Collision collision : cQueue) {
			if (collision.contains(particle)) {
				return collision;
			}
		}
		return null;
	}

	public boolean contains(Particle other) {
		return collision.contains(other);
	}

	@Override
	public int compareTo(Collision o) {
		return Double.compare(getTime(),o.getTime());
	}

	@Override
	public String toString() {
		return "Collision{" +
				"t=" + time + ", " +
				collision.toString() +
				'}';
	}

	private static class ParticlesCollision extends BaseCollision {

		Particle particle1;
		Particle particle2;
		public ParticlesCollision(Particle particle1, Particle particle2) {
			this.particle1 = particle1;
			this.particle2 = particle2;
		}

		@Override
		Particle[] collide() {
			updateVelocityParticleAgainstParticle(particle1,particle2);
			return new Particle[]{particle1,particle2};
		}

		private void updateVelocityParticleAgainstParticle(Particle p1, Particle p2) {

			double dx = p2.getX() - p1.getX();
			double dy = p2.getY() - p1.getY();

			double dvx = p2.getVelX() - p1.getVelX();
			double dvy = p2.getVelY() - p1.getVelY();

			/* dv.dr = (dvx)(dx) + (dvy)(dy) */
			double dvdr = (dx * dvx) + (dy * dvy);

			/* sigma = ri + rj */
			double sigma = p1.getRadius() + p2.getRadius();

			/* J = 2 mi mj (dv.dr) / sigma (mi+mj) */
			double j = (2 * p1.getMass() * p2.getMass() * dvdr) / (sigma * (p1.getMass() + p2.getMass()));

			/* Jx = J dx / sigma , Jy = J dy / sigma */
			double jx = j*dx/sigma;
			double jy = j*dy/sigma;

			/* vxi = vx0 + Jx/mi, vxj = vx0 - Jx/mj, vyi = vy0 + Jy/mi, vyj = vy0 - Jy/mj */
			p1.setProperties(p1.getX(), p1.getY(), p1.getVelX() + jx/p1.getMass(), p1.getVelY() + jy/p1.getMass() );
			p2.setProperties(p2.getX(), p2.getY(), p2.getVelX() - jx/p2.getMass(), p2.getVelY() - jy/p2.getMass() );

		}

		@Override
		boolean contains(Particle other) {
			return particle1.equals(other) || particle2.equals(other);
		}

		@Override
		public String toString() {
			return "particle1=" + particle1.getId() +
					", particle2=" + particle2.getId() ;
		}
	}

	private static class ParticleVerticalWallCollision extends BaseCollision {

		Particle particle;

		public ParticleVerticalWallCollision(Particle particle) {
			this.particle = particle;
		}

		@Override
		Particle[] collide() {
			updateVelocityVerticalWall(particle);
			return new Particle[]{particle};
		}

		private void updateVelocityVerticalWall(Particle p){
			/* If crashes against vertical wall -> (-vx,vy) */
			p.setProperties(p.getX(), p.getY(), -1 * p.getVelX(), p.getVelY() );
		}

		@Override
		boolean contains(Particle o) {
			return particle.equals(o);
		}

		@Override
		public String toString() {
			return "Particle: " + particle.getId();
		}
	}

	private static class ParticleHorizontalWallCollision extends BaseCollision {

		Particle particle;

		public ParticleHorizontalWallCollision(Particle particle) {
			this.particle = particle;
		}

		@Override
		Particle[] collide() {
			updateVelocityHorizontalWall(particle);
			return new Particle[]{particle};
		}

		private void updateVelocityHorizontalWall(Particle p){
			/* If crashes against horizontal wall -> (vx,-vy) */
			p.setProperties(p.getX(), p.getY(), p.getVelX(), -1 * p.getVelY() );
		}

		@Override
		boolean contains(Particle o) {
			return particle.equals(o);
		}

		@Override
		public String toString() {
			return "particle=" + particle.getId() ;
		}
	}

	private static abstract class BaseCollision {

		abstract Particle[] collide();
		abstract boolean contains(Particle other);
	}
}