package ar.edu.itba.ss;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

public class Algorithm {
	private final static double FRAME_TIME = 0.01;
	/** For no collision between particles, a big enough time is used meaning it will not affect the rest of the logic. */
	private final static double NO_COLLISION = Double.MAX_VALUE;
	private double l;
	private double totalTime = 0;
	/** Big particle plus N small particles */
    private List<Particle> particles;
    private PriorityQueue<Collision> cQueue = new PriorityQueue<Collision>();
	private int timeId = 1;
	private boolean firstTime = true;
	private boolean firstTimeCol = true;

	public Algorithm(List<Particle> particles) {
		this.l = Configuration.getInstance().getL();
		this.particles = particles;
		firstTime = true;
		firstTimeCol = true;
	}

	public void run() {
		generateCollisionTimeQueue();
		int iteration_limit = Configuration.getInstance().getIteration_limit();

		do {
			System.out.println(totalTime);
			Collision c = cQueue.poll();
			double cTime = c.getTime();
			recalculateParticles(cTime);
			Particle[] cParticles = c.collide();
			generateCollisionTimeQueue(cParticles,cTime);
		} while (totalTime <= iteration_limit);

	}

	/** Initial routine */
	public void generateCollisionTimeQueue() {
		for (int i = 0, s = particles.size(); i < s; i++) {
			generateCollisionTimeQueue(particles.get(i));
		}
	}
	
	/** Initial routine */
	private void generateCollisionTimeQueue(Particle current) {
		wallCollision(current);
		
		for (Particle other : particles) {
			if (current.equals(other)) continue;
			
			double currentTime = tCollisionParticles(current,other);
			
			if (currentTime != NO_COLLISION) {
				cQueue.add(new Collision(currentTime,current,other));
				if (currentTime < 0) {
					System.out.println("Hola");
				}
			}
		}
	}

	private void wallCollision(Particle current){
		double horizontalTc = tCollisionHorizontalWall(current);
		double verticalTc = tCollisionVerticalWall(current);
		
		if (verticalTc < horizontalTc){
			cQueue.add(Collision.ParticleVerticalWallCollision(verticalTc,current));
		} else {
			cQueue.add(Collision.ParticleHorizontalWallCollision(horizontalTc,current));
		}
	}
	
	/** Mid routine */
	public void generateCollisionTimeQueue(Particle[] collisionParticles, double lastColTime) {
		Queue<Collision> toBeRemoved = new LinkedList<Collision>();
		
		for (Collision collision : cQueue) {
			boolean isCollisionValid = true;
			
			for (Particle collisionParticle : collisionParticles) {
				if (collision.contains(collisionParticle)) {
					isCollisionValid = false;
					break;
				}
			}
			
			if (isCollisionValid) {
				collision.decreaseTime(lastColTime);
				if (collision.getTime() < 0) {
					System.out.println("Hola");
				}
			} else {
				toBeRemoved.add(collision);
			}
		}
		
		cQueue.removeAll(toBeRemoved);

		for (Particle collisionParticle : collisionParticles) {
			generateCollisionTimeQueue(collisionParticle);
		}
	}

	public double tCollisionVerticalWall(Particle particle) {
		/* if vxi > 0 - > tc = (xp2 - R - x0) / vx, if vxi < 0 - > tc = (xp1 + R - x0) / vx */
		return tCollisionWall(particle,particle.getX(),particle.getVelX());
	}

	public double tCollisionHorizontalWall(Particle particle) {
		/* if vyi > 0 - > tc = (yp2 - R - y0) / vy, if vyi < 0 - > tc = (yp1 + R - x0) / vy */
		return tCollisionWall(particle,particle.getY(),particle.getVelY());
	}

	private double tCollisionWall(Particle particle, double pos, double vel) {
		double t;
		if(vel > 0) {
			t = (l - particle.getRadius() - pos) / vel;
		} else if (vel < 0) {
			t = (particle.getRadius() - pos) / vel;
		} else {
			t = NO_COLLISION;
		}
		return t;
	}
	
	public double tCollisionParticles(Particle particle1, Particle particle2) {
		double deltaRx  = particle2.getX()    - particle1.getX();
		double deltaRy  = particle2.getY()    - particle1.getY();
		double deltaVx  = particle2.getVelX() - particle1.getVelX();
		double deltaVy  = particle2.getVelY() - particle1.getVelY();
		/* dv.dr = (dvx)(dx) + (dvy)(dy) */
		double deltaVmR = deltaVx*deltaRx     + deltaVy*deltaRy;

		if ( deltaVmR >= 0 ) {
			return NO_COLLISION;
		}

		/* sigma = ri + rj */
		double sigma  = particle1.getRadius() + particle2.getRadius();
		double deltaRmR = deltaRx*deltaRx     + deltaRy*deltaRy;
		double deltaVmV = deltaVx*deltaVx     + deltaVy*deltaVy;
		/* d =( dv.dr)^2 - (dv.dv)(dr.dr-sigma^2) */
		double d = deltaVmR*deltaVmR - deltaVmV*(deltaRmR - sigma*sigma);
		
		if (d < 0) {
			return NO_COLLISION;
		}

		/* tc = - dv . dr + sqrt(d) / dv . dv */
		double t = - ((deltaVmR + Math.sqrt(d)) / deltaVmV);
		return t;
	}
	
	public void recalculateParticles(double time) {
		double timeToNextFrame = FRAME_TIME - (totalTime % FRAME_TIME);
		
		while (time >= timeToNextFrame) {
			advance(timeToNextFrame);
			generateOutput(generateFileData(particles), timeId);
			generateBigColisionOutput(generateBigColisionData(particles, timeId), timeId);
			// generateFileData(particles);
			timeId++;
			time -= timeToNextFrame;
			timeToNextFrame = FRAME_TIME;
		}

		advance(time);
		String data = generateColisionData(particles, totalTime);
		generateColisionOutput(data, totalTime);
	}

	private void advance(double time) {
		for (Particle particle : particles) {
			particle.advance(time);
		}
		totalTime += time;
	}

	private void generateOutput(String particleData, int time){
		StringBuilder builder = new StringBuilder()
				.append(Configuration.outputDir)
				.append("seed")
				.append(Configuration.getInstance().getSeed())
				.append("_N")
				.append(Configuration.getInstance().getN())
				.append("_min")
				.append(Configuration.getInstance().getMinVelocity())
				.append("_max")
				.append(Configuration.getInstance().getMaxVelocity())
				.append(".txt");
		String filePath =  builder.toString();
		Path path = Paths.get(filePath);

		StringBuilder timeBuilder = new StringBuilder()
										.append(particles.size())
										.append("\r\n//")
                                        .append(time)
                                        .append("\r\n");
		StringBuilder toPrint = new StringBuilder()
									.append(timeBuilder)
									.append(particleData);
		try {
			if (!Files.exists(path)){
				Files.write(path,toPrint.toString().getBytes(),StandardOpenOption.CREATE);
			} else {
				Files.write(path,toPrint.toString().getBytes(),StandardOpenOption.APPEND);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void generateColisionOutput(String particleData, double time){
		StringBuilder builder = new StringBuilder()
				.append(Configuration.outputDir)
				.append("Col_seed")
				.append(Configuration.getInstance().getSeed())
				.append("_N")
				.append(Configuration.getInstance().getN())
				.append("_min")
				.append(Configuration.getInstance().getMinVelocity())
				.append("_max")
				.append(Configuration.getInstance().getMaxVelocity())
				.append(".csv");
		String filePath =  builder.toString();
		Path path = Paths.get(filePath);

		try {
			if (!Files.exists(path)){
				Files.write(path,getColisionHeader().toString().getBytes(),StandardOpenOption.CREATE);
			}
			Files.write(path,particleData.toString().getBytes(),StandardOpenOption.APPEND);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void generateBigColisionOutput(String particleData, int time){
		StringBuilder builder = new StringBuilder()
				.append(Configuration.outputDir)
				.append("BigCol_seed")
				.append(Configuration.getInstance().getSeed())
				.append("_N")
				.append(Configuration.getInstance().getN())
				.append("_min")
				.append(Configuration.getInstance().getMinVelocity())
				.append("_max")
				.append(Configuration.getInstance().getMaxVelocity())
				.append(".csv");
		String filePath =  builder.toString();
		Path path = Paths.get(filePath);

		try {
			if (!Files.exists(path)){
				Files.write(path,getBigColisionHeader().toString().getBytes(),StandardOpenOption.CREATE);
			}
			Files.write(path,particleData.toString().getBytes(),StandardOpenOption.APPEND);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String getBigColisionHeader(){
		return "ColTime,id,x,y,vx,vy,mass\r\n";
	}

	private String getColisionHeader(){
		return "ColTime,id,x,y,vx,vy,mass\r\n";
	}

	private String generateFileData(List<Particle> particleList){
		//StringBuilder builder = new StringBuilder()
		//		.append(particleList.size())
		//		.append("\r\n")
		//		.append("//ID\t X\t Y\t Vx\t Vy\t Radius\t R\t G\t B\t Mass\r\n");
				//.append("//ID\t X\t Y\t Radius\t R\t G\t B\t\r\n");
		StringBuilder builder = new StringBuilder();
		for (Particle current: particleList){
			builder.append(current.getId())
					.append(" ")
					.append(current.getX())
					.append(" ")
					.append(current.getY())
					.append(" ")
					.append(current.getVelX())
					.append(" ")
					.append(current.getVelY())
					.append(" ")
					.append(current.getRadius())
					.append(" ")
					.append(current.getColor())
					.append(" ")
					.append(current.getMass())
					.append("\r\n");
			// Ovito mapea R G B entre [0, 1]
		}
		return builder.toString();
	}

	private String generateColisionData(List<Particle> particleList, double colisionTime) {
		StringBuilder builder = new StringBuilder();
		for (Particle current: particleList){
			if (current.getId() <= Configuration.getInstance().getN() + 1){
				builder.append(colisionTime)
						.append(",")
						.append(current.getId())
						.append(",")
						.append(current.getX())
						.append(",")
						.append(current.getY())
						.append(",")
						.append(current.getVelX())
						.append(",")
						.append(current.getVelY())
						.append(",")
						.append(current.getMass())
						.append("\r\n");
			}
			// Ovito mapea R G B entre [0, 1]
		}
		return builder.toString();
	}

	private String generateBigColisionData(List<Particle> particleList, int time) {
		StringBuilder builder = new StringBuilder();
		Particle particle = particleList.get(0);
		builder.append(time)
			.append(",")
			.append(particle.getId())
			.append(",")
			.append(particle.getX())
			.append(",")
			.append(particle.getY())
			.append(",")
			.append(particle.getVelX())
			.append(",")
			.append(particle.getVelY())
			.append(",")
			.append(particle.getMass())
			.append("\r\n");
		particle = particleList.get(1);
		builder.append(time)
				.append(",")
				.append(particle.getId())
				.append(",")
				.append(particle.getX())
				.append(",")
				.append(particle.getY())
				.append(",")
				.append(particle.getVelX())
				.append(",")
				.append(particle.getVelY())
				.append(",")
				.append(particle.getMass())
				.append("\r\n");
			// Ovito mapea R G B entre [0, 1]
		return builder.toString();
	}

}