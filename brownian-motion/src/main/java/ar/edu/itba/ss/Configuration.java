package ar.edu.itba.ss;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

class Configuration {
	private final static String projectDir = System.getProperty("user.dir");
	final static String staticInfoDir = projectDir + "/resources/staticInfo.txt";
	final static String dynamicInfoDir = projectDir + "/resources/dynamicInfo.txt";
	final static String outputDir = projectDir + "/results/";
	final static String analyticsFileName = "analyticsFile.csv";

	private Properties properties;
	private InputStream inStream = null;

	private static Configuration instance;

	/** Seed */
	private long seed;

	private int iteration_limit;

	/** Number of Particles*/
	private int N;

	/** Length of board */
	private double L;

	/** Default Bigger Particle Radius */
	private double bigParticleRadius;

	/** Default Bigger Particle mass */
	private double bigParticleMass;

	/** Default small Particle Radius */
	private double smallParticleRadius;

	/** Default small Particle mass */
	private double smallParticleMass;

	/** Default Minimun Velocity */
	private double minVelocity;
	/** Default Maximun Velocity */
	private double maxVelocity;

	private Configuration(){
		properties = new Properties();
		try {
			String configInfoDir = projectDir + "/resources/config.properties";
			inStream = new FileInputStream(configInfoDir);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		loadConfiguration();
	}

	static Configuration getInstance(){
		if (instance == null){
			instance = new Configuration();
		}
		return instance;
	}

	private void loadConfiguration() {

		try {
			properties.load(inStream);
		} catch (IOException e){
			e.printStackTrace();
		}

		seed = Long.parseLong(properties.getProperty("seed"));


		iteration_limit = Integer.parseInt(properties.getProperty("iteration_limit"));

		L = Double.parseDouble(properties.getProperty("board_size"));

		N = Integer.parseInt(properties.getProperty("particle_count"));

		bigParticleMass = Double.parseDouble(properties.getProperty("bigParticleMass"));
		bigParticleRadius = Double.parseDouble(properties.getProperty("bigParticleRadius"));
		smallParticleMass = Double.parseDouble(properties.getProperty("smallParticleMass"));
		smallParticleRadius = Double.parseDouble(properties.getProperty("smallParticleRadius"));
		minVelocity = Double.parseDouble(properties.getProperty("minVelocity"));
		maxVelocity = Double.parseDouble(properties.getProperty("maxVelocity"));
	}

	long getSeed() {
		return seed;
	}

	int getIteration_limit() {
		return iteration_limit;
	}

	int getN() {
		return N;
	}

	double getL() {
		return L;
	}


	double getBigParticleRadius() {
		return bigParticleRadius;
	}

	double getBigParticleMass() {
		return bigParticleMass;
	}

	double getSmallParticleRadius() {
		return smallParticleRadius;
	}

	double getSmallParticleMass() {
		return smallParticleMass;
	}

	double getMinVelocity() {
		return minVelocity;
	}

	double getMaxVelocity() {
		return maxVelocity;
	}

	@Override
	public String toString() {
		return "Configuration{" +
				"seed=" + seed +
				", N=" + N +
				", L=" + L +
				", bigParticleRadius=" + bigParticleRadius +
				", bigParticleMass=" + bigParticleMass +
				", smallParticleRadius=" + smallParticleRadius +
				", smallParticleMass=" + smallParticleMass +
				'}';
	}
}