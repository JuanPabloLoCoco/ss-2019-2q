package ar.edu.itba.ss;

import java.io.IOException;
import java.util.List;

public class App {
    public static void main( String[] args ) throws IOException {
        System.out.println("It´s running Brownian Motion");
        System.out.println(Configuration.getInstance().toString());

        Long seed = Configuration.getInstance().getSeed();
        MyRandom.setSeed(seed);

        List<Particle> particles = ParticleFactory.getInstance().generateBrownianMotionParticles();
        for (Particle particle: particles){
            System.out.println(particle.toString());
        }
        double kineticEnergy = Particle.getkineticEnergy(particles);
        System.out.println("Kinetic Energy = " + kineticEnergy);


        Particle p1 = new Particle(0.001);
        p1.setId();
        Particle p2 = new Particle(0.001);
        Particle p3 = new Particle(0.001);
        Particle p4 = new Particle(0.001);
        p1.setProperties(0,0,0,0);
        p2.setProperties(0,Configuration.getInstance().getL(),0,0);
        p3.setProperties(Configuration.getInstance().getL(),0,0,0);
        p4.setProperties(Configuration.getInstance().getL(),Configuration.getInstance().getL(),0,0);
        p1.setId();
        p2.setId();
        p3.setId();
        p4.setId();
        particles.add(p1);
        particles.add(p2);
        particles.add(p3);
        particles.add(p4);

        Algorithm algorithm = new Algorithm(particles);
        algorithm.run();

    }
}