package ar.edu.itba.ss;

import sun.awt.ConstrainableGraphics;

import java.util.ArrayList;
import java.util.List;

public class ParticleFactory {
    private static ParticleFactory instance;


    public static ParticleFactory getInstance(){
        if (instance == null){
            instance = new ParticleFactory();
        }
        return instance;
    }

    public List <Particle> generateBrownianMotionParticles(){
        List<Particle> particles = new ArrayList<>();
        /* Big Particle generation */
        Particle bigParticle = generateBigParticle();
        particles.add(bigParticle);

        /* Small Particles Generation */
        generateSmallParticles(particles);

        return particles;
    }

    private Particle generateBigParticle(){
        Particle ret = new Particle(Configuration.getInstance().getBigParticleRadius());
        Double l = Configuration.getInstance().getL();
        ret.setProperties(l/2, l/2, 0, 0);
        ret.setMass(Configuration.getInstance().getBigParticleMass());
        ret.setRadius(Configuration.getInstance().getBigParticleRadius());
        ret.setId();
        return ret;
    }

    private void generateSmallParticles(List<Particle> list){
        int i = 0;
        int n = Configuration.getInstance().getN();
        double L = Configuration.getInstance().getL();
        double radius = Configuration.getInstance().getSmallParticleRadius();
        double mass = Configuration.getInstance().getSmallParticleMass();
        while (i < n){
            Particle particle = Particle.generateRandom(L,radius);
            while (!Particle.isValidParticle(particle, list)){
                particle = Particle.generateRandom(L, radius);
            }
            particle.setId();
            particle.setMass(mass);
            list.add(particle);
            i++;
        }
    }
}
