# Ej3.m

# '.\results\BigCol_seed1250_N200_min0.0_max4.0.csv'
fileName = '.\results\Ej4\BigCol_seed1234_N200_min0.0_max4.0.csv'
[col, size, particles, data] = readParticles(fileName);
size1 = firstTouch(col, size)

for x = 1:40
  posX = col{x,2}(1,3);
  posY = col{x,2}(1,4);
  velX = col{x,2}(1,5);
  velY = col{x,2}(1,6);
  posArray(x,1) = posX;
  posArray(x,2) = posY;
endfor

plot(posArray(:,1), posArray(:,2))
axis([0 0.5 0 0.5])
