function [dc, size] = generateDC2(col, size)
  size1 = firstTouch2(col, size)

  initial = floor(size1/2);

  index = 1;
  for x = initial : size1
    posX = col{x,1}(1,3);
    posY = col{x,1}(1,4);
    lineArray(index,1) = posX;
    lineArray(index,2) = posY;
    index = index + 1;  
  endfor
  index = index - 1;
  index;
  
  Xi = lineArray(1,1);
  Yi = lineArray(1,2);

  for y = 1 : index
    if (y == 1)
      dc(y) = 0;
    else 
      Xf = lineArray(y,1);
      Yf = lineArray(y,2);
      dc(y) = (Xf - Xi)*(Xf - Xi)+(Yf - Yi)*(Yf - Yi); 
    endif
  endfor
  size = index;
end