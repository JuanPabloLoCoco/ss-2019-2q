# Ej 2

fileName = '.\results\Ej1y2\Col_seed1230_N200_min0.0_max0.1.csv'
[col, size, particles, data] = readParticles(fileName);

ter = floor(size * 2/3);
index = 1
for x = ter:size
  for i = 1 : particles
    vx = col{x, i}(1, 5);
    vy = col{x, i}(1, 6);
    vel = sqrt(vx * vx + vy * vy);
    velocity(1,index) = vel;
    index = index + 1;
  endfor
endfor
velSize = index - 1;

indexIn = 1
for i = 1 : particles
  vx = col{1, i}(1, 5);
  vy = col{1, i}(1, 6);
  vel = sqrt(vx * vx + vy * vy);
  velocityIn(1,indexIn) = vel;
  indexIn = indexIn + 1;
endfor
velSizeIni = indexIn - 1;

figure(1);
[velCount, velCenters] = hist(velocity,20);
bar(velCenters,velCount/velSize);
xlabel('Velocidad','FontSize',20);
ylabel('Probabilidad','FontSize',20);

figure(2);
[velCountIni, velCentersIni] = hist(velocityIn,20);
bar(velCentersIni,velCountIni/velSizeIni);
xlabel('Velocidad','FontSize',20);
ylabel('Probabilidad','FontSize',20);
