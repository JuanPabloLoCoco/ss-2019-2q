# KineticEnergy
function [energy] = calculateKineticEnergy(colisions, particles)
  energy = 0;
  for x = 1 : particles
   vx = colisions{1, x}(1, 5);
   vy = colisions{1, x}(1, 6);
   mass = colisions{1,x}(1,7);
   vel = vx * vx + vy * vy;
   energy = energy + mass * vel;
  endfor
  energy = energy/2;
  energy = energy/particles;
end
