# Ej 1

# '.\results\Ej1y2\Col_seed1230_N100_min0.0_max0.1.csv'
fileName = '.\results\Ej1y2\Col_seed1230_N50_min0.0_max0.1.csv'
[col, size, particles, data] = readParticles(fileName);

[freq] = calculateFrequency(col,size);

freq
colTimeSum = 0;
for x = 1 : size - 1
  colTime1 = col{x,2}(1,1);
  colTime2 = col{x + 1,2}(1,1);
  colTime(1,x) = colTime2 - colTime1;
  colTimeSum = colTimeSum + colTime2 - colTime1;
endfor
colTimeAverage = colTimeSum / x
colTimeSize = x;

[colTimeCount, colTimeCenters] = hist(colTime,20);
bar(colTimeCenters,colTimeCount/x)
xlabel('Tiempos de colision','FontSize',20);
ylabel('Probabilidad','FontSize',20);



