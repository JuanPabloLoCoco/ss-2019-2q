function x = firstTouch(col, size)
  
  for x = 1:size
    posX = col{x,1}(1,3);
    posY = col{x,1}(1,4);
    velX = col{x,1}(1,5);
    velY = col{x,1}(1,6);
    if (x != 1)
      if (lastVx == (-1)* velX)
        return
      elseif(lastVy == (-1)* velY)
        return
      endif
    endif
    lastVx = velX;
    lastVy = velY;
  endfor
  return;
end

function resp = TouchWithWall(posX, posY, velX, velY, lastVx, lastVy)
  resp = 0;
  if (lastVx * velX > 0 && lastVy * velY > 0)
    return;
  endif
  
  if (lastVx + lastVy > 0.005 && lastVx + lastVy > 0.005)
    return;
  endif
  resp = 1;
end