function [colision, colisionsSize, particles, data] = readParticles(fileName)
  # Cargo los datos. 
  A = importdata(fileName,',',1);
  datos = A.data;
  countDatos = size(datos)(1,1);

  #Separo los datos por tiempos
  i = 1;
  index = 1;
  colindex = 1;

  for i = 1:countDatos 
    myIndex = datos(i,:)(1,2);
    if (i != 1) 
      if (myIndex < lastIndex)
        index = 1;
        colindex = colindex + 1;
      endif  
    endif
    lastIndex = myIndex;
    colision{colindex, index} = datos(i,:);
    index = index + 1;
  endfor
  colisionsSize = size(colision)(1,1);
  particles = size(colision)(1,2);
  data = A.textdata;
  end