## ej2
clear;

fileName = '.\results\L\Data_N9_Seed1000_Heu1.0_L11.0_Games5.csv';
[avgRounds,stdRound, avgGameTime, stdGameTime, avgFirstTouch, stdFirstTouch] = getData(fileName);

value(1, 1) = 11;
value(1, 2) = avgRounds;
value(1, 3) = stdRound;
value(1, 4) = avgGameTime;
value(1, 5) = stdGameTime;
value(1, 6) = avgFirstTouch;
value(1, 7) = stdFirstTouch;

fileName = '.\results\L\Data_N9_Seed1000_Heu1.0_L13.0_Games5.csv';
[avgRounds,stdRound, avgGameTime, stdGameTime, avgFirstTouch, stdFirstTouch] = getData(fileName);

value(2, 1) = 13;
value(2, 2) = avgRounds;
value(2, 3) = stdRound;
value(2, 4) = avgGameTime;
value(2, 5) = stdGameTime;
value(2, 6) = avgFirstTouch;
value(2, 7) = stdFirstTouch;

fileName = '.\results\L\Data_N9_Seed1000_Heu1.0_L15.0_Games5.csv';
[avgRounds,stdRound, avgGameTime, stdGameTime, avgFirstTouch, stdFirstTouch] = getData(fileName);

value(3, 1) = 15;
value(3, 2) = avgRounds;
value(3, 3) = stdRound;
value(3, 4) = avgGameTime;
value(3, 5) = stdGameTime;
value(3, 6) = avgFirstTouch;
value(3, 7) = stdFirstTouch;

fileName = '.\results\L\Data_N9_Seed1000_Heu1.0_L17.0_Games5.csv';
[avgRounds,stdRound, avgGameTime, stdGameTime, avgFirstTouch, stdFirstTouch] = getData(fileName);

value(4, 1) = 17;
value(4, 2) = avgRounds;
value(4, 3) = stdRound;
value(4, 4) = avgGameTime;
value(4, 5) = stdGameTime;
value(4, 6) = avgFirstTouch;
value(4, 7) = stdFirstTouch;


figure(1)
plot(value(:,1),value(:,2),'o','Color','b','LineWidth',2);
hold on

p1 = errorbar(value(:,1), value(:,2),value(:,3));
set(p1, "linestyle", "none");
hold off

figure(2)
plot(value(:,1),value(:,4),'o','Color','b','LineWidth',2);
hold on

p1 = errorbar(value(:,1), value(:,4),value(:,5));
set(p1, "linestyle", "none");
hold off

figure(3)
plot(value(:,1),value(:,6),'o','Color','b','LineWidth',2);
hold on

p1 = errorbar(value(:,1), value(:,6),value(:,7));
set(p1, "linestyle", "none");
hold off
