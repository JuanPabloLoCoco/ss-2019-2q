function [avgRound,stdRound, avgGameTime, stdGameTime, avgFirstTouch, stdFirstTouch] = getData(fileName)
  ## GameCount,round,time,goodMenCount,badMenCount
  # Cargo los datos.   
  A = importdata(fileName,',',1);
  datos = A.data;
  countDatos = size(datos)(1,1);

  #Separo los datos por tiempos
  time = 1;
  round = 1;
  badMenCount = 1;
  goodMenCountInitial = datos(1, 4);
  index = 1;
  colindex = 1;
  game = 1;
  
  for time = 1:countDatos 
    indexTime = datos(time, 3);
    goodMenCount = datos(time, 4);
    game = datos(time, 1) + 1;
    # Agrego FirstTouch
    if (goodMenCount < goodMenCountInitial && datos(time - 1, 4) == goodMenCountInitial)
        gameData(game, 3) = datos(time - 1, 3);
    endif
    if (game > 1 && (datos(time - 1, 1) + 1) < game)
      gameData(game - 1, 1) = datos(time - 1, 2) + 1; 
      gameData(game - 1, 2) = datos(time - 1, 3);
    endif
  endfor
  game = datos(time, 1) + 1;
  gameData(game, 1) = datos(time, 2) + 1;
  gameData(game, 2) = datos(time, 3); 
  
  gameData
  
  avgRound = mean(gameData(:, 1));
  stdRound = std(gameData(:, 1));
  avgGameTime = mean(gameData(:, 2));
  stdGameTime = std(gameData(:, 2));
  avgFirstTouch = mean(gameData(:, 3));
  stdFirstTouch = std(gameData(:, 3));
end