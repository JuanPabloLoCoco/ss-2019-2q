# ss-2019-2q

staticInfo.txt format:
Periodic state ("PERIODIC" if periodic. Something else if not).
N:  Number of particles
L:  Length of board
M:  Dimension of board
Rc: Default Particle Radius
R:  Interaction Radius
... (Particles Info (Radius <= Default and Properties) from 1 to N)
