## Analiticos BadMan

clear;

fileName = '.\results\L13\Data_N9_Seed1000_Heu0.25_L13.0_Games5.csv';
[avgRounds,stdRound, avgGameTime, stdGameTime, avgFirstTouch, stdFirstTouch] = getData(fileName);

value(1, 1) = 0.25;
value(1, 2) = avgRounds;
value(1, 3) = stdRound;
value(1, 4) = avgGameTime;
value(1, 5) = stdGameTime;
value(1, 6) = avgFirstTouch;
value(1, 7) = stdFirstTouch;

fileName = '.\results\L13\Data_N9_Seed1000_Heu0.5_L13.0_Games5.csv';
[avgRounds,stdRound, avgGameTime, stdGameTime, avgFirstTouch, stdFirstTouch] = getData(fileName);

value(2, 1) = 0.50;
value(2, 2) = avgRounds;
value(2, 3) = stdRound;
value(2, 4) = avgGameTime;
value(2, 5) = stdGameTime;
value(2, 6) = avgFirstTouch;
value(2, 7) = stdFirstTouch;

fileName = '.\results\L13\Data_N9_Seed1000_Heu0.75_L13.0_Games5.csv';
[avgRounds,stdRound, avgGameTime, stdGameTime, avgFirstTouch, stdFirstTouch] = getData(fileName);

value(3, 1) = 0.75;
value(3, 2) = avgRounds;
value(3, 3) = stdRound;
value(3, 4) = avgGameTime;
value(3, 5) = stdGameTime;
value(3, 6) = avgFirstTouch;
value(3, 7) = stdFirstTouch;

fileName = '.\results\L13\Data_N9_Seed1000_Heu1.0_L13.0_Games5.csv';
[avgRounds,stdRound, avgGameTime, stdGameTime, avgFirstTouch, stdFirstTouch] = getData(fileName);

value(4, 1) = 1;
value(4, 2) = avgRounds;
value(4, 3) = stdRound;
value(4, 4) = avgGameTime;
value(4, 5) = stdGameTime;
value(4, 6) = avgFirstTouch;
value(4, 7) = stdFirstTouch;


figure(1)
plot(value(:,1),value(:,2),'o','Color','b','LineWidth',2);
hold on

p1 = errorbar(value(:,1), value(:,2),value(:,3));
set(p1, "linestyle", "none");
hold off

figure(2)
plot(value(:,1),value(:,4),'o','Color','b','LineWidth',2);
hold on

p1 = errorbar(value(:,1), value(:,4),value(:,5));
set(p1, "linestyle", "none");
hold off

figure(3)
plot(value(:,1),value(:,6),'o','Color','b','LineWidth',2);
hold on

p1 = errorbar(value(:,1), value(:,6),value(:,7));
set(p1, "linestyle", "none");
hold off



#plot(minOverlap(2,4),minOverlap(2,3),'o','Color','k','LineWidth',2);
#p2 = errorbar(heu2Data(:,1), heu2Data(:,8),heu2Data(:,9));
#set(p2, "linestyle", "none");

#plot(minOverlap(3,4),minOverlap(3,3),'o','Color','r','LineWidth',2);
#p3 = errorbar(heu4Data(:,1), heu4Data(:,8),heu4Data(:,9));
#set(p3, "linestyle", "none");

#plot(minOverlap(4,4),minOverlap(4,3),'o','Color','c','LineWidth',2);
#p4 = errorbar(heu5Data(:,1), heu5Data(:,9),heu5Data(:,9));
#set(p4, "linestyle", "none");

hold off


