package ar.edu.itba.ss;

import javax.swing.*;
import java.awt.geom.Point2D;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

class Configuration {
	private final static String projectDir = System.getProperty("user.dir");
	final static String outputDir = projectDir + "/results/";

	private Properties properties;
	private InputStream inStream = null;

	private static Configuration instance;

	/**
	 * Seed
	 */
	private long seed;

	private double dt;

	private double W;

	private double L;

	private int goodManCount;

	private int goodManHeuristic;

	private double goodManHeuristicParameter;

	private double goodManMaxVelocity;

	private double goodManMinVelocity;

	private int badManHeuristic;

	private double badManHeuristicParameter;
	
	private double badManMaxRadio;

	private double playersRadio;

	private double badManLineDistance;

	private double touchProbability;

	private double roundTime;

	private int games;

	private Configuration(){
		properties = new Properties();
		try {
			String configInfoDir = projectDir + "/resources/config.properties";
			inStream = new FileInputStream(configInfoDir);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		loadConfiguration();
	}

	static Configuration getInstance(){
		if (instance == null){
			instance = new Configuration();
		}
		return instance;
	}

	private void loadConfiguration() {
		try {
			properties.load(inStream);
		} catch (IOException e){
			e.printStackTrace();
		}

		seed = Long.parseLong(properties.getProperty("seed"));

		dt = Double.parseDouble(properties.getProperty("dt"));
		W = Double.parseDouble(properties.getProperty("W"));
		L = Double.parseDouble(properties.getProperty("L"));

		goodManCount = Integer.parseInt(properties.getProperty("goodMan.count"));
		goodManHeuristic = Integer.parseInt(properties.getProperty("goodMan.heuristic"));
		goodManHeuristicParameter = Double.parseDouble(properties.getProperty("goodMan.heuristic.parameter"));
		goodManMaxVelocity = Double.parseDouble(properties.getProperty("goodMan.maxVelocity"));
		goodManMinVelocity = Double.parseDouble(properties.getProperty("goodMan.minVelocity"));

		badManHeuristic = Integer.parseInt(properties.getProperty("badMan.heuristic"));
		badManHeuristicParameter = Double.parseDouble(properties.getProperty("badMan.heuristic.parameter"));
		badManMaxRadio = Double.parseDouble(properties.getProperty("badMan.maxRadius"));

		playersRadio = Double.parseDouble(properties.getProperty("players.radio"));

		badManLineDistance = Double.parseDouble(properties.getProperty("badMan.line.distance"));
		touchProbability = Double.parseDouble(properties.getProperty("touch.probability"));
		roundTime = Double.parseDouble(properties.getProperty("roundTime"));
		games = Integer.parseInt(properties.getProperty("games"));
	}

	public long getSeed() {
		return seed;
	}

	public double getW() {
		return W;
	}

	public double getL() {
		return L;
	}

	public double getDt() {
		return dt;
	}

	public int getGoodManCount() {
		return goodManCount;
	}

	public int getGoodManHeuristic() {
		return goodManHeuristic;
	}

	public double getGoodManHeuristicParameter() {
		return goodManHeuristicParameter;
	}

	public double getGoodManMaxVelocity() {
		return goodManMaxVelocity;
	}

	public double getGoodManMinVelocity() {
		return goodManMinVelocity;
	}

	public int getBadManHeuristic() {
		return badManHeuristic;
	}

	public double getBadManHeuristicParameter() {
		return badManHeuristicParameter;
	}
	
	public double badManMaxRadio() {
		return badManMaxRadio;
	}

	public double getPlayersRadio() {
		return playersRadio;
	}

	public double getBadManLineDistance() {
		return badManLineDistance;
	}

	public double getTouchProbability() {
		return touchProbability;
	}

	public double getRoundTime() {
		return roundTime;
	}

	public int getGames() {
		return games;
	}

	@Override
	public String toString() {
		return "Configuration{" +
				"seed=" + seed +
				", dt=" + dt +
				", W=" + W +
				", L=" + L +
				", goodManCount=" + goodManCount +
				", goodManHeuristic=" + goodManHeuristic +
				", goodManHeuristicParameter=" + goodManHeuristicParameter +
				", goodManMaxVelocity=" + goodManMaxVelocity +
				", goodManMinVelocity=" + goodManMinVelocity +
				", badManHeuristic=" + badManHeuristic +
				", badManHeuristicParameter=" + badManHeuristicParameter +
				", playersRadio=" + playersRadio +
				", badManLineDistance=" + badManLineDistance +
				", touchProbability=" + touchProbability +
				", roundTime=" + roundTime +
				'}';
	}
}