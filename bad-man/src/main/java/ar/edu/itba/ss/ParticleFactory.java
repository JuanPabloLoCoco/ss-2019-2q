package ar.edu.itba.ss;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class ParticleFactory {
    private static ParticleFactory instance;

    public static ParticleFactory getInstance() {
        if (instance == null) {
            instance = new ParticleFactory();
        }
        return instance;
    }

    public List<Particle> generatePlayers() {
        List<Particle> particleList = new ArrayList<>();

        int goodManCount = Configuration.getInstance().getGoodManCount();
        double radius = Configuration.getInstance().getPlayersRadio();
        double minVelocity = Configuration.getInstance().getGoodManMinVelocity();

        double badManMaxRadius = Configuration.getInstance().badManMaxRadio();

        double L = Configuration.getInstance().getL();
        double W = Configuration.getInstance().getW();

        // Generate Bad Man
        Player badMan = new Player(radius, badManMaxRadius);
        badMan.setId();
        badMan.setProperties(W / 2, L / 2, 0, 0);
        badMan.setBadMan();
        badMan.generateMaxVelocity();
        double velocityModule = MyRandom.getInstance().nextDouble(minVelocity, badMan.getMaxVelocity());
        badMan.setVelocityModule(velocityModule);
        particleList.add(badMan);

        // Generate other Particles;
        List<Particle> goodMen = new ArrayList<>();
        double distance = (L * 1.0) / goodManCount;

        for (int i = 0; i < goodManCount; i++){
            Player newPlayer = new Player(radius, badManMaxRadius);
            newPlayer.generateMaxVelocity();
            double velMod = MyRandom.getInstance().nextDouble(minVelocity, newPlayer.getMaxVelocity());
            newPlayer.setRadius(radius);
            newPlayer.setVelocityModule(velMod);
            newPlayer.setProperties(0, MyRandom.getInstance().nextDouble(i * distance + radius, (i + 1) * (distance * 1.0) - radius), 0, 0);
            newPlayer.setId();
            goodMen.add(newPlayer);
        }

        particleList.addAll(goodMen);
        return particleList;
    }


	private List<Particle> generateWallParticles () {
        List<Particle> particleList = new ArrayList<>();
        double heigth = Configuration.getInstance().getL();
        double width = Configuration.getInstance().getW();
        double wallRadio =  Configuration.getInstance().getPlayersRadio()/20.0;


        int count = (int)((width)/wallRadio) + 1;

        for (int i = 0; i <= count; i++){
            Particle p1 = new Player(wallRadio);
            Particle p2 = new Player(wallRadio);
            p1.setProperties(i * wallRadio,0 , 0,0 );
            p2.setProperties(i * wallRadio, heigth, 0, 0);
            p1.setId();
            p2.setId();
            particleList.add(p1);
            particleList.add(p2);
        }

        int heigthCount = (int)(heigth/wallRadio);
        for (int i = 0; i < heigthCount; i++) {
            Particle p1 = new Player(wallRadio);
            Particle p2 = new Player(wallRadio);
            Particle p3 = new Player(wallRadio);
            p1.setProperties(0, i * wallRadio, 0, 0);
            p2.setProperties(width, i * wallRadio, 0, 0);
            p3.setProperties(width/2.0, i * wallRadio, 0, 0);
            p1.setId();
            p2.setId();
            p3.setId();
            particleList.add(p1);
            particleList.add(p2);
            particleList.add(p3);
        }

        return particleList;
    }

    public void generateWall(){
        List<Particle> particleList = generateWallParticles();
        double heigth = Configuration.getInstance().getL();
        double width = Configuration.getInstance().getW();

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Wall_Count")
                .append(particleList.size())
                .append("Height")
                .append(heigth)
                .append("witdh")
                .append(width);

        generateTestParticles(particleList, stringBuilder.toString());
    }

    private void generateTestParticles(List<Particle> particleList, String fileName) {
        List<Particle> toPrint = new ArrayList<>();
        toPrint.addAll(particleList);
        // toPrint.addAll(generateParticleBox());
        String dataToPrint = generateParticlesWallData(toPrint);
        StringBuilder builder = new StringBuilder()
                .append(Configuration.outputDir)
                .append(fileName)
                .append(".txt");
        String filePath = builder.toString();
        Path path = Paths.get(filePath);

        StringBuilder timeBuilder = new StringBuilder()
                .append(toPrint.size())
                .append("\r\n\r\n")
                .append(dataToPrint);
        try {
            if (!Files.exists(path)){
                Files.write(path, timeBuilder.toString().getBytes(), StandardOpenOption.CREATE);
            } else {

            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    private List<Particle> generateParticleBox() {
        double L = Configuration.getInstance().getL();
        double W = Configuration.getInstance().getW();

        Player p1 = new Player(0.01);
        p1.setProperties(0, 0, 0, 0);
        p1.setId();
        Player p2 = new Player(0.01);
        p2.setProperties(W, 0, 0, 0);
        p2.setId();
        Player p3 = new Player(0.01);
        p3.setProperties(0, L, 0, 0);
        p3.setId();
        Player p4 = new Player(0.01);
        p4.setProperties(W, L, 0, 0);
        p4.setId();
        List<Particle> particleList = new ArrayList<>();
        particleList.add(p1);
        particleList.add(p2);
        particleList.add(p3);
        particleList.add(p4);
        return particleList;
    }

    private String generateParticlesWallData(List<Particle> particleList){
        StringBuilder builder = new StringBuilder();
        for(Particle current: particleList){
            builder.append(current.getId())
                    .append(" ")
                    .append(current.getX())
                    .append(" ")
                    .append(current.getY())
                    .append(" ")
                    .append(current.getRadius())
                    .append(" ")
                    .append(((Player)current).getColor())
                    .append("\r\n");
        }
        return builder.toString();
    }

    /*
    public void generateParticleFile (List<Particle> particleList, String fileName){
        String dataToPrint = generateParticlesWallData(particleList);
        StringBuilder builder = new StringBuilder()
                .append(Configuration.outputDir)
                .append(fileName)
                .append(".txt");
        String filePath = builder.toString();
        Path path = Paths.get(filePath);

        StringBuilder timeBuilder = new StringBuilder()
                .append(particleList.size())
                .append("\r\n\r\n")
                .append(dataToPrint);
        try {
            if (!Files.exists(path)){
                Files.write(path, timeBuilder.toString().getBytes(), StandardOpenOption.CREATE);
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public void generateLJWall() {
        List<Particle> particleList = generateParticles();
        //List<Particle> particleList = generateWallParticles();

        String dataToPrint = generateParticlesWallData(particleList);
        StringBuilder builder = new StringBuilder()
                .append(Configuration.outputDir)
                .append("Wall_")
                //.append(Configuration.getInstance().getD())
                .append(".txt");
        String filePath =  builder.toString();
        Path path = Paths.get(filePath);

        StringBuilder timeBuilder = new StringBuilder()
                .append(particleList.size())
                .append("\r\n\r\n")
                .append(dataToPrint);
        try {
            if (!Files.exists(path)){
                Files.write(path, timeBuilder.toString().getBytes(), StandardOpenOption.CREATE);
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }

     */

}
