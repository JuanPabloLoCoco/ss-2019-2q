package ar.edu.itba.ss;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

public class Algorithm {
	/** Percentage of W after which the good man is considered safe enough from bad to be considered on target */
	private static double TARGET_FACTOR = 0.75d;
	private Heuristic goodManHeuristicSelected, badManHeuristicSelected;
	// private final static Point2D GOAL = Configuration.getInstance().getGoal();
	/**
	 * For no collision between particles, a big enough time is used meaning it will not affect the rest of the logic.
	 */
	private final static double NO_COLLISION = Double.MAX_VALUE;
	private double totalTime = 0;
	private int gamesCount = 0;
	private int round = 0;
	// private Particle movingPedestrian;
	private List<Particle> particles;
	private double target;
	private double deltaT;
	private double totalOverlapping = 0;

	/** Collision queues for each particle. */
	private List<PriorityQueue<Collision>> cQueues = new LinkedList<>();
	/** Player Index corresponding to Player Id */
	private Map<Integer,Integer> idIndexRel = new HashMap<Integer,Integer>();

	public Algorithm(List<Particle> particles) {
		this.particles = particles;
		/** target excludes each particle radius/2 (to be considered in each case) */
		target = TARGET_FACTOR * Configuration.getInstance().getW();

		goodManHeuristicSelected = Heuristic.getGoodManHeuristic();

		int i = particles.size();
		while (i > 0) {
			idIndexRel.put(particles.get(i-1).getId(),i-1);
			cQueues.add(new PriorityQueue<Collision>());
			i--;
		}
	}

	private void printToFile(List<Particle> goodMen, List<Particle> badMen) {
		String particlesData = generateParticlesData(goodMen, badMen, totalTime);
		generateParticlesOutput(particlesData);
		String particleSimulation = generateParticlesSimulationData(particles);
		generateParticleSimulationOutput(particleSimulation, totalTime);
		String particleSimulationVelocity = generateParticlesSimulationDataWithVelocity(particles);
		generateParticleSimulationOutputWithVelocity(particleSimulationVelocity, totalTime);
	}

    private void dt1Routine(List<Particle> goodMen, List<Particle> badMen, List<Particle> inTarget, List<Particle> converted){
    	/** New angles based heuristic are stored in a map before being set so they are replaced all at once */
    	Map<Particle,Double> newAngles = new HashMap<Particle,Double>();
    	
    	updateGoodMenDirection(goodMen,badMen,newAngles);
		updateBadMenDirection(badMen,goodMen,newAngles);

		/** Update all particles with new angles */
		for (Particle particle : particles) {
			Double current = newAngles.get(particle);
			if (current != null) {
				particle.setAngle(newAngles.get(particle));
			}
		}
		
        /*for (Particle p1 : particles){
        	if (((Player) p1).isMoveable()) {
        		((Player) p1).acceleration();
        	}
        }*/
		
        badMenContractileFix(badMen);
        
        wallFix();
    	
        for (Particle p1 : particles){
			//Player p = (Player) p1;
			//System.out.println("V:" + p.getVelocityModule() + ", max v:" + p.getMaxVelocity() );
        	p1.advance(deltaT);
        }
        
        List<Particle> toConverted = new LinkedList<>(), toInTarget = new LinkedList<>();
        
        for (Particle goodMan : goodMen) {
        	boolean isCaught = false;
    		for (Particle badMan : badMen) {
    			if (((Player) goodMan).isCaught(badMan)) {
    				toConverted.add(goodMan);
    				goodMan.setVelocityModule(0);
            		isCaught = true;
    				break;
    			}
    		}
        	/** If good man passes bad man line it advances to next round */
        	if (!isCaught && goodMan.getX() > targetClearPosition()) {
        		toInTarget.add(goodMan);
        		((Player) goodMan).setFinished();
        	}
        }
        
        converted.addAll(toConverted);
        goodMen.removeAll(toConverted);
        inTarget.addAll(toInTarget);
        goodMen.removeAll(toInTarget);
        
        printToFile(goodMen, badMen);
	}
    
    private double targetClearPosition() {
    	// return target + goodMan.getRadius();
    	return target;
    }
    
    private void updateGoodMenDirection(List<Particle> goodMen, List<Particle> badMen, Map<Particle, Double> newAngles) {
    	Player.updateToGoal(goodMen);
    	
    	/** Generate future collisions for good men */
    	generateCollisionTimeQueue(goodMen,badMen);
    	
		for (Particle goodMan : goodMen) {
			PriorityQueue<Collision> cQueue = getCQueue(goodMan);
			
			if (!cQueue.isEmpty()) {
				newAngles.put(goodMan,((Player) goodMan).updatedAngle(badMen,cQueue,goodManHeuristicSelected));
			}
		}
    }

    /** Routine used to calculate bad men ideal direction */
    private void updateBadMenDirection(List<Particle> badMen, List<Particle> goodMen, Map<Particle, Double> newAngles) {
		for(Particle badman: badMen){
			((Player) badman).setPreviousAngle();
		}

    	/** Generate future collisions for bad men */
    	generateCollisionTimeQueue(badMen,goodMen);
    	
    	List<Particle> goodMenCopy = new LinkedList<>(goodMen);
		goodMenCopy.removeIf(c -> c.getX()+c.getRadius() > badMen.get(0).getX());

    	/** Use indexes to distribute bad men from bottom to top. */
		int b = 0, t = badMen.size() - 1;
		boolean topNext = false;
		
		while (b <= t) {
			Particle bottom = badMen.get(b);
			Particle top = badMen.get(t);
			Particle bottomClosest = bottom.getClosestParticle(goodMenCopy);
			Particle topClosest = top.getClosestParticle(goodMenCopy);
			if(bottomClosest != null && topClosest != null){
				topNext = (top.getDistance(topClosest) < bottom.getDistance(bottomClosest));
			}else {
				topNext = !topNext;
			}
			Particle current, closest;
			
			if (topNext) {
				current = badMen.get(t--);
			} else {
				current = badMen.get(b++);
			}
			
			closest = current.getClosestParticle(goodMenCopy);
			
			if (closest == null) {return;}
			
			if ((current.getY() < closest.getY() && current.getAngle() < 0) ||
					(current.getY() > closest.getY() && current.getAngle() > 0)) {
				newAngles.put(current,current.getReversedAngle());
			}

			goodMenCopy.remove(closest);
			
			/** If last bad man was on top the next one is at the bottom for a more efficient assignment of good men */
			//topNext = !topNext;
		}
	}

	private void badMenContractileFix(List<Particle> badMen) {
        Particle prev, current = badMen.get(0);
        
        /** Each neighbor couple is fixed starting with elements 0 and 1 and finishing with the elements size-2 and size-1 */
        for (Particle badMan : badMen.subList(1, badMen.size())) {
        	prev = current;
        	current = badMan;
        	//System.out.println("vel: " + current.getY());
        	((Player) prev).contractileFix(current);
        	((Player) current).contractileFix(prev);
        }
	}

	/** Limit is reduced to avoid going out of bounds */
	private void wallFix() {
		for (Particle particle : particles) {
			((Player) particle).wallFix();
		}
	}
	
	public void run() {
        double dt = Configuration.getInstance().getDt();
		deltaT = dt;
		List<Particle> goodMen = new LinkedList<>(), badMen = new LinkedList<>(),
				inTarget = new LinkedList<>(), converted = new LinkedList<>();

		int games = Configuration.getInstance().getGames();
		
		initialLists(goodMen,badMen);

		for (; gamesCount < games; gamesCount++) {
			round = 0;
			totalOverlapping = 0;
			totalTime = 0;

			printToFile(goodMen, badMen);
			
			while (notFinishGame(goodMen)) {
				initialBadMenAngle(badMen);
				
				while (notFinishRound(goodMen,inTarget)){
					totalTime += dt;
					System.out.println("Game = " + gamesCount + "- totalTime =" + totalTime);
					dt1Routine(goodMen,badMen,inTarget,converted);
					for(Particle badman: badMen) {
						System.out.println("bad max vel: " + ((Player) badman).getMaxVelocity() + ", velocidad: " + badman.getVelocityModule());
					}
					for(Particle goodman: goodMen) {
						System.out.println("good max vel: " + ((Player) goodman).getMaxVelocity() + ", velocidad: " + goodman.getVelocityModule());
					}
				}
				resetRound(goodMen,badMen,inTarget,converted);
				round++;
			}
			
			setInitialPositions(goodMen,badMen);
		}

		System.out.println("Winner = " + getWinner(goodMen,badMen));
	}
    
    private void initialBadMenAngle(List<Particle> badMen) {
		for (Particle badMan : badMen) {
			badMan.setAngle(Math.PI/2);
		}
	}

	private void initialLists(List<Particle> goodMen, List<Particle> badMen) {
		for (Particle particle : particles) {
			if (((Player) particle).isBadMan()) {
				badMen.add(particle);
			} else {
				goodMen.add(particle);
			}
		}
	}

	private void setInitialPositions(List<Particle> goodMen, List<Particle> badMen) {
        double L = Configuration.getInstance().getL();
        double W = Configuration.getInstance().getW();
        double radius = Configuration.getInstance().getPlayersRadio();
        
        Player.clearAll(goodMen,badMen);
        
        Player firstBad = (Player) goodMen.remove(0);
        firstBad.setBadMan();
        badMen.add(firstBad);
        firstBad.setProperties(W / 2, L / 2, 0, 0);

        double distance = (L * 1.0) / goodMen.size();

        for (int i = 0; i < goodMen.size(); i++){
        	goodMen.get(i).setProperties(0, MyRandom.getInstance().nextDouble(i * distance + radius, (i + 1) * (distance * 1.0) - radius), 0, 0);
        }
        
        //goodMen.addAll(badMen);
	}

    /**
     * goodMen and BadMen will be placed for the new round with an increasing y according to the index in their lists.
     * This means for example that goodMen.get(0).y < goodMen.get(1).y and badMen.get(0).y < badMen.get(1).y
     * This is done this way to facilitate bad men heuristic.
     * */ 
   private void resetRound(List<Particle> goodMen, List<Particle> badMen, List<Particle> inTarget, List<Particle> converted) {
	   // Before setting new positions, converted are moved to badMan and inTarget are moved to goodMen for next round.
	   menRoundReset(goodMen,badMen,inTarget,converted);

       double L = Configuration.getInstance().getL();
       double radius = Configuration.getInstance().getPlayersRadio();
       double distance = (L * 1.0) / goodMen.size();

       for (int i = 0; i < goodMen.size(); i++){
       	goodMen.get(i).setProperties(0, MyRandom.getInstance().nextDouble(i * distance + radius, (i + 1) * (distance * 1.0) - radius), 0, 0);
       }
	   
	   /** Bad men are distributed equidistantly */
	   double yCurrent = 0;
       double W = Configuration.getInstance().getW();
	   double badMenDistance = Configuration.getInstance().getL()/(badMen.size()+1);
	   for (int i = 0; i < badMen.size(); i++) {
		   yCurrent += badMenDistance;
		   badMen.get(i).setProperties(W / 2, yCurrent, 0, 0);
		   double velocityModule = MyRandom.getInstance().nextDouble(Configuration.getInstance().getGoodManMinVelocity(), ((Player) badMen.get(i)).getMaxVelocity());
		   badMen.get(i).setVelocityModule(velocityModule);
	   }
    }
    
    private void menRoundReset(List<Particle> goodMen, List<Particle> badMen, List<Particle> inTarget, List<Particle> converted) {
 		goodMen.addAll(inTarget);
 		inTarget.clear();
 		badMen.addAll(converted);
 		Player.turnAll(converted);
     }
    
	private Particle getWinner(List<Particle> goodMen, List<Particle> badMen) {
    	if (goodMen.size() > 0) {
    		return goodMen.get(0);
    	}
    	/** If there is no good man left, the winner is the last that turned bad */
    	return badMen.get(badMen.size()-1);
    }

	private boolean notFinishRound(List<Particle> goodMen, List<Particle> inTarget) {
		/** Round is not finished if there are good men left to cross the target.
		 * If there is only 1 and there are no other good men for next round, this is the winner.
		 * If not, round continues.*/
		return goodMen.size() > 1 || (goodMen.size() == 1 && inTarget.size() > 0);
	}

	private boolean notFinishGame(List<Particle> goodMen) {
		return goodMen.size() > 1;
	}

	public void generateCollisionTimeQueue(List<Particle> particles, List<Particle> others) {
		for (Particle particle : particles) {
			generateCollisionTimeQueue(getCQueue(particle),particle,others);
		}
	}
	
	private PriorityQueue<Collision> getCQueue(Particle particle) {
		int queueIndex = idIndexRel.get(particle.getId());
		return cQueues.get(queueIndex);
	}

	public void generateCollisionTimeQueue(PriorityQueue<Collision> cQueue, Particle particle, List<Particle> others) {
		cQueue.clear();
		//double L = Configuration.getInstance().getL();
		for (Particle other : others) {
			generateCollisionTimeQueue(cQueue, particle, other, other.getY());
			//generateCollisionTimeQueue(cQueue, particle, other, other.getY() - L); // Top periodic condition
			//generateCollisionTimeQueue(cQueue, particle, other, other.getY() + L); // Bottom periodic condition
		}
	}

	private void generateCollisionTimeQueue(PriorityQueue<Collision> cQueue, Particle particle, Particle particle2, double particle2Ypos) {
		double currentTime = tCollisionParticles(particle,particle2, particle2Ypos);

		if (currentTime != NO_COLLISION) {
			cQueue.add(new Collision(currentTime,particle2));
		}
	}

	public double tCollisionParticles(Particle particle, Particle particle2, double particle2Ypos) {
		double deltaRx  = particle2.getX()    - particle.getX();
		double deltaRy  = particle2Ypos       - particle.getY();
		double deltaVx  = particle2.getVelX() - particle.getVelX();
		double deltaVy  = particle2.getVelY() - particle.getVelY();
		/* dv.dr = (dvx)(dx) + (dvy)(dy) */
		double deltaVmR = deltaVx*deltaRx     + deltaVy*deltaRy;

		if ( deltaVmR >= 0 ) {
			return NO_COLLISION;
		}

		/* sigma = ri + rj */
		double sigma  = particle.getRadius() + particle2.getRadius();
		double deltaRmR = deltaRx*deltaRx     + deltaRy*deltaRy;
		double deltaVmV = deltaVx*deltaVx     + deltaVy*deltaVy;
		/* d =( dv.dr)^2 - (dv.dv)(dr.dr-sigma^2) */
		double d = deltaVmR*deltaVmR - deltaVmV*(deltaRmR - sigma*sigma);

		if (d < 0) {
			return NO_COLLISION;
		}

		/* tc = - dv . dr + sqrt(d) / dv . dv */
		double t = - ((deltaVmR + Math.sqrt(d)) / deltaVmV);
		return t;
	}

	/** Prints to File */
    private void generateParticleSimulationOutput(String particleData, double time) {
        String filePath = Configuration.outputDir +
                "Simu_N" +
				(particles.size())+
                "_Seed" +
                Configuration.getInstance().getSeed() +
				".txt";
		Path path = Paths.get(filePath);

        String timeBuilder = (particles.size()) +
                "\r\n//" +
                time +
                "_time,id,x,y,radio,r,g,b" +
                "\r\n";
        StringBuilder toPrint = new StringBuilder()
				.append(timeBuilder)
				.append(particleData);

		try {
			if (!Files.exists(path)){
				Files.write(path, toPrint.toString().getBytes(), StandardOpenOption.CREATE);
			} else {
				Files.write(path, toPrint.toString().getBytes(), StandardOpenOption.APPEND);
			}
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	private void generateParticleSimulationOutputWithVelocity(String particleData, double time) {
		String filePath = Configuration.outputDir +
				"SimuVel_N" +
				(particles.size())+
				"_Seed" +
				Configuration.getInstance().getSeed() +
				".txt";
		Path path = Paths.get(filePath);

		String timeBuilder = (particles.size()) +
				"\r\n//" +
				time +
				"_time,id,x,y,vx,vy,radio,r,g,b" +
				"\r\n";
		StringBuilder toPrint = new StringBuilder()
				.append(timeBuilder)
				.append(particleData);

		try {
			if (!Files.exists(path)){
				Files.write(path, toPrint.toString().getBytes(), StandardOpenOption.CREATE);
			} else {
				Files.write(path, toPrint.toString().getBytes(), StandardOpenOption.APPEND);
			}
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	private void generateParticlesOutput(String particleData) {
	String filePath = Configuration.outputDir +
                "Data_N" +
                particles.size() +
                "_Seed" +
                Configuration.getInstance().getSeed() +
				"_Heu" +
				Configuration.getInstance().getGoodManHeuristicParameter() +
				".csv";
		Path path = Paths.get(filePath);

		try {
			if (!Files.exists(path)){
				Files.write(path, getParticleHeader().getBytes(), StandardOpenOption.CREATE);
			}
			Files.write(path, particleData.getBytes(), StandardOpenOption.APPEND);
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	/** Particles Simulation Data */
	private String generateParticlesSimulationData(List<Particle> particleList){
		StringBuilder builder = new StringBuilder();
		int lastId = 0;
		for (Particle current: particleList){
			builder.append(current.getId())
					.append(" ")
					.append(current.getX())
					.append(" ")
					.append(current.getY())
					.append(" ")
					.append(current.getRadius())
					.append(" ")
					.append(((Player) current).getColor())
					.append("\r\n");
		}

		return builder.toString();
	}

	private String generateParticlesSimulationDataWithVelocity(List<Particle> particleList){
		StringBuilder builder = new StringBuilder();
		for (Particle current: particleList){
			builder.append(current.getId())
					.append(" ")
					.append(current.getX())
					.append(" ")
					.append(current.getY())
					.append(" ")
					.append(current.getVelX())
					.append(" ")
					.append(current.getVelY())
					.append(" ")
					.append(current.getRadius())
					.append(" ")
					.append(((Player)current).getColor())
					.append("\r\n");
		}
		return builder.toString();
	}

	/** Headers */
	private String getParticleHeader () {
		return "GameCount,round,time,goodMenCount,badMenCount\r\n";
	}
	/**  Particles Data */
	private String generateParticlesData(List<Particle> goodMen,List<Particle> badMen, double time){
		StringBuilder builder = new StringBuilder();
		builder.append(gamesCount)
				.append(",")
				.append(round)
				.append(",")
				.append(time)
				.append(",")
				.append(goodMen.size())
				.append(",")
				.append(badMen.size())
				.append("\r\n");
		return builder.toString();
	}


}