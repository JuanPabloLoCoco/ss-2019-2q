package ar.edu.itba.ss;

import java.io.IOException;
import java.util.List;

public class App {
    public static void main( String[] args ) throws IOException {
        System.out.println("It´s running Bad man");
        Long seed = Configuration.getInstance().getSeed();
        MyRandom.setSeed(seed);

        // Method to create wall
        ParticleFactory.getInstance().generateWall();

        List<Particle> particleList = ParticleFactory.getInstance().generatePlayers();
        //ParticleFactory.getInstance().generateTestParticles(particleList, "Test");

        System.out.println(Configuration.getInstance().toString());


        // List<Particle> particleList = ParticleFactory.getInstance().generateParticles();
        Algorithm algorithm = new Algorithm(particleList);

        algorithm.run();


    }
}