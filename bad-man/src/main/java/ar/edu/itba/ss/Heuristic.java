package ar.edu.itba.ss;

import java.util.List;
import java.util.PriorityQueue;

public abstract class Heuristic {
    protected abstract double getAngle(Particle movingPedestrian, List<Particle> particles, PriorityQueue<Collision> cQueue);

    public static Heuristic getGoodManHeuristic() {
    	int heuristicGoodType = Configuration.getInstance().getGoodManHeuristic();
    	
		if (heuristicGoodType == 1) {
			return Heuristic.getHeuristic1();
		} else if (heuristicGoodType == 2) {
			return Heuristic.getHeuristic2();
		} else if (heuristicGoodType == 3) {
			return Heuristic.getHeuristic3();
		}
		return null;
    }

    public static Heuristic getBadManHeuristic() {
    	//int heuristicBadType = Configuration.getInstance().getBadManHeuristic();
    	
		/**if (heuristicBadTye == 1) {
			badManHeuristicSelected = Heuristic.getHeuristic1();
		} else{
			badManHeuristicSelected = null;
		}*/
    	
		return null;
    }
    
    /** Exponential heuristic used for good men (without position considerations) */
    public static Heuristic getHeuristic1() {
        return new Heuristic() {
            @Override
            protected double getAngle(Particle movingPedestrian, List<Particle> particles, PriorityQueue<Collision> cQueue) {
            	Collision nextCollision = cQueue.peek();
            	double currentAngle = movingPedestrian.getAngle();
            	double sign = addSign(currentAngle,nextCollision.getParticle().getAngle());
            	double A = Configuration.getInstance().getGoodManHeuristicParameter();
                return currentAngle + sign*(A / Math.exp(nextCollision.getTime()));
            }
        };
    }
    
    /** Exponential heuristic used for good men (with positions considerations) */
    public static Heuristic getHeuristic2() {
        return new Heuristic() {
            @Override
            protected double getAngle(Particle movingPedestrian, List<Particle> particles, PriorityQueue<Collision> cQueue) {
            	Collision nextCollision = cQueue.peek();
            	double currentAngle = movingPedestrian.getAngle();
            	double sign = addSign(currentAngle,nextCollision.getParticle().getAngle(),movingPedestrian.getY(),nextCollision.getParticle().getY());
            	double A = Configuration.getInstance().getGoodManHeuristicParameter();
                return currentAngle + sign*(A / Math.exp(nextCollision.getTime()));
            }
        };
    }

	public static Heuristic getHeuristic3() {
		return new Heuristic() {
			@Override
			protected double getAngle(Particle movingPedestrian, List<Particle> particles, PriorityQueue<Collision> cQueue) {
				Collision nextCollision = cQueue.peek();
				double currentAngle = movingPedestrian.getAngle();
				double sign = addSign(currentAngle,nextCollision.getParticle().getAngle(),movingPedestrian.getY(),nextCollision.getParticle().getY());
				double A = Configuration.getInstance().getGoodManHeuristicParameter();
				double time = nextCollision.getTime();
				double e = 0.2*time*time + 0.3*time;
				return currentAngle + sign*(A / Math.exp(e));
			}
		};
	}
    

    private static double addSign(double vel1, double vel2) {
    	if (sameDirection(vel1,vel2)) {
    		return -1 * sign(vel1);
    	}
    	return sign(vel1);
    }
    

    private static double addSign(double vel1, double vel2, double y1, double y2) {
    	if (sameDirection(vel1,vel2)) {
    		return -1 * sign(vel1);
    	}
    	return sign(vel1,y1,y2);
    }

    private static double sign(double value){
        return Math.signum(value)>=0?1.0:-1.0;
    }

    private static double sign(double vel1, double y1, double y2){
    	if (y1 > y2) {
    		return 1;
    	} else if (y1 < y2) {
    		return -1;
    	}
    	return Math.signum(vel1)>=0?1.0:-1.0;
    }
    
    private static boolean sameDirection(double angle1, double angle2) {
    	return (angle1 > 0 && angle2 > 0) || (angle1 < 0 && angle2 < 0);
    }
}