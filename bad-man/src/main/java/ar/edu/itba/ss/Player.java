package ar.edu.itba.ss;

import java.util.List;
import java.util.PriorityQueue;

public class Player extends Pedestrian {
	private final static double ACCELERATION_FACTOR = 1.7;
	private final static double ANGLE_FIX_TO_GOAL = 0.02;
	private final static double MIN_FACTOR = 0.7;
	private final static double MAX_VELOCITY = Configuration.getInstance().getGoodManMaxVelocity();
	private final static double MIN_VELOCITY = 0.2;
	private final static double LOWER_MAX_VELOCITY = 0.6;
	private double maxVelocity = MAX_VELOCITY;
	private State state = State.GOOD_MAN;
	private double previousAngle = 0;
	/** max radius is only used for bad men personal space */
	private double maxRadius;

	public Player(double radius) {
		super(radius);
	}

	public Player(double radius, double maxRadius) {
		this(radius);
		this.maxRadius = maxRadius;
	}

	public double getMaxVelocity() {
		return maxVelocity;
	}

	public void generateMaxVelocity() {
		//this.maxVelocity = MyRandom.getInstance().nextDouble(LOWER_MAX_VELOCITY, MAX_VELOCITY);
		this.maxVelocity = MAX_VELOCITY;
	}

	/** Goal is the vertical line behind the bad men */
    public void updateToGoal() {
        setAngle(0);
    }

    public void updateToGoal2() {
        setPreviousAngle();
        setAngle(goalAngle());
    }

	public void setPreviousAngle() {
		previousAngle = getAngle();
	}

	private double goalAngle() {
		double newAngle;
		if (getAngle() > 0) {
			newAngle = getAngle() - ANGLE_FIX_TO_GOAL;
			if (newAngle < 0) {
				newAngle = 0;
			}
		} else {
			newAngle = getAngle() + ANGLE_FIX_TO_GOAL;
			if (newAngle > 0) {
				newAngle = 0;
			}
		}
		return newAngle;
	}
	
	public static void updateToGoal(List<Particle> goodMen) {
		for (Particle goodMan : goodMen) {
			((Player) goodMan).updateToGoal();
		}
	}
	
	public void updateAngleCollision(Particle other) {
		setAngle(reverseAngle(angleBetween(other)));
	}
	
	/** Reverse angle in -pi : pi range. */
	public double reverseAngle(double angle) {
		if (angle > 0) return angle - Math.PI;
		return angle + Math.PI;
	}
	
	public double updatedAngle(List<Particle> particles, PriorityQueue<Collision> cQueue, Heuristic heuristic) {
		return heuristic.getAngle(this,particles,cQueue);
	}
	
	/** Acceleration for straight line Deceleration due to lateral movement for good men */
	public void acceleration() {
		double variation = Math.abs(getAngle()-previousAngle);
//		if(isBadMan()){
//			System.out.println("variation: " + variation + ", angle: " + getAngle() + ", prev: " + previousAngle);
//		}
		if (variation > Math.PI/2) {
			variation = Math.PI/2;
		}
		double factor = (((Math.PI/2)-variation)+MIN_FACTOR)/(Math.PI/2);
		
		double newVelocity = ACCELERATION_FACTOR*factor*getVelocityModule();
		
		if (newVelocity > maxVelocity) {
			newVelocity = maxVelocity;
		} else if (newVelocity < MIN_VELOCITY) {
			newVelocity = MIN_VELOCITY;
		}
		
		setVelocityModule(newVelocity);
	}
	
	/** Used for bad men only */
	public void contractileFix(Particle other) {
		Particle lower, higher;
		if (this.getY() > other.getY()) {
			higher = this; lower = other;
		} else {
			higher = other; lower = this;
		}
		/** If bad men go in opposite directions and closer to each other */
		if ((higher.getAngle() < 0 && lower.getAngle() > 0) || (higher.getAngle() > 0 && lower.getAngle() > 0 && higher.getDistance(lower) <= lower.getRadius() + higher.getRadius())) {
			double contractileVel = contractileVel(other);
			/** velocity is only fixed if higher than contractile velocity */
			if (contractileVel < this.getVelocityModule()) {
				this.setVelocityModule(contractileVel);
			}
		}
	}
	
	/** Used for bad men only */
	private double contractileVel(Particle other) {
		double r = this.getDistance(other) - other.getRadius();
		
		/** put r between contractile limits */
		if (r < this.getRadius()) {
			r = this.getRadius();
		} else if (r > this.maxRadius) {
			r = this.maxRadius;
		}
		
		return maxVelocity*(r-getRadius())/(maxRadius-getRadius());
	}

	public int getClosest(List<Particle> particles) {
		int closest = 0;
		
		for (int i = 1; i < particles.size(); i++) {
			if (getDistance(particles.get(i)) < getDistance(particles.get(closest))) {
				closest = i;
			}
		}
		return closest;
	}
	
	@Override
	public String getColor() {
		if (isBadMan()) {
			return super.getColor(0,1,0);
		} else if (isCoverted()) {
			return super.getColor(0.67,0.67,0.67);
		}
		return super.getColor(1,0,0);
	}

	public boolean isMoveable() {
		return isGoodMan() || isBadMan();
	}
	
	public boolean isGoodMan() {
		return state.equals(State.GOOD_MAN);
	}

	public boolean isBadMan() {
		return state.equals(State.BAD_MAN);
	}
	
	public boolean isCoverted() {
		return state.equals(State.CONVERTED);
	}

	public void setGoodMan() {
		state = State.GOOD_MAN;
	}

	public void setBadMan() {
		state = State.BAD_MAN;
	}
	
	private void setConverted() {
		state = State.CONVERTED;
	}
	
	public void setFinished() {
		state = State.GOOD_MAN_FINISHED;
	}

	public boolean isCaught(Particle badMan) {
		double overlap = getOverlap(badMan) ;
		double touchProbability = Configuration.getInstance().getTouchProbability();
		double overlapProbability = (touchProbability * overlap) / (badMan.getRadius() * 2.0);
		double randomNumber = MyRandom.getInstance().nextDouble(0, 1.0);
		if (randomNumber < overlapProbability) {
			setConverted();
			return true;
		}
		return false;
	}
	
	public static void turnAll(List<Particle> converted) {
		for (Particle particle : converted) {
			((Player) particle).setBadMan();
		}
		converted.clear();
	}
	
	public static void clearAll(List<Particle> goodMen, List<Particle> badMen) {
		for (Particle badMan : badMen) {
			goodMen.add(badMan);
			((Player) badMan).setGoodMan();
		}
		badMen.clear();
	}
	
	public void wallFix() {
		double l = Configuration.getInstance().getL();
		//double minDist = getY() - 0, maxDist = l - getY();
		//Particle wall = new Particle(0);
		if ((getY() < 0) || (getY() > l)) {
			if(isBadMan()){
				if(getY() < 0) {
					this.hitWall(2);
				} else {
					this.hitWall(-2);
				}
			} else {
				this.hitWall(4);
			}
		}
		//wall.setProperties(getX(),minDist < maxDist? 0:l ,0,0);
		
		//contractileFix(wall);
	}

	public String getState() {
		return state.toString();
	}
}