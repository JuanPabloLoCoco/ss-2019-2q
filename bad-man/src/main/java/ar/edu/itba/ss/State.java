package ar.edu.itba.ss;

public enum State {
	/** Good man waiting to complete round */ GOOD_MAN,
	/** Bad man */BAD_MAN,
	/** Good man converted to bad man for next round */ CONVERTED,
	/** Good Man with round completed */ GOOD_MAN_FINISHED;
}