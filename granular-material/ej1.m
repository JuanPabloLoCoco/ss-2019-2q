#Beverlo Analysis
fileName = '.\results\Diferent_holes\OutBoxList_N400_Seed1000_hole0.15dt1_1.0E-5.csv';
[qVal1, size1] = qaudal(fileName);

for q = 2 : size1
  aux1(q - 1, 1) = qVal1(q,1);
  aux1(q - 1, 2) = qVal1(q,2);
endfor
toPrint(1,1) = 0.15;
toPrint(1,2) = mean(aux1(:,2));
toPrint(1,3) = std(aux1(:,2)); 

fileName = '.\results\Diferent_holes\OutBoxList_N400_Seed1000_hole0.18dt1_1.0E-5.csv';
[qVal2, size2] = qaudal(fileName);
clear('aux1')
for q = 2 : size2
  aux1(q - 1, 1) = qVal2(q,1);
  aux1(q - 1, 2) = qVal2(q,2);
endfor
toPrint(2,1) = 0.18;
toPrint(2,2) = mean(aux1(:,2));
toPrint(2,3) = std(aux1(:,2));

fileName = '.\results\Diferent_holes\OutBoxList_N400_Seed1000_hole0.21dt1_1.0E-5.csv';
[qVal3, size3] = qaudal(fileName);
clear('aux1');
for q = 2 : size3
  aux1(q - 1, 1) = qVal3(q,1);
  aux1(q - 1, 2) = qVal3(q,2);
endfor
toPrint(3,1) = 0.21;
toPrint(3,2) = mean(aux1(:,2));
toPrint(3,3) = std(aux1(:,2));

fileName = '.\results\Diferent_holes\OutBoxList_N400_Seed1000_hole0.24dt1_1.0E-5.csv';
[qVal4, size4] = qaudal(fileName);
clear('aux1');
for q = 2 : size4;
  aux1(q - 1, 1) = qVal4(q,1);
  aux1(q - 1, 2) = qVal4(q,2);
endfor
toPrint(4,1) = 0.24;
toPrint(4,2) = mean(aux1(:,2));
toPrint(4,3) = std(aux1(:,2));

density = 0.21320;
b = (400/density) * sqrt(9.81);
r = 0.0116394;
index = 0;
minC = 1;
for c = 0:0.01:10;
  index = index + 1;
  sum = 0;
  for p = 1 : 4
    d = toPrint(p,1);
    sum = sum + (toPrint(p,2) - beverloo(c, r, d, b))^2;
  endfor 
  test(index, 1) = c;
  test(index, 2) = sum/4;
  if (test(minC,2) > test(index,2) )
    minC = index;
  endif
endfor

plot(test(:,1), test(:,2),'o')

display("Minimo")
min(test(minC,1))
min(test(:,2))

toPrint(1,4) = beverloo(test(minC,1),r,toPrint(1,1),b);
toPrint(2,4) = beverloo(test(minC,1),r,toPrint(2,1),b);
toPrint(3,4) = beverloo(test(minC,1),r,toPrint(3,1),b);
toPrint(4,4) = beverloo(test(minC,1),r,toPrint(4,1),b);

figure(2)
plot(toPrint(:,1), toPrint(:,2), 'o',"linewidth", 3)
hold on
plot(toPrint(:,1), toPrint(:,4),'o','Color', 'r',"linewidth", 3)
hold off
