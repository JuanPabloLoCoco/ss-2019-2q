# Q = B * (d - cr) ^ 1.5
# d = Diametro de apertura de salida
# B = parametro a ajustar
# cr = radio medio particulas
function [ans] = beverloo( c, r, d, b)
  Q = b * (( d - c * r)^1.5);
  ans = Q;
end
