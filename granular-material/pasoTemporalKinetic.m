#pasoTemporalPor kinetic
clear;

fileName = '.\results\Time_election\Kinetic_N400_Seed1000_hole0.15dt1_0.01.csv';
A = importdata(fileName,',',1);
datos1 = A.data;

fileName = '.\results\Time_election\Kinetic_N400_Seed1000_hole0.15dt1_0.001.csv';
A = importdata(fileName,',',1);
datos2 = A.data;

fileName = '.\results\Time_election\Kinetic_N400_Seed1000_hole0.15dt1_1.0E-4.csv';
A = importdata(fileName,',',1);
datos3 = A.data;

fileName = '.\results\Time_election\Kinetic_N400_Seed1000_hole0.15dt1_1.0E-5.csv';
A = importdata(fileName,',',1);
datos4 = A.data;

fileName = '.\results\Time_election\Kinetic_N400_Seed1000_hole0.15dt1_1.0E-6.csv';
A = importdata(fileName,',',1);
datos5 = A.data;

#semilogy(datos1(:,1), datos1(:,2), 'o')
#hold on
semilogy(datos2(:,1), datos2(:,2),'.')
#semilogy(datos4(:,1), datos4(:,2),'.','Color', 'y')
#semilogy(datos5(:,1), datos5(:,2),'.','Color', 'k')
#semilogy(datos5(:,1), datos5(:,2), 'o')
hold off