# Aperturas

fileName = '.\results\Diferent_holes\OutBoxList_N400_Seed1000_hole0.15dt1_1.0E-5.csv';
[qVal1, size1] = qaudal(fileName);

for q = 2 : size1
  aux1(q - 1, 1) = qVal1(q,1);
  aux1(q - 1, 2) = qVal1(q,2);
endfor
toPrint(1,1) = 0.15;
toPrint(1,2) = mean(aux1(:,2));
toPrint(1,3) = std(aux1(:,2));
  

fileName = '.\results\Diferent_holes\OutBoxList_N400_Seed1000_hole0.18dt1_1.0E-5.csv';
[qVal2, size2] = qaudal(fileName);
clear('aux1')
for q = 2 : size2
  aux1(q - 1, 1) = qVal2(q,1);
  aux1(q - 1, 2) = qVal2(q,2);
endfor
toPrint(2,1) = 0.18;
toPrint(2,2) = mean(aux1(:,2));
toPrint(2,3) = std(aux1(:,2));

fileName = '.\results\Diferent_holes\OutBoxList_N400_Seed1000_hole0.21dt1_1.0E-5.csv';
[qVal3, size3] = qaudal(fileName);
clear('aux1');
for q = 2 : size3
  aux1(q - 1, 1) = qVal3(q,1);
  aux1(q - 1, 2) = qVal3(q,2);
endfor
toPrint(3,1) = 0.21;
toPrint(3,2) = mean(aux1(:,2));
toPrint(3,3) = std(aux1(:,2));

fileName = '.\results\Diferent_holes\OutBoxList_N400_Seed1000_hole0.24dt1_1.0E-5.csv';
[qVal4, size4] = qaudal(fileName);
clear('aux1');
for q = 2 : size4;
  aux1(q - 1, 1) = qVal4(q,1);
  aux1(q - 1, 2) = qVal4(q,2);
endfor
toPrint(4,1) = 0.24;
toPrint(4,2) = mean(aux1(:,2));
toPrint(4,3) = std(aux1(:,2));



p1 = errorbar(toPrint(:,1), toPrint(:,2), toPrint(:,3))
hold on
set(p1, "linestyle", "none");
plot(toPrint(:,1),toPrint(:,2),'o', 'Color', 'r', 'linewidth', 3)
hold off
#plot(qVal5(:,1) , qVal5(:,2) ,'o','Color', 'r', 'linewidth', 1)
