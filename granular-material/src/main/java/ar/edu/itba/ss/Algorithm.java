package ar.edu.itba.ss;

import java.awt.geom.Point2D;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class Algorithm {
	private double totalTime = 0;
    private List<Particle> particles;
    private double deltaT;
    private double dt2;
    private double maxHeigth;

	public Algorithm(List<Particle> particles) {
		this.particles = particles;
	}

    public void setParticles(List<Particle> particles) {
        this.particles = particles;
    }

    private void dt2Routine(){
        String particlesData = generateParticlesData(particles, totalTime);
        String particleSimulation = generateParticlesSimulationData(particles);
        String kineticEnergy = generateKineticEnergyData(particles, totalTime);

        generateKineticOutput(kineticEnergy);
        generateParticlesOutput(particlesData);
        generateParticleSimulationOutput(particleSimulation, totalTime);
    }

    private void dt1Routine(){
        for (Particle p1 : particles){
			p1.setForce(particles);
            if (p1.getPreviousX() == 0 && p1.getPreviousY() == 0){
                setPreviousPositionWithEuler(p1);
            }
            verlet(p1);
        }

        double finalL = Configuration.getInstance().getFinalL();

        // Update MaxHeight only when has to;
		boolean calculateMaxHeigth = true;

        for (Particle p1 : particles){
        	p1.updatePositions();
        	if (p1.getPreviousY() >= 0 && p1.getY() < 0){
				String data = generateOutboxData(p1, totalTime);
				generateParticleOutBoxOutput(data);
			}

        	if (p1.getY() < finalL){
				// flow.addToFlow();
				maxHeigth = Particle.getMaxHeigth(particles);
				//if (calculateMaxHeigth){
				//
				//	calculateMaxHeigth = false;
				//}
				generateReinsertionPosition(p1);
			}
        }
    }

    private void generateReinsertionPosition(Particle particle){
		double L = Configuration.getInstance().getL() - particle.getRadius();
		double minPosibleY = maxHeigth < 0 ? L/2 : maxHeigth;
		double maxPosibleY = L;
		boolean generationPositions = true;
		double n = 0;
		double particleTries = 1000;

		do{
			double posY = MyRandom.getInstance().nextDouble(minPosibleY + particle.getRadius(), maxPosibleY);
			double posX = MyRandom.getInstance().nextDouble(particle.getRadius(), Configuration.getInstance().getW() - particle.getRadius());
			particle.setProperties(posX, posY, 0,0);
			n++;
		} while (!Particle.isValidParticle(particle, particles) && n <= particleTries);


		if (n > particleTries){
			maxHeigth = maxHeigth * 1.1;
			generateReinsertionPosition(particle);
		}
		particle.setPreviousPosition(0,0);
	}

    private void verlet(Particle p1) {
        // Se supone que llego con las fuerzas calculadas.
		double rx = p1.getX();
		double ry = p1.getY();
        double newX = (2 * rx) - p1.getPreviousX() + ((Math.pow(deltaT, 2) * p1.getForceX())/p1.getMass());
        double newY = (2 * ry) - p1.getPreviousY() + ((Math.pow(deltaT, 2) * p1.getForceY())/p1.getMass());
        double newVx = (newX - p1.getPreviousX())/(2 * deltaT);
        double newVy = (newY - p1.getPreviousY())/(2 * deltaT);

        p1.setFutureProperties(newX, newY, newVx, newVy);
        p1.setForce(new Point2D.Double(0, 0));
    }

    private void setPreviousPositionWithEuler(Particle p1) {
        double rx = p1.getX() - deltaT * p1.getVelX();
        double ry = p1.getY() - deltaT * p1.getVelY();
        rx += Math.pow(deltaT, 2) * p1.getForceX() / (2 * p1.getMass());
        ry += Math.pow(deltaT, 2) * p1.getForceY() / (2 * p1.getMass());
        p1.setPreviousPosition(rx, ry);
    }

    public void run() {
        double dt1 = Configuration.getInstance().getDt1();
		deltaT = dt1;
		dt2 = Configuration.getInstance().getDt2();
		double cutTime = Configuration.getInstance().getFinalTime();

        int dt2Rdt1 = (int) Math.round(dt2 / dt1);
		int dt2Rdt1Counter = 0;

		/* Initial Situation. totalTime = 0 */
        dt2Routine();

        boolean cutTimeOn = true;
        while (!cutTimeOn || totalTime < cutTime) {
            System.out.println("Total Time: " + totalTime + " - Kinetic Energy: " + Particle.getKineticEnergy(particles));
            // dtRoutine(); ¿¿Va a aca??? ¿¿O abajo??
            totalTime += dt1;
			dt1Routine();
			dt2Rdt1Counter++;

			if ((dt2Rdt1Counter % dt2Rdt1) == 0) {
				dt2Routine();
			}
		}
	}

	/** Prints to File */
    private void generateParticleSimulationOutput(String particleData, double time) {
        String filePath = Configuration.outputDir +
                "Simu_N" +
                particles.size() +
                "_Seed" +
                Configuration.getInstance().getSeed() +
                "_hole" +
                Configuration.getInstance().getD() +
                "dt1_" +
                Configuration.getInstance().getDt1() +
                ".txt";
		Path path = Paths.get(filePath);

        String timeBuilder = particles.size() +
                "\r\n//" +
                time +
                "_time,id,x,y,radio,r,g,b" +
                "\r\n";
        StringBuilder toPrint = new StringBuilder()
				.append(timeBuilder)
				.append(particleData);

		try {
			if (!Files.exists(path)){
				Files.write(path, toPrint.toString().getBytes(), StandardOpenOption.CREATE);
			} else {
				Files.write(path, toPrint.toString().getBytes(), StandardOpenOption.APPEND);
			}
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	private void generateParticlesOutput(String particleData) {
        String filePath = Configuration.outputDir +
                "Data_N" +
                particles.size() +
                "_Seed" +
                Configuration.getInstance().getSeed() +
                "_hole" +
                Configuration.getInstance().getD() +
                "dt1_" +
                Configuration.getInstance().getDt1() +
                ".csv";
		Path path = Paths.get(filePath);

		try {
			if (!Files.exists(path)){
				Files.write(path, getParticlesHeader().getBytes(), StandardOpenOption.CREATE);
			}
			Files.write(path, particleData.getBytes(), StandardOpenOption.APPEND);
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	private void generateKineticOutput(String particleData){
		StringBuilder builder = new StringBuilder()
				.append(Configuration.outputDir)
				.append("Kinetic_N")
				.append(particles.size())
				.append("_Seed")
				.append(Configuration.getInstance().getSeed())
				.append("_hole")
				.append(Configuration.getInstance().getD())
				.append("dt1_")
				.append(Configuration.getInstance().getDt1())
				.append(".csv");
		String filePath = builder.toString();
		Path path = Paths.get(filePath);

		try {
			if (!Files.exists(path)){
				Files.write(path, getKineticHeader().getBytes(), StandardOpenOption.CREATE);
			}
			Files.write(path, particleData.getBytes(), StandardOpenOption.APPEND);
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	private void generateParticleOutBoxOutput(String particleData){
		StringBuilder builder = new StringBuilder()
				.append(Configuration.outputDir)
				.append("OutBoxList_N")
				.append(particles.size())
				.append("_Seed")
				.append(Configuration.getInstance().getSeed())
				.append("_hole")
				.append(Configuration.getInstance().getD())
				.append("dt1_")
				.append(Configuration.getInstance().getDt1())
				.append(".csv");
		String filePath = builder.toString();
		Path path = Paths.get(filePath);

		try {
			if (!Files.exists(path)){
				Files.write(path, getOutboxHeader().getBytes(), StandardOpenOption.CREATE);
			}
			Files.write(path, particleData.getBytes(), StandardOpenOption.APPEND);
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	/** Headers */
	private String getParticlesHeader () {
		return "time,id,x,y,vx,vy, mass\r\n";
	}

	private String getOutboxHeader() {
		return "time,id\r\n";
	}

    private String getKineticHeader () { return "time,energy\r\n";}

	/** Granular Particles Simulation Data */
	private String generateParticlesSimulationData(List<Particle> particleList){
		StringBuilder builder = new StringBuilder();
		for (Particle current: particleList){
			builder.append(current.getId())
					.append(" ")
					.append(current.getX())
					.append(" ")
					.append(current.getY())
					.append(" ")
					.append(current.getRadius())
					.append(" ")
					.append(current.getColor())
					.append("\r\n");
		}
		return builder.toString();
	}

	/**  Granular Particles Data */
	private String generateParticlesData(List<Particle> particleList, double time){
		StringBuilder builder = new StringBuilder();
		for (Particle current: particleList){
			builder.append(time)
					.append(",")
					.append(current.getId())
					.append(",")
					.append(current.getX())
					.append(",")
					.append(current.getY())
					.append(",")
					.append(current.getVelX())
					.append(",")
					.append(current.getVelY())
					.append(",")
					.append(current.getMass())
					.append("\r\n");
		}
		return builder.toString();
	}

	private String generateKineticEnergyData(List<Particle> particleList, double time){
        StringBuilder builder;
        builder = new StringBuilder()
                .append(time)
                .append(",")
                .append(Particle.getKineticEnergy(particleList))
                .append("\r\n");
        return builder.toString();
	}

	private String generateOutboxData(Particle particle, double time){
        String builder;
        builder = time +
                "," +
                particle.getId() +
                "\r\n";
        return builder;
	}

}