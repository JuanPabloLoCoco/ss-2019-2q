package ar.edu.itba.ss;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

public class SystemForces {

    private static double kt = Configuration.getInstance().getkT();
    private static double kn = Configuration.getInstance().getkN();
    private static double gamma = Configuration.getInstance().getGamma();
    private static double width = Configuration.getInstance().getW();
    private static double hole = Configuration.getInstance().getD();

    public static double getForceN2(Particle p1, Particle p2) {
        /* Fn = - Kn * O */
        return - kn * p1.getOverlap(p2);
    }

    private static double getForceT3(Particle p1, Particle p2) {
        /* Ft = - Kt * O * [vrelativa * tversor] */
        return - kt * p1.getOverlap(p2) * ( dot(p1.getRelativeVelocity(p2),p1.getVersorT(p2)) );
    }

    public static Point2D.Double getForceG( Particle p ) {
        return new Point2D.Double(0,-9.8 * p.getMass());
    }

    private static double dot(Point2D.Double v1, Point2D.Double v2) {
        return v1.getX() * v2.getX() + v1.getY() * v2.getY();
    }

    public static Point2D.Double sum(Point2D.Double v1, Point2D.Double v2) {
        return new Point2D.Double(v1.getX() + v2 .getX(), v1.getY() + v2.getY());
    }

    public static Point2D.Double getForceN(Particle p1, Particle p2) {
        return getForce(getForceN2(p1,p2), p1.getVersorN(p2));
    }

    public static Point2D.Double getForceT(Particle p1, Particle p2) {
        return getForce(getForceT3(p1,p2), p1.getVersorT(p2));
    }

    private static Point2D.Double getForce(double force, Point2D.Double versor) {
        return new Point2D.Double(force * versor.getX() , force * versor.getY());
    }

    public static void setForces(List<Particle> particles) {
        for (Particle p: particles) {
        	p.setForce(particles);
        }
    }

    public static List<Particle> getWalls(Particle p) {
        List<Particle> walls = new ArrayList<>();

        // VerticalWalls
        Particle leftWall = new Particle(0);
        leftWall.setProperties(0, p.getY(),0,0);
        if (leftWall.overlaps(p)){
            walls.add(leftWall);
        }

        Particle rightWall = new Particle(0);
        rightWall.setProperties(width, p.getY(), 0,0);
        if (rightWall.overlaps(p)){
            walls.add(rightWall);
        }

        // Horizontal Wall
        Particle bottomWall;
        if( p.getX() < (width-hole)/2 || p.getX() > (width+hole)/2 ){
            bottomWall = new Particle(0);
            bottomWall.setProperties(p.getX(), 0, 0,0);
            if (bottomWall.overlaps(p)){
                walls.add(bottomWall);
            }
        } else {
            bottomWall = new Particle(0);
            bottomWall.setProperties((width-hole)/2, 0, 0,0);
            if (bottomWall.overlaps(p)){
                walls.add(bottomWall);
            }
            bottomWall = new Particle(0);
            bottomWall.setProperties((width+hole)/2, 0, 0,0);
            if (bottomWall.overlaps(p)){
                walls.add(bottomWall);
            }
        }
        
        return walls;
    }
}
