package ar.edu.itba.ss;

import java.util.LinkedList;
import java.util.Queue;

public class Flow {
	/** N */
	private static int WINDOW_SIZE = Configuration.getInstance().getWindow();
	/** Window measured in time units */
	private static double WINDOW = WINDOW_SIZE * Configuration.getInstance().getDt1();
	private int windowCounter = 0;
	private Queue<Integer> window = new LinkedList<Integer>();
	private int newSlice = 0;
	
	/** adds particles that left through the hole in dt1 */
	public void addToFlow() {
		newSlice++;
	}
	
	private void addSliceToFlow() {
		window.offer(newSlice);
		windowCounter++;
	}
	
	/** Check for window in dt2 */
	public boolean windowAvailable() {
		boolean windowOn = (windowCounter >= WINDOW_SIZE);
		if (windowOn) {window.poll();} // 1st slice is no longer part of window
		addSliceToFlow();
		return windowOn;
	}
	
	public double getFlow() {
		int flow = 0;
		for (Integer windowSlice : window) {
			flow += windowSlice;
		}
		return (double )flow / WINDOW;
	}
}