package ar.edu.itba.ss;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

public class Particle {
	private static double MAX_PRESSURE = 1100;

	// private double velocityModule = 0;
	private double velocityAngle = 0;

	private static int NEW_ID = 1;
	private static int RADIUS_ID = 0;
	private static int COLOR_ID = 1;
	private static int X_ID = 0, Y_ID = 1, VEL_X_ID = 2, VEL_Y_ID = 3;
	private static String DEFAULT_COLOR = "#FFF";
	private int id = 0;
	private Point2D position = new Point2D.Double();
	private Point2D previousPosition = new Point2D.Double();
	private Point2D velocity = new Point2D.Double();
	private Point2D previousVelocity = new Point2D.Double();
	private Point2D acceleration = new Point2D.Double(0.0,0.0);
	private Point2D derivative3 = new Point2D.Double(0.0,0.0);
	private Point2D derivative4 = new Point2D.Double(0.0,0.0);
	private Point2D derivative5 = new Point2D.Double(0.0,0.0);
	private Point2D previousAcceleration = new Point2D.Double();
	private Point2D force = new Point2D.Double();
	private Point2D futurePosition = new Point2D.Double();
	private Point2D futureVelocity = new Point2D.Double();
	private double radius;
	private String color;
	private double mass = 0;
	private double pressure = 0;

	public Particle(String[] details) {
		this(Double.parseDouble(details[RADIUS_ID]), details[COLOR_ID]);
	}

	public Particle(double radius) {
		this(radius, DEFAULT_COLOR);
	}

	public Particle(double radius, String color) {
		this.radius = radius;
		this.color = color;
		// this.velocityAngle = MyRandom.getInstance().nextDouble(0, Math.PI * 2);
	}
	
	public double getPressure() {
		return pressure;
	}

	public void setPressure(double pressure) {
		this.pressure = pressure;
	}
	
	void setId() {
		if (id == 0) {
			this.id = NEW_ID;
			NEW_ID++;
		}
	}

	/**
	 * public static void setPeriodic(boolean periodic){
	 * isPeriodic = periodic;
	 * }
	 */

	int getId() {
		return id;
	}

	double getRadius() {
		return radius;
	}

	public Point2D getPosition() {
		return position;
	}

	double getY() {
		return position.getY();
	}

	double getX() {
		return position.getX();
	}

	double getForceY() {
		return force.getY();
	}

	double getForceX() {
		return force.getX();
	}

	double getAccelerationX() { return acceleration.getX(); }

	double getAccelerationY() { return acceleration.getY(); }

	double getDerivative3X() { return derivative3.getX(); }

	double getDerivative3Y() { return derivative3.getY(); }

	double getDerivative4X() { return derivative4.getX(); }

	double getDerivative4Y() { return derivative4.getY(); }

	double getDerivative5X() { return derivative5.getX(); }

	double getDerivative5Y() { return derivative5.getY(); }

	double getPreviousY() {
		return previousPosition.getY();
	}

	double getPreviousX() {
		return previousPosition.getX();
	}

	double getPreviousVelocityX() { return previousVelocity.getX(); }

	double getPreviousVelocityY() { return previousVelocity.getY(); }

	double getPreviousAccelerationX() { return previousAcceleration.getX(); }

	double getPreviousAccelerationY() { return previousAcceleration.getY(); }

	public double getAngle() {
		return velocityAngle;
	}

	void setAngle(double velocityAngle) {
		this.velocityAngle = velocityAngle;
	}

	/**
	 * @param cl cell length
	 * @return y normalized index
	 */
	int getYIndex(double cl) {
		return (int) (getY() / cl) % (int) cl;
	}

	/**
	 * @param cl cell length
	 * @return x normalized index
	 */
	int getXIndex(double cl) {
		return (int) (getX() / cl) % (int) cl;
	}

	public Particle setProperties(String[] properties) {
		return setProperties(Double.parseDouble(properties[X_ID]), Double.parseDouble(properties[Y_ID]),
				Double.parseDouble(properties[VEL_X_ID]), Double.parseDouble(properties[VEL_Y_ID]));
	}

	Particle setProperties(double x, double y, double velX, double velY) {
		position.setLocation(x,y);
		velocity.setLocation(velX, velY);
		return this;
	}

	Particle setFutureProperties(double x, double y, double velX, double velY){
		futurePosition.setLocation(x, y);
		futureVelocity.setLocation(velX, velY);
		return this;
	}

	public void updatePositions(){
		previousPosition.setLocation(position);
		position.setLocation(futurePosition);
		velocity.setLocation(futureVelocity);
	}

	public void setPreviousPosition(double x, double y){
		previousPosition.setLocation(x,y);
	}

	public void setPreviousVelocity(double x, double y) { previousVelocity.setLocation(x,y); }

	public void setPreviousAcceleration(double x, double y) { previousAcceleration.setLocation(x,y); }

	public void setAcceleration(double x, double y) { acceleration.setLocation(x,y); }

	public void setDerivative3(double x, double y) { derivative3.setLocation(x,y); }

	public void setDerivative4(double x, double y) { derivative4.setLocation(x,y); }

	public void setDerivative5(double x, double y) { derivative5.setLocation(x,y); }


	public double getDistance(double x1, double y1, double x2, double y2){
		double x = x1 - x2;
		double y = y1 - y2;
		return Math.sqrt(x * x + y * y);
	}

	public double getDistance(Particle other) {
		/** Distance between centers minus the radius of the particles */
		return position.distance(other.position);
	}

	public void advance(double time) {
		/** %l accounts for periodic condition */
		position.setLocation(position.getX() + getVelX()*time, position.getY() + getVelY()*time);
	}

	@Override
	public String toString() {
		return "Id: " + id + " (Radius: " + radius + ", Position x: " + position.getX() + ", y: " + position.getY() + ", Velocity x: " + velocity.getX() + ", Velocity y: " + velocity.getY() + ")";
	}
	
	public String getStaticRepresentation() {
		return radius + " " + color;
	}

	public String getDynamicRepresentation() {
		return position.getX() + " " + position.getY() + " " + getVelX() + " " + getVelY();
	}
	
	public double getVelX() {
		return velocity.getX();
		// return velocity * Math.cos(velocityAngle);
	}
	
	public double getVelY() {
		return velocity.getY();
		// return velocity * Math.sin(velocityAngle);
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public double getMass() {
		return mass;
	}

	public void setMass(double mass) {
		this.mass = mass;
	}

	public static boolean isValidParticle(Particle newParticle, List<Particle> currentParticles) {
		for (Particle currentParticle : currentParticles) {

			if (currentParticle.overlaps(newParticle) && !newParticle.equals(currentParticle)) {
				return false;
			}
		}
		return true;
	}
	
	public static Particle generateRandom(double width, double length, double radius) {
		double xPos, yPos;

		xPos = MyRandom.getInstance().nextDouble(radius, width - radius);
		yPos = MyRandom.getInstance().nextDouble(radius, length - radius);
		// xPos = Math.round(xPos*100)/(double)100;
		// yPos = Math.round(yPos*100)/(double)100;

		Particle particle = new Particle(radius);
		particle.setProperties(xPos, yPos, 0, 0);

		return particle;
	}

	public String getColor(){
		double red, blue, green;
		/**
		 *  SCALE:
		 *  1 to 1/3 of max pressure: (r: 0, b: 0 to 1, g: 0) white to blue
		 *  1/3 to 2/3: (r: 0 to 1, b: 1, g: 0) blue to purple
		 *  2/3 to max (r: 1, b: 1 to 0, g: 0) purple to red
		 */
		double relPressure = this.getPressure()/MAX_PRESSURE;
		if (relPressure > 1) {relPressure = 1;}
		
		if (relPressure >= 0 && relPressure < 0.333) {
			red = 1 - 3*relPressure; blue = 1; green = 1 - 3*relPressure;
		} else if (relPressure >= 0.333 && relPressure < 0.666) {
			red = 3*(relPressure - 0.333); blue = 1; green = 0;
		} else {
			red = 1; blue = 1 - 3*(relPressure - 0.666); green = 0;
		}
		
		StringBuilder builder = new StringBuilder()
				.append(red)
				.append(" ")
				.append(green)
				.append(" ")
				.append(blue);
		return builder.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Particle particle = (Particle) o;
		return id == particle.id;
	}

	@Override
	public int hashCode() {
		return id;
	}

	public double getVelocityModule(){
		final double velocityModule;
		velocityModule = Math.sqrt(velocity.getX() * velocity.getX() + velocity.getY() * velocity.getY());
		return velocityModule;
	}

	public Point2D.Double getVelocity() {
		return new Point2D.Double(getVelX(),getVelY());
	}

	public double getVersorX(Particle other) {
		return (other.getX() - this.getX()) / getDistance(other);
	}

	public double getVersorY(Particle other) {
		return (other.getY() - this.getY()) / getDistance(other);
	}

	public Point2D.Double getVersorN(Particle other) {
		return new Point2D.Double(getVersorX(other),getVersorY(other));
	}

	public Point2D.Double getVersorT(Particle other) {
		return new Point2D.Double(-getVersorY(other),getVersorX(other));
	}

	public static double getKineticEnergy(List <Particle> list){
		double kineticEnergy = 0;
		for (Particle particle : list){
			kineticEnergy = kineticEnergy +  particle.getMass() * particle.getVelocityModule() * particle.getVelocityModule();
		}
		return (kineticEnergy/2)/list.size();
	}

	public double getOverlap(Particle other) {
		double overlap = this.radius + other.radius - getDistance(other);
		if (overlap <= 0) return 0;
		return overlap;
	}

	public Point2D.Double getRelativeVelocity(Particle other) {
		return new Point2D.Double(this.getVelX() - other.getVelX(), this.getVelY() - other.getVelY());
	}

	public boolean overlaps(Particle other) {
		return getOverlap(other) > 0;
	}

	public void setId(int id){
		this.id = id;
	}

	public void setForce(Point2D force) {
		this.force = force;
	}
	
	public void setForce(List<Particle> particleList) {
		setForce(getForce(particleList));
	}

	public Point2D getForce() {
		return force;
	}

	public static double getMaxHeigth(List<Particle> particleList){
		boolean first = true;
		double maxHeigth = 0;
		for (Particle particle: particleList){
			if (first){
				maxHeigth = particle.getY() + particle.getRadius();
			} else {
				if ((particle.getY() + particle.getRadius())> maxHeigth){
					maxHeigth = particle.getY() + particle.getRadius();
				}
			}
		}
		return maxHeigth;
	}
	
	private Point2D.Double getForce(List<Particle> particleList) {
		double nForce = 0;
        Point2D.Double force = new Point2D.Double();
        List<Particle> listWithWalls = new ArrayList<Particle>(particleList);
        listWithWalls.addAll(SystemForces.getWalls(this));

        for (Particle p2 : listWithWalls) {
            if(!this.equals(p2) && overlaps(p2)) {
            	nForce = nForce + SystemForces.getForceN2(this,p2);
            	force = SystemForces.sum(SystemForces.getForceN(this,p2), force);
                force = SystemForces.sum(SystemForces.getForceT(this,p2), force);
            }
        }
        force = SystemForces.sum(SystemForces.getForceG(this),force);
        
        this.setPressure(Math.abs(nForce)/(2*Math.PI*this.getRadius()));

        return force;
	}
}