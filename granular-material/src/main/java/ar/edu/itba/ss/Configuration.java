package ar.edu.itba.ss;

import javax.swing.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

class Configuration {
	private final static String projectDir = System.getProperty("user.dir");
	final static String staticInfoDir = projectDir + "/resources/staticInfo.txt";
	final static String dynamicInfoDir = projectDir + "/resources/dynamicInfo.txt";
	final static String outputDir = projectDir + "/results/";
	final static String analyticsFileName = "analyticsFile.csv";

	private Properties properties;
	private InputStream inStream = null;

	private static Configuration instance;

	/** Seed */
	private long seed;

	private double dt1;

	private double dt2;

	private double finalL;

	private double mass;

	private double kN;

	private double kT;

	private double gamma;

	private double minRadius;

	private double maxRadius;

	private double W;

	private double L;

	private double D;

	private int particleTries;

	private double finalTime;

	private int window;

	private Configuration(){
		properties = new Properties();
		try {
			String configInfoDir = projectDir + "/resources/config.properties";
			inStream = new FileInputStream(configInfoDir);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		loadConfiguration();
	}

	static Configuration getInstance(){
		if (instance == null){
			instance = new Configuration();
		}
		return instance;
	}

	private void loadConfiguration() {

		try {
			properties.load(inStream);
		} catch (IOException e){
			e.printStackTrace();
		}

		seed = Long.parseLong(properties.getProperty("seed"));

		dt2 = Double.parseDouble(properties.getProperty("dt2"));
		dt1 = Double.parseDouble(properties.getProperty("dt1"));
		window = Integer.parseInt(properties.getProperty("window"));

		mass = Double.parseDouble(properties.getProperty("mass"));
		kN = Double.parseDouble(properties.getProperty("k.N"));
		kT = 2 * kN;

		gamma = Double.parseDouble(properties.getProperty("gamma"));
		minRadius = Double.parseDouble(properties.getProperty("minDiameter"))/2;
		maxRadius = Double.parseDouble(properties.getProperty("maxDiameter"))/2;

		W = Double.parseDouble(properties.getProperty("W"));
		L = Double.parseDouble(properties.getProperty("L"));
		D = Double.parseDouble(properties.getProperty("D"));
		finalL = - (L/10.0);
		particleTries = Integer.parseInt(properties.getProperty("particleTries"));
		finalTime = Double.parseDouble(properties.getProperty("finalTime"));
	}

	public long getSeed() {
		return seed;
	}

	public double getDt1() {
		return dt1;
	}

	public double getDt2() {
		return dt2;
	}

	public double getFinalL() {
		return finalL;
	}

	public double getMass() {
		return mass;
	}

	public double getkN() {
		return kN;
	}

	public double getkT() {
		return kT;
	}

	public double getGamma() {
		return gamma;
	}

	public double getMinRadius() {
		return minRadius;
	}

	public double getMaxRadius() {
		return maxRadius;
	}

	public double getW() {
		return W;
	}

	public double getL() {
		return L;
	}

	public double getD() {
		return D;
	}

	public int getParticleTries() {
		return particleTries;
	}

	public double getFinalTime () { return finalTime; }

	public int getWindow() {
		return window;
	}

	@Override
	public String toString() {
		return "Configuration{" +
				"seed=" + seed +
				", dt1=" + dt1 +
				", dt2=" + dt2 +
				", finalL=" + finalL +
				", mass=" + mass +
				"\n" +
				", kN=" + kN +
				", kT=" + kT +
				", gamma=" + gamma +
				", minRadius=" + minRadius +
				"\n" +
				", maxRadius=" + maxRadius +
				", W=" + W +
				", L=" + L +
				", D=" + D +
				"\n" +
				", particleTries=" + particleTries +
				", finalTime=" + finalTime +
				'}';
	}
}