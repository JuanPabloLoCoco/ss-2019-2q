package ar.edu.itba.ss;

import sun.awt.geom.AreaOp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class ParticleFactory {
    private static ParticleFactory instance;

    public static ParticleFactory getInstance(){
        if (instance == null){
            instance = new ParticleFactory();
        }
        return instance;
    }

    public List<Particle> generateTestParticles() {
        double L = Configuration.getInstance().getL();
        double W = Configuration.getInstance().getW();
        double radius = Configuration.getInstance().getMinRadius();
        double mass = Configuration.getInstance().getMass();
        Particle p1 = new Particle(radius);
        Particle p2 = new Particle(radius);

        p1.setProperties(W/4.0 + radius - 0.04, L/4 + radius, 0,0);
        p1.setId();
        p1.setMass(mass);
        p2.setProperties(W/4.0 + radius - 0.04, L* 3/4.0 + radius, 0,0);
        p2.setId();
        p2.setMass(mass);


        List<Particle> particleList = new ArrayList<>();
        particleList.add(p1);
        particleList.add(p2);
        return particleList;
    }

    public List<Particle> generateParticles (){
        List<Particle> particleList = new ArrayList<>();

        int i = 0;
        int n = 0;
        double mass = Configuration.getInstance().getMass();

        boolean generationParticles = true;

        while (generationParticles){
            Particle particle = tryParticle(particleList);
            if (particle != null) {
                particle.setMass(mass);
                particle.setId();
                particleList.add(particle);
            } else {
                generationParticles = false;
            }
        }
        return particleList;
    }

    private Particle tryParticle (List<Particle> particleList){
        Particle particle;
        int n = 0;
        int particleTries = Configuration.getInstance().getParticleTries();
        double length = Configuration.getInstance().getL();
        double width = Configuration.getInstance().getW();

        do {

            double minRadius = Configuration.getInstance().getMinRadius();
            double maxRadius = Configuration.getInstance().getMaxRadius();
            double radius = MyRandom.getInstance().nextDouble(minRadius, maxRadius);
            particle = Particle.generateRandom(width, length, radius);
            n++;
        } while (!Particle.isValidParticle(particle, particleList) && n <= particleTries);
        if (n <= particleTries){
            return particle;
        }
        return null;
    }

	private List<Particle> generateWallParticles () {
        List<Particle> particleList = new ArrayList<>();
        double heigth = Configuration.getInstance().getL();
        double hole = Configuration.getInstance().getD();
        double width = Configuration.getInstance().getW();
        double wallRadio =  Configuration.getInstance().getMinRadius()/2;
        double radius = Configuration.getInstance().getMinRadius();

        int count = (int)((width - hole)/radius);

        /** Wall Particles */
        for (int i = 0; i <= count; i++){
            Particle particle = new Particle(wallRadio);
            particle.setProperties(i * wallRadio,0 , 0,0 );
            particle.setId();
            particleList.add(particle);
        }
        for (int j = 0; j <= count; j++){
            Particle particle = new Particle(wallRadio);
            particle.setProperties(j * wallRadio + (hole + width)/2, 0, 0,0 );
            particle.setId();
            particleList.add(particle);
        }

        /** Vertical Border Particles */
        int heigthCount = (int)(heigth/radius) * 2;
        for (int i = 0; i < heigthCount; i++) {
            Particle p1 = new Particle(wallRadio);
            Particle p2 = new Particle(wallRadio);
            p1.setProperties(0, i * wallRadio, 0, 0);
            p2.setProperties(width, i * wallRadio, 0, 0);
            p1.setId();
            p2.setId();
            particleList.add(p1);
            particleList.add(p2);
        }

        /** Horizontal Border Particles */
        int widthCount = (int)(width/radius) * 2;
        for (int i = 0; i < widthCount; i++) {
            Particle p1 = new Particle(wallRadio);
            p1.setProperties(i * wallRadio, heigth, 0, 0);
            p1.setId();
            particleList.add(p1);
        }

        return particleList;
    }

    private String generateParticlesWallData(List<Particle> particleList){
        StringBuilder builder = new StringBuilder();
        for(Particle current: particleList){
            builder.append(current.getId())
                    .append(" ")
                    .append(current.getX())
                    .append(" ")
                    .append(current.getY())
                    .append(" ")
                    .append(current.getRadius())
                    .append("\r\n");
        }
        return builder.toString();
    }

    public void generateParticleFile (List<Particle> particleList, String fileName){
        String dataToPrint = generateParticlesWallData(particleList);
        StringBuilder builder = new StringBuilder()
                .append(Configuration.outputDir)
                .append(fileName)
                .append(".txt");
        String filePath = builder.toString();
        Path path = Paths.get(filePath);

        StringBuilder timeBuilder = new StringBuilder()
                .append(particleList.size())
                .append("\r\n\r\n")
                .append(dataToPrint);
        try {
            if (!Files.exists(path)){
                Files.write(path, timeBuilder.toString().getBytes(), StandardOpenOption.CREATE);
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public void generateLJWall() {
        List<Particle> particleList = generateParticles();
        //List<Particle> particleList = generateWallParticles();

        String dataToPrint = generateParticlesWallData(particleList);
        StringBuilder builder = new StringBuilder()
                .append(Configuration.outputDir)
                .append("Wall_")
                .append(Configuration.getInstance().getD())
                .append(".txt");
        String filePath =  builder.toString();
        Path path = Paths.get(filePath);

        StringBuilder timeBuilder = new StringBuilder()
                .append(particleList.size())
                .append("\r\n\r\n")
                .append(dataToPrint);
        try {
            if (!Files.exists(path)){
                Files.write(path, timeBuilder.toString().getBytes(), StandardOpenOption.CREATE);
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }

}
