# Eleccion Paso Temporal, 

fileName = '.\results\Time_election\OutBoxList_N400_Seed1000_hole0.15dt1_0.01.csv';
[qVal1, size1] = qaudal(fileName);

for q = 2 : size1
  aux1(q - 1, 1) = qVal1(q,1);
  aux1(q - 1, 2) = qVal1(q,2);
endfor
toPrint(1,1) = 0.01;
toPrint(1,2) = mean(aux1(:,2));
toPrint(1,3) = std(aux1(:,2));
  

fileName = '.\results\Time_election\OutBoxList_N400_Seed1000_hole0.15dt1_0.001.csv';
[qVal2, size2] = qaudal(fileName);
clear('aux1')
for q = 2 : size2
  aux1(q - 1, 1) = qVal2(q,1);
  aux1(q - 1, 2) = qVal2(q,2);
endfor
toPrint(2,1) = 0.001;
toPrint(2,2) = mean(aux1(:,2));
toPrint(2,3) = std(aux1(:,2));

fileName = '.\results\Time_election\OutBoxList_N400_Seed1000_hole0.15dt1_1.0E-4.csv';
[qVal3, size3] = qaudal(fileName);
clear('aux1');
for q = 2 : size3
  aux2(q - 1, 1) = qVal3(q,1);
  aux2(q - 1, 2) = qVal3(q,2);
endfor
toPrint(3,1) = 0.0001;
toPrint(3,2) = mean(aux2(:,2));
toPrint(3,3) = std(aux2(:,2));

fileName = '.\results\Time_election\OutBoxList_N400_Seed1000_hole0.15dt1_1.0E-5.csv';
[qVal4, size4] = qaudal(fileName);
clear('aux1');
for q = 2 : size4;
  aux3(q - 1, 1) = qVal3(q,1);
  aux3(q - 1, 2) = qVal3(q,2);
endfor
toPrint(4,1) = 0.00001;
toPrint(4,2) = mean(aux1(:,2));
toPrint(4,3) = std(aux1(:,2));

fileName = '.\results\Time_election\OutBoxList_N400_Seed1000_hole0.15dt1_1.0E-6.csv';
[qVal5, size5] = qaudal(fileName);
clear('aux1');
for q = 2 : size5
  aux1(q - 1, 1) = qVal5(q,1);
  aux1(q - 1, 2) = qVal5(q,2);
endfor
toPrint(5,1) = 0.000001;
toPrint(5,2) = mean(aux1(:,2));
toPrint(5,3) = std(aux1(:,2));

semilogx(toPrint(:,1),toPrint(:,2),'o', 'Color', 'r', 'linewidth', 3)

#plot(qVal5(:,1) , qVal5(:,2) ,'o','Color', 'r', 'linewidth', 1)

