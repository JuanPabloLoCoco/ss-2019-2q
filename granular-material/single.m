
clear
fileName = '.\results\Diferent_holes\OutBoxList_N400_Seed1000_hole0.15dt1_1.0E-5.csv';
[qVal1, size1] = qaudal(fileName);

fileName = '.\results\Diferent_holes\OutBoxList_N400_Seed1000_hole0.18dt1_1.0E-5.csv';
[qVal2, size2] = qaudal(fileName);

fileName = '.\results\Diferent_holes\OutBoxList_N400_Seed1000_hole0.21dt1_1.0E-5.csv';
[qVal3, size3] = qaudal(fileName);

fileName = '.\results\Diferent_holes\OutBoxList_N400_Seed1000_hole0.24dt1_1.0E-5.csv';
[qVal4, size4] = qaudal(fileName);

plot(qVal1(:,1),qVal1(:,2),'o','linewidth', 2)
hold on

plot(qVal2(:,1),qVal2(:,2),'o','Color', 'r','linewidth', 2)
plot(qVal3(:,1),qVal3(:,2),'o','Color', 'g','linewidth', 2)
plot(qVal4(:,1),qVal4(:,2),'o','Color', 'k','linewidth', 2)
hold off


