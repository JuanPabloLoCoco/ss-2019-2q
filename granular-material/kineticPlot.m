
# Kinetic Energy Plot
fileName = '.\results\Kinetic_N400_Seed1000_hole0.15dt1_1.0E-5.csv';
A = importdata(fileName,',',1);
datos = A.data;

# datos (x, 1) se encuetra el tiempo
# datos (x, 2) contiene la enegia cinetica

# loglog(datos(:,1),datos(:,2), 'o', 'Color', 'r', 'linewidth', 3)
plot(datos(:,1),datos(:,2), 'Color', 'r', 'linewidth', 3)