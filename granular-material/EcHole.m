#Hole_Kinetic

fileName = '.\results\Diferent_holes\Kinetic_N400_Seed1000_hole0.15dt1_1.0E-5.csv';
A = importdata(fileName,',',1);
datos1 = A.data;

fileName = '.\results\Diferent_holes\Kinetic_N400_Seed1000_hole0.18dt1_1.0E-5.csv';
A = importdata(fileName,',',1);
datos2 = A.data;

fileName = '.\results\Diferent_holes\Kinetic_N400_Seed1000_hole0.21dt1_1.0E-5.csv';
A = importdata(fileName,',',1);
datos3 = A.data;

fileName = '.\results\Diferent_holes\Kinetic_N400_Seed1000_hole0.24dt1_1.0E-5.csv';
A = importdata(fileName,',',1);
datos4 = A.data;


# semilogy (x, y)

semilogy(datos1(:,1), datos1(:,2))
hold on

semilogy(datos2(:,1), datos2(:,2),'Color', 'r')
semilogy(datos3(:,1), datos3(:,2),'Color', 'y')
semilogy(datos4(:,1), datos4(:,2),'Color', 'k')
hold off
