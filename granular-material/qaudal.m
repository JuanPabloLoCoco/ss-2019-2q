function [qVal, qValSize] = qaudal( fileName ) 
# Qaudal
  # fileName = '.\results\OutBoxList_N400_Seed1000_hole0.24dt1_1.0E-5.csv';
  A = importdata(fileName,',',1);
  datos = A.data;

  # datos (x, 1) se encuetra el tiempo
  # datos (x, 2) contiene el ID de particula. 
  clear('qVal')
  window = 0.1;
  index = 1;
  next = datos(index, 1);
  qIndex = 1;
  maxIndex = size(datos,1);
  for x = 0 : window : 5
    qVal(qIndex, 1) = x;
    qVal(qIndex, 2) = 0;
    while (x > next && index < 1727)
      qVal(qIndex, 2) = qVal(qIndex, 2) + 1; 
      index = index + 1;
      if ( index < maxIndex)
        next = datos(index, 1);
      else
        next = x;
      endif
    end
    qIndex = qIndex + 1;
  end
  
  size = size(qVal, 1);
  
  for y = 1 : size
    qVal(y, 2) = qVal(y, 2) / window;  
  end

  qValSize = size;

endfunction
#plot(qVal(:,1) , qVal(:,2) ,'o','Color', 'r', 'linewidth', 1)