package ar.edu.itba.ss;

import java.util.ArrayList;
import java.util.List;

public abstract class Derivatives {
    abstract double der0(Particle particle, List<Particle> particles);
    abstract double der1(Particle particle, List<Particle> particles);
    abstract double der2(Particle particle, List<Particle> particles);
    abstract double der3(Particle particle, List<Particle> particles);
    abstract double der4(Particle particle, List<Particle> particles);
    abstract double der5(Particle particle, List<Particle> particles);

    public static Derivatives oscilatorRDerivatives() {
        
        return new Derivatives() {
            @Override
            double der0(Particle particle, List<Particle> particles) {
                return particle.getX();
            }

            @Override
            double der1(Particle particle, List<Particle> particles) {
                return particle.getVelX();
            }

            @Override
            double der2(Particle particle, List<Particle> particles) {
                return oscilatorEquation(this.der0(particle, particles),this.der1(particle, particles),particle);
            }

            @Override
            double der3(Particle particle, List<Particle> particles) {
                return oscilatorEquation(this.der1(particle, particles),this.der2(particle, particles),particle);

            }

            @Override
            double der4(Particle particle, List<Particle> particles) {
                return oscilatorEquation(this.der2(particle, particles),this.der3(particle, particles),particle);
            }

            @Override
            double der5(Particle particle, List<Particle> particles) {
                return oscilatorEquation(this.der3(particle, particles),this.der4(particle, particles),particle);
            }

            private double oscilatorEquation(double arg1, double arg2, Particle particle) {
                return (- Configuration.getInstance().getOsciK() * arg1 - Configuration.getInstance().getOsciGamma() * arg2 ) / particle.getMass();
            }
        };
    }

    public static Derivatives lennardVDerivativesX() {

        return new Derivatives() {
            @Override
            double der0(Particle particle, List<Particle> particles) {
                return particle.getX();
            }

            @Override
            double der1(Particle particle, List<Particle> particles) {
                return particle.getVelX();
            }

            @Override
            double der2(Particle particle, List<Particle> particles) {
                return lennardEquation(2, particle, particles);
            }

            @Override
            double der3(Particle particle, List<Particle> particles) {
                return lennardEquation(3, particle, particles);

            }

            @Override
            double der4(Particle particle, List<Particle> particles) {
                return lennardEquation(4, particle, particles);
            }

            @Override
            double der5(Particle particle, List<Particle> particles) {
                return lennardEquation(5, particle, particles);
            }

            private double lennardEquation(int i, Particle particle, List<Particle> particles) {
                double sumForces = lennardEquationInner(i,particle,walls(particle));
                sumForces += lennardEquationInner(i,particle,particles);
                
                return sumForces / particle.getMass();
            }
            
            private double lennardEquationInner(int i, Particle particle, List<Particle> particles) {
            	double sumForces = 0;
                for (Particle particle2 : particles) {
                    sumForces += lennardEquation(i, particle, particle2) * ((particle2.getX() - particle.getX())/particle2.getDistance(particle));
                }
            	return sumForces;
            }

            private double lennardEquation(int i, Particle particle1, Particle particle2) {
                double rRelation = Configuration.getInstance().getLJRadius()/particle2.getDistance(particle1);
                double eCoef = Configuration.getInstance().getLJEpsilon()/Math.pow(Configuration.getInstance().getLJRadius(),i);
                double coef12 = coefNumb(12,i);
                double coef6 = coefNumb(6,i);

                return eCoef * ((coef12 * Math.pow(rRelation,12+i)) - (2 * coef6 * Math.pow(rRelation, 6+i)));
            }

            private double coefNumb(int num, int i) {
                int coef = 1;

                for (int j = 0; j < i; j++) {
                    coef *= (num + j);
                }
                return coef;
            }
        };
    }

    public static Derivatives lennardVDerivativesY() {

        return new Derivatives() {
            @Override
            double der0(Particle particle, List<Particle> particles) {
                return particle.getY();
            }

            @Override
            double der1(Particle particle, List<Particle> particles) {
                return particle.getVelY();
            }

            @Override
            double der2(Particle particle, List<Particle> particles) {
                return lennardEquation(2, particle, particles);
            }

            @Override
            double der3(Particle particle, List<Particle> particles) {
                return lennardEquation(3, particle, particles);

            }

            @Override
            double der4(Particle particle, List<Particle> particles) {
                return lennardEquation(4, particle, particles);
            }

            @Override
            double der5(Particle particle, List<Particle> particles) {
                return lennardEquation(5, particle, particles);
            }

            private double lennardEquation(int i, Particle particle, List<Particle> particles) {
                double sumForces = lennardEquationInner(i,particle,walls(particle));
                sumForces += lennardEquationInner(i,particle,particles);
                
                return sumForces / particle.getMass();
            }

            private double lennardEquationInner(int i, Particle particle, List<Particle> particles) {
                double sumForces = 0;
                for (Particle particle2 : particles) {
                	sumForces += lennardEquation(i, particle, particle2) * ((particle2.getY() - particle.getY())/particle2.getDistance(particle));
                }
                return sumForces / particle.getMass();
            }

            private double lennardEquation(int i, Particle particle1, Particle particle2) {
                double rRelation = Configuration.getInstance().getLJRadius()/particle2.getDistance(particle1);
                double eCoef = Configuration.getInstance().getLJEpsilon()/Math.pow(Configuration.getInstance().getLJRadius(),i);
                double coef12 = coefNumb(12,i);
                double coef6 = coefNumb(6,i);

                return eCoef * ((coef12 * Math.pow(rRelation,12+i)) - (2 * coef6 * Math.pow(rRelation, 6+i)));
            }

            private double coefNumb(int num, int i) {
                int coef = 1;

                for (int j = 0; j < i; j++) {
                    coef *= (num + j);
                }
                return coef;
            }
        };
    }
    
    private static List<Particle> walls(Particle particle) {
    	Particle fictitiousParticle;
    	List<Particle> fictitiousParticles = new ArrayList<Particle>();
    	double interactionDistance = Configuration.getInstance().getLJInteractionDistance();
    	double width = Configuration.getInstance().getLJWidth();
    	double height = Configuration.getInstance().getLjHeight();
    	double closestHorizontalWall = (particle.getY() < height/2? 0 : height);
    	
    	if (particle.getX() < interactionDistance) {
    		fictitiousParticle = new Particle(0).setProperties(0,particle.getY(),0,0);
    		fictitiousParticles.add(fictitiousParticle);
    	}
    	
    	if (Math.abs(particle.getY() - closestHorizontalWall) < interactionDistance) {
    		fictitiousParticle = new Particle(0).setProperties(particle.getX(),closestHorizontalWall,0,0);
    		fictitiousParticles.add(fictitiousParticle);
    	}
    	
    	if (Math.abs(particle.getX() - width/2) >= interactionDistance) { // Particle is far away from hole
    		return fictitiousParticles;
    	}
    	
    	double holeSize = Configuration.getInstance().getLJHole();
    	double holeLimit1 = (height - holeSize)/2;
    	double holeLimit2 = height - holeLimit1;
    	
    	if (holeLimit1 < particle.getY() && particle.getY() < holeLimit2) { // Inside hole y limits
    		fictitiousParticle = new Particle(0).setProperties(width/2,holeLimit1,0,0); // Limit 1
    		
    		if (particle.getDistance(fictitiousParticle) < interactionDistance) {
    			fictitiousParticles.add(fictitiousParticle);
    		}
    		
    		fictitiousParticle = new Particle(0).setProperties(width/2,holeLimit2,0,0); // Limit 2
    		
    		if (particle.getDistance(fictitiousParticle) < interactionDistance) {
    			fictitiousParticles.add(fictitiousParticle);
    		}
    	} else { // Particle in front of middle wall (already checked to be inside interaction distance)
    		fictitiousParticle = new Particle(0).setProperties(width/2,particle.getY(),0,0);
    		fictitiousParticles.add(fictitiousParticle);
    	}
    	
    	return fictitiousParticles;
    }
}