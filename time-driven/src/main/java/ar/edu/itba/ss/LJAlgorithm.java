package ar.edu.itba.ss;

public class LJAlgorithm extends BaseAlgorithm {
	Derivatives LJDerivativesX = Derivatives.lennardVDerivativesX();
	Derivatives LJDerivativesY = Derivatives.lennardVDerivativesY();

	
	@Override
	double systemForce(Particle particle) {
		return 0;
	}

	@Override
	double forceBetweenParticles(Particle particle1, Particle particle2) {

			/* double rRelation = Configuration.getInstance().getLJRadius()/Particle.distanceBetween;

			return (Configuration.getInstance().getLJEpsilon() / Configuration.getInstance().getLJRadius()) * 12
				* (Math.pow(rRelation,13) - Math.pow(rRelation, 7)); */
			return 0.0;

	}

	@Override
	Derivatives getDerivatives() {
		return LJDerivativesX;
	}

	@Override
	Derivatives getDerivativesY() { return LJDerivativesY; }

	@Override
	double getAcceleration(double arg1, double arg2, Particle particle) {
		return 0.0;
	}
}