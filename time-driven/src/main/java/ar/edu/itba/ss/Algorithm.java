package ar.edu.itba.ss;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class Algorithm {
	private double totalTime = 0;
	private int dt2Rdt1;
    private List<Particle> particles;
    private BaseAlgorithm baseAlgorithm;
	private AlgorithmMethod algorithmMethod;
	private Dt1Routine dt1Routine;
	private Dt2Routine dt2Routine;
	private CutTimeHandler cutTimeSetter;
	private double cutTime;
	private boolean cutTimeOn = false;

	public Algorithm(List<Particle> particles, AlgorithmMethod algorithmMethod, BaseAlgorithm baseAlgorithm) {
		this.particles = particles;
		this.algorithmMethod = algorithmMethod;
		this.baseAlgorithm = baseAlgorithm;
	}

    public void setParticles(List<Particle> particles) {
        this.particles = particles;
    }

    public void setDt1Routine(Dt1Routine dt1Routine) {
		this.dt1Routine = dt1Routine;
	}

    public void setDt2Routine(Dt2Routine dt2Routine) {
		this.dt2Routine = dt2Routine;
	}

    public void setCutTimeHandler(CutTimeHandler cutTimeSetter) {
		this.cutTimeSetter = cutTimeSetter;
	}
    
    public static Algorithm getAlgorithm() {
    	int problem = Configuration.getInstance().getProblem();
    	String algorithm = (problem == 0? "Oscillator" : "LJ");
    	
    	AlgorithmMethod method = AlgorithmMethod.getMethod(); // Gear, Beeman or Verlet
    	
    	switch(algorithm) {
    		case "Oscillator":
    			return getOscillatorAlgorithm(method);
    		case "LJ":
    			return getLJAlgorithm(method);
    		default:
    			return null;
    	}
    }

	public static Algorithm getOscillatorAlgorithm(AlgorithmMethod algorithmMethod) {
		Algorithm algorithm = new Algorithm(ParticleFactory.getInstance().generateOscilatorParticle(),algorithmMethod,new OscillatorAlgorithm());
		algorithm.setDt1Routine(new Dt1Routine() {
			@Override
			public void generateOutput(String particleData) {
				algorithm.generateOscilatorOutput(particleData);
			}
			@Override
			public String generateData(List<Particle> particleList, double time) {
				return algorithm.generateOscilatorData(particleList,time);
			}
		});
		algorithm.setDt2Routine(new Dt2Routine() {
			@Override
			public void generateSimulationOutput(String particleData, double time) {
			}
			@Override
			public String generateSimulationData(List<Particle> particleList, double time) {
				return null;
			}
			@Override
			public void octaveOutput(List<Particle> particleList, double time) {
			}
		});
		algorithm.setCutTimeHandler(new CutTimeHandler() {
			@Override
			public void cutTimeSetRoutine() {
				if (algorithm.cutTimeOn) return;
				
				algorithm.cutTime = Configuration.getInstance().getOsciFinalTime();
				algorithm.cutTimeOn = true;
			}
		});
		return algorithm;
	}

	public static Algorithm getLJAlgorithm(AlgorithmMethod algorithmMethod) {
		Algorithm algorithm = new Algorithm(ParticleFactory.getInstance().generateLJParticles(),algorithmMethod,new LJAlgorithm());
		algorithm.setDt1Routine(new Dt1Routine() {
			@Override
			public void generateOutput(String particleData) {
				algorithm.generateLJOutput(particleData);
			}
			@Override
			public String generateData(List<Particle> particleList, double time) {
				return algorithm.generateParticlesData(particleList,time);
			}
		});
		algorithm.setDt2Routine(new Dt2Routine() {
			@Override
			public void generateSimulationOutput(String particleData, double time) {
				//algorithm.generateLJSimulationOutput(particleData,time);
			}
			@Override
			public String generateSimulationData(List<Particle> particleList, double time) {
				return null;
				//return algorithm.generateParticlesSimulationData(particleList,time);
			}
			@Override
			public void octaveOutput(List<Particle> particleList, double time) {
				String ej2 = algorithm.generateEj2(particleList,time);
				algorithm.generateEj2Output(ej2);
				if (algorithm.cutTimeOn) {
					String ej4 = algorithm.generateEj4(particleList,time);
					algorithm.generateEj4Output(ej4);
				}
			}
		});
		algorithm.setCutTimeHandler(new CutTimeHandler() {
			@Override
			public void cutTimeSetRoutine() {
				if (algorithm.cutTimeOn) return;
				
				int leftCount = 0;
				
				for (Particle particle : algorithm.particles) {
					if (particle.getX() < 200) {
						leftCount++;
					}
				}
				
				if (leftCount == algorithm.particles.size()/2) {
					algorithm.cutTime = Configuration.getInstance().getLJTimeFactor() * algorithm.totalTime;
					algorithm.cutTimeOn = true;
				}
			}
		});
		return algorithm;
	}
	
	public void run() {
		double dt1 = Configuration.getInstance().getdt1();
		double dt2 = Configuration.getInstance().getdt2();
		dt2Rdt1 = (int) Math.round(dt2/dt1);
		int dt2Rdt1Counter = 0;
		
		cutTimeSetter.cutTimeSetRoutine();

		String data = dt2Routine.generateSimulationData(particles,totalTime);
		dt2Routine.generateSimulationOutput(data, totalTime);

		while ((cutTimeOn && totalTime < cutTime) || !cutTimeOn) {
			algorithmMethod.run(particles,baseAlgorithm);
			
			totalTime += dt1;
			dt2Rdt1Counter++;
			
			data = dt1Routine.generateData(particles,totalTime);
			dt1Routine.generateOutput(data);

			System.out.println("Total Time: " + totalTime);

			if ((dt2Rdt1Counter % dt2Rdt1) == 0) {
				data = dt2Routine.generateSimulationData(particles,totalTime);
				dt2Routine.generateSimulationOutput(data,totalTime);
				dt2Routine.octaveOutput(particles,totalTime);
			}
			
			cutTimeSetter.cutTimeSetRoutine();
		}
	}

	/** Prints to File */
	private void generateLJSimulationOutput(String particleData, double time) {
		StringBuilder builder = new StringBuilder()
				.append(Configuration.outputDir)
				.append("Simu")
				.append(algorithmMethod.toString())
				.append("_N")
				.append(Configuration.getInstance().getLJn())
				.append("_Seed")
				.append(Configuration.getInstance().getSeed())
				.append("_hole")
				.append(Configuration.getInstance().getLJHole())
                .append("dt1_")
                .append(Configuration.getInstance().getdt1())
				.append(".txt");
		String filePath =  builder.toString();
		Path path = Paths.get(filePath);

		StringBuilder timeBuilder = new StringBuilder()
				.append(particles.size())
				.append("\r\n//")
				.append(time)
				.append("_time,id,x,y,vx,vy,radio,mass")
				.append("\r\n");
		StringBuilder toPrint = new StringBuilder()
				.append(timeBuilder)
				.append(particleData);

		try {
			if (!Files.exists(path)){
				Files.write(path, toPrint.toString().getBytes(), StandardOpenOption.CREATE);
			} else {
				Files.write(path, toPrint.toString().getBytes(), StandardOpenOption.APPEND);
			}
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	private void generateOscilatorOutput(String particleData) {
		StringBuilder builder = new StringBuilder()
				.append(Configuration.outputDir)
				.append(algorithmMethod.toString())
				.append("_dt")
				.append(Configuration.getInstance().getdt1())
				.append(".csv");
		String filePath = builder.toString();
		Path path = Paths.get(filePath);

		try {
			if (!Files.exists(path)){
				Files.write(path, getOscilatorHeader().getBytes(), StandardOpenOption.CREATE);
			}
			Files.write(path, particleData.getBytes(), StandardOpenOption.APPEND);
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	private void generateLJOutput(String particleData) {
		StringBuilder builder = new StringBuilder()
				.append(Configuration.outputDir)
				.append(algorithmMethod.toString())
				.append("_N")
				.append(Configuration.getInstance().getLJn())
				.append("_Seed")
				.append(Configuration.getInstance().getSeed())
				.append("_hole")
				.append(Configuration.getInstance().getLJHole())
                .append("dt1_")
                .append(Configuration.getInstance().getdt1())
				.append(".csv");
		String filePath =  builder.toString();
		Path path = Paths.get(filePath);

		try {
			if (!Files.exists(path)){
				Files.write(path, getLJHeader().getBytes(), StandardOpenOption.CREATE);
			}
			Files.write(path, particleData.getBytes(), StandardOpenOption.APPEND);
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	private void generateEj2Output(String particleData){
		StringBuilder builder = new StringBuilder()
				.append(Configuration.outputDir)
				.append("Ej2")
				.append(algorithmMethod.toString())
				.append("_N")
				.append(Configuration.getInstance().getLJn())
				.append("_Seed")
				.append(Configuration.getInstance().getSeed())
				.append("_hole")
				.append(Configuration.getInstance().getLJHole())
				.append("dt1_")
				.append(Configuration.getInstance().getdt1())
				.append(".csv");
		String filePath = builder.toString();
		Path path = Paths.get(filePath);

		try {
			if (!Files.exists(path)){
				Files.write(path, getEj2Header().getBytes(), StandardOpenOption.CREATE);
			}
			Files.write(path, particleData.getBytes(), StandardOpenOption.APPEND);
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	private void generateEj4Output(String particleData){
		StringBuilder builder = new StringBuilder()
				.append(Configuration.outputDir)
				.append("Ej4")
				.append(algorithmMethod.toString())
				.append("_N")
				.append(Configuration.getInstance().getLJn())
				.append("_Seed")
				.append(Configuration.getInstance().getSeed())
				.append("_hole")
				.append(Configuration.getInstance().getLJHole())
				.append("dt1_")
				.append(Configuration.getInstance().getdt1())
				.append(".csv");
		String filePath = builder.toString();
		Path path = Paths.get(filePath);

		try {
			if (!Files.exists(path)){
				Files.write(path, getEj4Header().getBytes(), StandardOpenOption.CREATE);
			}
			Files.write(path, particleData.getBytes(), StandardOpenOption.APPEND);
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	/** Headers */
	private String getLJHeader () {
		return "time,id,x,y,vx,vy\r\n";
	}

	private String getOscilatorHeader() {
	    return "time,x\r\n";
    }

    private String getEj2Header () { return "time,count\r\n";}
	private String getEj4Header () { return "time,vel\r\n";}
	/** Particles Data */
	private String generateParticlesSimulationData(List<Particle> particleList, double time){
		StringBuilder builder = new StringBuilder();
		for (Particle current: particleList){
			builder.append(current.getId())
					.append(" ")
					.append(current.getX())
					.append(" ")
					.append(current.getY())
					.append(" ")
					.append(current.getVelX())
					.append(" ")
					.append(current.getVelY())
					.append(" ")
					.append(Configuration.getInstance().getSimulationRadio()) // TODO: Radio? A Oliver no le gusta.
					.append(" ")
					.append(current.getMass())
					.append("\r\n");
		}
		return builder.toString();
	}

	private String generateParticlesData(List<Particle> particleList, double time){
		StringBuilder builder = new StringBuilder();
		for (Particle current: particleList){
			builder.append(time)
					.append(",")
					.append(current.getId())
					.append(",")
					.append(current.getX())
					.append(",")
					.append(current.getY())
					.append(",")
					.append(current.getVelX())
					.append(",")
					.append(current.getVelY())
					.append("\r\n");
		}
		return builder.toString();
	}

	private String generateOscilatorData(List<Particle> particleList, double time) {
        StringBuilder builder = new StringBuilder();
        Particle particle = particleList.get(0);
        builder.append(time)
                .append(",")
                .append(particle.getX())
                .append("\r\n");
        return builder.toString();
    }

    private int leftParticleCount (List<Particle> particleList) {
		double width = Configuration.getInstance().getLJWidth();
		int count = 0;
		for (Particle particle: particleList){
			if (particle.getX() <= width/2.0){
				count++;
			}
		}
		return count;
	}

	private String generateEj2(List<Particle> particleList, double time){
		StringBuilder builder = new StringBuilder()
				.append(time)
				.append(",")
				.append(leftParticleCount(particleList))
				.append("\r\n");
		return builder.toString();
	}

	private String generateEj4(List<Particle> particleList, double time){
		 StringBuilder builder = new StringBuilder();
		 for (Particle particle: particleList){
		 	double vel = Math.sqrt(particle.getVelX() * particle.getVelX() + particle.getVelY() * particle.getVelY());
		 	builder.append(time)
					.append(",")
					.append(vel)
					.append("\r\n");
		 }
		 return builder.toString();
	}

	private interface Dt1Routine {
		void generateOutput(String particleData);
		String generateData(List<Particle> particleList, double time);
	}
	
	private interface Dt2Routine {
		void generateSimulationOutput(String particleData, double time);
		String generateSimulationData(List<Particle> particleList, double time);
		void octaveOutput(List<Particle> particleList, double time);
	}
	
	private interface CutTimeHandler {
		void cutTimeSetRoutine();
	}
}