package ar.edu.itba.ss;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class ParticleFactory {
    private static ParticleFactory instance;

    public static ParticleFactory getInstance(){
        if (instance == null){
            instance = new ParticleFactory();
        }
        return instance;
    }

    public List<Particle> generateOscilatorParticle () {
        List<Particle> particles = new ArrayList<>();
        Particle ret = new Particle(0);
        double xini = Configuration.getInstance().getOsciPosIni();
        double gamma = Configuration.getInstance().getOsciGamma();
        double mass = Configuration.getInstance().getOsciMass();
        double vx = (-1) * (gamma / (2 * mass));
        ret.setProperties(xini, 0, vx, 0);
        ret.setMass(mass);
        ret.setId();
        particles.add(ret);
        return particles;
    }

    public List<Particle> generateLJParticles () {
        List<Particle> particles = new ArrayList<>();
        generateSmallParticles(particles);
        return particles;
    }

    private void generateSmallParticles(List<Particle> list){
        int i = 0;
        int n = Configuration.getInstance().getLJn();
        double height = Configuration.getInstance().getLjHeight();

        double radius = 0;
        double mass = Configuration.getInstance().getLJMass();

        // double fraction = (double)1 / (double) 6;
        // double factor = Math.pow(2, fraction);
        // double sigma = Configuration.getInstance().getLJRadius()/ factor;

        double interactionDistance = Configuration.getInstance().getLJInteractionDistance();

        while (i < n){
            Particle particle = Particle.generateRandom(height, interactionDistance);
            while (!Particle.isValidParticle(particle, list)){
                particle = Particle.generateRandom(height, interactionDistance);
            }
            particle.setRadius(0);
            particle.setId();
            particle.setMass(mass);
            list.add(particle);
            i++;
        }
    }

	private List<Particle> generateWallParticles () {
        List<Particle> particleList = new ArrayList<>();
        double heigth = Configuration.getInstance().getLjHeight();
        double hole = Configuration.getInstance().getLJHole();
        double width = Configuration.getInstance().getLJWidth();
        double wallRadio = Configuration.getInstance().getSimulationRadio()/2;


        int count = (int)(heigth - hole);
        /** Middle wall*/
        /** Borders Particles */
        Particle particle1 = new Particle(wallRadio);
        particle1.setProperties(0, 0,0,0);
        particle1.setId();
        Particle particle2 = new Particle(wallRadio);
        particle2.setProperties(0, heigth,0,0);
        particle2.setId();
        Particle particle3 = new Particle(wallRadio);
        particle3.setProperties(width, 0,0,0);
        particle3.setId();
        Particle particle4 = new Particle(wallRadio);
        particle4.setProperties(width, heigth,0,0);
        particle4.setId();
        particleList.add(particle1);
        particleList.add(particle2);
        particleList.add(particle3);
        particleList.add(particle4);

        /** Wall Particles */
        for (int i = 0; i < count; i++){
            Particle particle = new Particle(wallRadio);
            particle.setProperties(heigth, i * wallRadio, 0,0 );
            particle.setId();
            particleList.add(particle);
        }
        for (int j = 0; j < count; j++){
            Particle particle = new Particle(wallRadio);
            particle.setProperties(heigth, j * wallRadio + (hole + heigth)/2, 0,0 );
            particle.setId();
            particleList.add(particle);
        }

        /** Borders Particles */
        for (int i = 0; i < heigth * 2; i++) {
            Particle p1 = new Particle(wallRadio);
            Particle p2 = new Particle(wallRadio);
            p1.setProperties(0, i * wallRadio, 0, 0);
            p2.setProperties(width, i * wallRadio, 0, 0);
            p1.setId();
            p2.setId();
            particleList.add(p1);
            particleList.add(p2);
        }
        for (int i = 0; i < width * 2; i++) {
            Particle p1 = new Particle(wallRadio);
            Particle p2 = new Particle(wallRadio);
            p1.setProperties(i* wallRadio, 0, 0, 0);
            p2.setProperties(i* wallRadio, heigth, 0, 0);
            p1.setId();
            p2.setId();
            particleList.add(p1);
            particleList.add(p2);
        }

        return particleList;
    }

    private String generateParticlesWallData(List<Particle> particleList){
        StringBuilder builder = new StringBuilder();
        for(Particle current: particleList){
            builder.append(current.getId())
                    .append(" ")
                    .append(current.getX())
                    .append(" ")
                    .append(current.getY())
                    .append(" ")
                    .append(current.getVelX())
                    .append(" ")
                    .append(current.getVelY())
                    .append(" ")
                    .append(current.getRadius())
                    .append("\r\n");
        }
        return builder.toString();
    }

    public void generateLJWall() {
        List<Particle> particleList = generateWallParticles();
        for (Particle part : particleList) {
            System.out.println(part.toString());
        }

        String dataToPrint = generateParticlesWallData(particleList);
        StringBuilder builder = new StringBuilder()
                .append(Configuration.outputDir)
                .append("Wall_Height")
                .append(Configuration.getInstance().getLjHeight())
                .append("_Hole")
                .append(Configuration.getInstance().getLJHole())
                .append(".txt");
        String filePath =  builder.toString();
        Path path = Paths.get(filePath);

        StringBuilder timeBuilder = new StringBuilder()
                .append(particleList.size())
                .append("\r\n\r\n")
                .append(dataToPrint);
        try {
            if (!Files.exists(path)){
                Files.write(path, timeBuilder.toString().getBytes(), StandardOpenOption.CREATE);
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }

}
