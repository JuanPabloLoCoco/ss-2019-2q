package ar.edu.itba.ss;

public class OscillatorAlgorithm extends BaseAlgorithm {
	Derivatives oscillatorDerivatives = Derivatives.oscilatorRDerivatives();
	
	@Override
	double systemForce(Particle particle) {
		return 0;
	}

	@Override
	double forceBetweenParticles(Particle particle1, Particle particle2) {
		return 0;
	}

	@Override
	Derivatives getDerivatives() {
		return oscillatorDerivatives;
	}

	@Override
	Derivatives getDerivativesY() { return null; }

	@Override
	double getAcceleration(double arg1, double arg2, Particle particle) {
		return (- Configuration.getInstance().getOsciK() * arg1 - Configuration.getInstance().getOsciGamma() * arg2 ) / particle.getMass();
	}
}