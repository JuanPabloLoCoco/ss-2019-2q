package ar.edu.itba.ss;

public abstract class BaseAlgorithm {
	abstract double systemForce(Particle particle);
	
	abstract double forceBetweenParticles(Particle particle1, Particle particle2);
	
	abstract Derivatives getDerivatives();

	abstract Derivatives getDerivativesY();

	abstract double getAcceleration(double arg1, double arg2, Particle particle);

}