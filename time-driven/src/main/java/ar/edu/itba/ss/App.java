package ar.edu.itba.ss;

import java.io.IOException;
import java.util.List;

public class App {
    public static void main( String[] args ) throws IOException {
        System.out.println("It´s running Time Driven");
        System.out.println(Configuration.getInstance().toString());

        Long seed = Configuration.getInstance().getSeed();
        MyRandom.setSeed(seed);
        
        Algorithm algorithm = Algorithm.getAlgorithm();

        //int problem = Configuration.getInstance().getProblem();
        //List<Particle> particles;
        //if (problem == 0) {
        //    particles = ParticleFactory.getInstance().generateOscilatorParticle();
        //} else {
        //    particles = ParticleFactory.getInstance().generateLJParticles();
        //}
        //algorithm.setParticles(particles);
        
        algorithm.run();


        boolean isLJ = Configuration.getInstance().getProblem() == 1; // TODO: En realidad sería comparar con el valor de config

        /* ONLY FOR OVITO SIMULATION AND STUDY
        if (isLJ) {
        	ParticleFactory.getInstance().generateLJWall();
        }
        */
    }
}