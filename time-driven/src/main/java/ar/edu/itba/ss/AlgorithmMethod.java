package ar.edu.itba.ss;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

public abstract class AlgorithmMethod {
    private static double deltaT = Configuration.getInstance().getdt1();

    abstract void method(List<Particle> particles, BaseAlgorithm algorithm);
    public abstract String toString();

    public void run(List<Particle> particles, BaseAlgorithm algorithm) {
        method(particles,algorithm);
    }
    
    public static AlgorithmMethod getMethod() {
        int methodNumber = Configuration.getInstance().getMethod();
        StringBuilder method = new StringBuilder();

        if (methodNumber == 0) {
            method.append("Beeman");
        } else if (methodNumber == 1) {
            method.append("GearOscillator");
        } else if (methodNumber == 2){
            method.append("Verlet");
        } else if(methodNumber == 3){
            method.append("GearLJ");
        } else if (methodNumber == 4){
            method.append("VerletLJ");
        }
    	
    	switch(method.toString()) {
    		case "GearOscillator":
    			return getGearOscillator();
            case "GearLJ":
                return getGearLJ();
    		case "Beeman":
    			return getBeeman();
    		case "Verlet":
    			return getVerletVariation();
            case "VerletLJ":
                return getVerletLJ();
    		default:
    			return null;
    	}
    }

    public static AlgorithmMethod getBeeman() {
        return new AlgorithmMethod () {
            @Override
            void method(List<Particle> particles, BaseAlgorithm algorithm) {
                getPreviousAcceleration(particles);

                for (Particle particle : particles) {
                	method(particle, algorithm);
                }

            }
            
            private void method(Particle particle, BaseAlgorithm algorithm) {
                /* r(t + dt) = r(t) + v(t)(dt) + 2/3 a(t) dt² - 1/6 a(t - dt) dt² */
                /** Set current Acceleration*/
                double x = particle.getX();
                double vx = particle.getVelX();
                double ax = getForce(x, vx)/particle.getMass();

                /** Get new Position */
                double newRx = newPosition(x, vx, particle.getPreviousAccelerationX(), ax);


                /** Predict Velocity */
                double predictedVelocity = getVelPredicted(vx, ax, particle.getPreviousAccelerationX());

                /** Get New Acceleration */
                double newAx = getForce(newRx, predictedVelocity)/particle.getMass();

                /** Get New Velocity */
                /* v(t + dt) = v(t) + 1/3 a(t + dt) dt + 5/6 a(t) dt - 1/6 a(t - dt) dt */
                double newVx = newVelocity(predictedVelocity, particle.getPreviousAccelerationX(), newAx, ax);

                particle.setProperties(newRx,0,newVx,0);
                particle.setPreviousAcceleration(ax, 0);
            }

            private double newVelocity (double velocity, double previousAcceleration, double newAcceleration, double acceleration) {
                return velocity +
                        ((1/3) * newAcceleration * deltaT) +
                        ((5/6) * acceleration * deltaT) -
                        ((1/6) * previousAcceleration * deltaT);
            }

            private double newPosition(double pos, double vel, double previousAcceleration, double acceleration){
               return  pos + vel*deltaT + ((2/3)*acceleration*Math.pow(deltaT,2)) -
                       ((1/6)*previousAcceleration * Math.pow(deltaT, 2));
            }

            private double getAOscilator(double arg1, double arg2, Particle particle) {
                return ((-1) * Configuration.getInstance().getOsciK() * arg1 - Configuration.getInstance().getOsciGamma() * arg2 ) / particle.getMass();
            }

            private double getForce(double position, double velocity){
                double k = Configuration.getInstance().getOsciK();
                double gamma = Configuration.getInstance().getOsciGamma();
                return (-k * position) - (gamma * velocity);
            }

            private double getVelPredicted(double velocity, double acceleration, double previousAcceleration) {
                /* v(t + dt) = v(t) + 3/2 a(t) dt - 1/2 a(t - dt) dt */
                return velocity + (3/2*acceleration*deltaT) - ((1/2)*previousAcceleration*deltaT);
            }

            private void getPreviousAcceleration(List<Particle> particleList){
                for (Particle particle : particleList){
                    double x = particle.getX();
                    double vx = particle.getVelX();
                    double previousAcceleration = getAccelationWithEuler(x, vx, particle.getMass());
                    particle.setPreviousAcceleration(previousAcceleration, 0);
                }
            }

            private double getAccelationWithEuler(double position, double velocity, double mass){
                double previousForce = getForce(position, velocity);
                double newPos = position + velocity*(-deltaT) + ((Math.pow(deltaT, 2) /(2*mass))* previousForce);
                double newVelocity = velocity + ((-deltaT/mass) * previousForce);
                return getForce(newPos, newVelocity);
            }

            @Override
            public String toString() {
                return "beeman";
            }
        };
    }

    public static AlgorithmMethod getVerletVariation() {
        return new AlgorithmMethod () {
            @Override
            void method(List<Particle> particles, BaseAlgorithm algorithm) {

                setPreviousPosition(particles);

                for (Particle particle : particles) {
                	method(particle, algorithm);
                }
            }
            
            private void method(Particle particle, BaseAlgorithm algorithm) {

                /* ri(t + dt) = 2 ri(t) - ri(t - dt) + (dt²/mi * fi(t)) + O(dt⁴) */
                double forceX = getForce(particle.getX(), particle.getVelX());
                double forceY = getForce(particle.getY(), particle.getVelY());

                /** Get new position */
                double newRx = 2 * particle.getX() - particle.getPreviousX() + ((Math.pow(deltaT,2) * forceX)/particle.getMass());
                double newRy = 2 * particle.getY() - particle.getPreviousY() + ((Math.pow(deltaT,2) * forceY)/particle.getMass());

                /** Get new Velocity*/
                /* vi(t) = ((ri(t + dt) - ri(t - dt)) / 2dt) + O(dt³) */
                double newVx = (newRx - particle.getPreviousX()) / (2 * deltaT);
                double newVy = (newRy - particle.getPreviousY()) / (2 * deltaT);

                particle.setPreviousPosition(particle.getX(),particle.getY());
                particle.setProperties(newRx,newRy,newVx,newVy);
            }

            private void setPreviousPosition(List<Particle> particleList){
                for (Particle particle: particleList) {
                   double x = particle.getX();
                   double vx = particle.getVelX();
                   double forceX = getForce(x,vx);
                   double y = particle.getY();
                   double vy = particle.getVelY();
                   double forceY = getForce(y,vy);
                   double previousX = getPreviousPositionWithEuler(x, vx, forceX, particle);
                   double previousY = getPreviousPositionWithEuler(y, vy, forceY, particle);
                   particle.setPreviousPosition(previousX,previousY);
                }
            }

            private double getPreviousPositionWithEuler(double position, double velocity, double force,Particle particle){
                double pos = position - deltaT * velocity;
                pos -= Math.pow(deltaT, 2) * force / (2 * particle.getMass());
                return pos;
            }

            private double getForce(double position, double velocity){
                double k = Configuration.getInstance().getOsciK();
                double gamma = Configuration.getInstance().getOsciGamma();
                return (-k * position) - (gamma * velocity);
            }

            @Override
            public String toString() {
                return "verlet";
            }
        };
    }

    public static AlgorithmMethod getGearOscillator() {
        return new AlgorithmMethod () {
            @Override
            void method(List<Particle> particles, BaseAlgorithm algorithm) {
                for (Particle particle : particles) {
                    method(particle, algorithm);
                }
            }

            private void method(Particle particle, BaseAlgorithm algorithm) {

                /* rp(t + dt) = r(t) + r1(t) * (dt) + r2(t) * (dt)²/2! + r3(t) * (dt)³/3! + r4(t) * (dt)⁴/4! + r5(t) * (dt)⁵/5! */
                double Rp0 = algorithm.getDerivatives().der0(particle, null) + algorithm.getDerivatives().der1(particle, null) * deltaT +
                        particle.getAccelerationX() * Math.pow(deltaT,2) / factorial(2) +
                        particle.getDerivative3X() * Math.pow(deltaT,3) / factorial(3) +
                        particle.getDerivative4X() * Math.pow(deltaT,4) / factorial(4) +
                        particle.getDerivative5X() * Math.pow(deltaT,5) / factorial(5);

                /* rp1(t + dt) = r1(t) + r2(t) * (dt) + r3(t) * (dt)²/2! + r4(t) * (dt)³/3! + r5(t) * (dt)⁴/4!  */
                double Rp1 = algorithm.getDerivatives().der1(particle, null) + particle.getAccelerationX() * deltaT +
                        particle.getDerivative3X() * Math.pow(deltaT,2) / factorial(2) +
                        particle.getDerivative4X() * Math.pow(deltaT,3) / factorial(3) +
                        particle.getDerivative5X() * Math.pow(deltaT,4) / factorial(4);

                /* rp2(t + dt) = r2(t) + r3(t) * (dt) + r4(t) * (dt)²/2! + r5(t) * (dt)³/3!  */
                double Rp2 = particle.getAccelerationX() + particle.getDerivative3X() * deltaT +
                        particle.getDerivative4X() * Math.pow(deltaT,2) / factorial(2) +
                        particle.getDerivative5X() * Math.pow(deltaT,3) / factorial(3);

                /* rp3(t + dt) = r3(t) + r4(t) * (dt) + r5(t) * (dt)²/2!  */
                double Rp3 = particle.getDerivative3X() + particle.getDerivative4X() * deltaT +
                        particle.getDerivative5X() * Math.pow(deltaT,2) / factorial(2);

                /* rp4(t + dt) = r4(t) + r5(t) * (dt)  */
                double Rp4 = particle.getDerivative4X() + particle.getDerivative5X() * deltaT;

                /* rp5(t + dt) = r5(t)  */
                double Rp5 = particle.getDerivative5X();

                double newA = getAOscilator(Rp0, Rp1, particle);
                double newAp = Rp2;

                double deltaA = newA - newAp;
                /* dR2 = da * (dt)²/2!  */
                double deltaR2 = deltaA * Math.pow(deltaT,2) / factorial(2);

                double alfa0 = 3.0/16.0;
                double alfa1 = 251.0/360.0;
                double alfa2 = 1.0;
                double alfa3 = 11.0/18.0;
                double alfa4 = 1.0/6.0;
                double alfa5 = 1.0/60.0;

                double Rc0 = Rp0 + (alfa0 * deltaR2 * factorial(0) / Math.pow(deltaT,0));
                double Rc1 = Rp1 + (alfa1 * deltaR2 * factorial(1) / Math.pow(deltaT,1));
                double Rc2 = Rp2 + (alfa2 * deltaR2 * factorial(2) / Math.pow(deltaT,2));
                double Rc3 = Rp3 + (alfa3 * deltaR2 * factorial(3) / Math.pow(deltaT,3));
                double Rc4 = Rp4 + (alfa4 * deltaR2 * factorial(4) / Math.pow(deltaT,4));
                double Rc5 = Rp5 + (alfa5 * deltaR2 * factorial(5) / Math.pow(deltaT,5));

                particle.setProperties(Rc0, particle.getY(), Rc1, particle.getVelY());
                particle.setAcceleration(Rc2, particle.getAccelerationY());
                particle.setDerivative3(Rc3, particle.getDerivative3Y());
                particle.setDerivative4(Rc4, particle.getDerivative3Y());
                particle.setDerivative5(Rc5, particle.getDerivative3Y());

            }

            private double getAOscilator(double arg1, double arg2, Particle particle) {
                return (- Configuration.getInstance().getOsciK() * arg1 - Configuration.getInstance().getOsciGamma() * arg2 ) / particle.getMass();
            }

            private int factorial(int n){
                return n <= 1 ? 1 : n * factorial(n-1);
            }

            @Override
            public String toString() {
                return "GearOscillator";
            }
        };
    }

    public static AlgorithmMethod getGearLJ() {
        return new AlgorithmMethod () {
            @Override
            void method(List<Particle> particles, BaseAlgorithm algorithm) {
                for (Particle particle : particles) {
                    method(particle, algorithm, interactionParticles(particle, particles));
                }
            }

            private void method(Particle particle, BaseAlgorithm algorithm, List<Particle> particles) {

                /* rp(t + dt) = r(t) + r1(t) * (dt) + r2(t) * (dt)²/2! + r3(t) * (dt)³/3! + r4(t) * (dt)⁴/4! + r5(t) * (dt)⁵/5! */
                double Rp0 = algorithm.getDerivatives().der0(particle, particles) + algorithm.getDerivatives().der1(particle, particles) * deltaT +
                        particle.getAccelerationX() * Math.pow(deltaT,2) / factorial(2) +
                        particle.getDerivative3X() * Math.pow(deltaT,3) / factorial(3) +
                        particle.getDerivative4X() * Math.pow(deltaT,4) / factorial(4) +
                        particle.getDerivative5X() * Math.pow(deltaT,5) / factorial(5);
                double Rp0y = algorithm.getDerivativesY().der0(particle, particles) + algorithm.getDerivativesY().der1(particle, particles) * deltaT +
                        particle.getAccelerationY() * Math.pow(deltaT,2) / factorial(2) +
                        particle.getDerivative3Y() * Math.pow(deltaT,3) / factorial(3) +
                        particle.getDerivative4Y() * Math.pow(deltaT,4) / factorial(4) +
                        particle.getDerivative5Y() * Math.pow(deltaT,5) / factorial(5);

                /* rp1(t + dt) = r1(t) + r2(t) * (dt) + r3(t) * (dt)²/2! + r4(t) * (dt)³/3! + r5(t) * (dt)⁴/4!  */
                double Rp1 = algorithm.getDerivatives().der1(particle, particles) + particle.getAccelerationX() * deltaT +
                        particle.getDerivative3X() * Math.pow(deltaT,2) / factorial(2) +
                        particle.getDerivative4X() * Math.pow(deltaT,3) / factorial(3) +
                        particle.getDerivative5X() * Math.pow(deltaT,4) / factorial(4);
                double Rp1y = algorithm.getDerivativesY().der1(particle, particles) + particle.getAccelerationY() * deltaT +
                        particle.getDerivative3Y() * Math.pow(deltaT,2) / factorial(2) +
                        particle.getDerivative4Y() * Math.pow(deltaT,3) / factorial(3) +
                        particle.getDerivative5Y() * Math.pow(deltaT,4) / factorial(4);

                /* rp2(t + dt) = r2(t) + r3(t) * (dt) + r4(t) * (dt)²/2! + r5(t) * (dt)³/3!  */
                double Rp2 = particle.getAccelerationX() + particle.getDerivative3X() * deltaT +
                        particle.getDerivative4X() * Math.pow(deltaT,2) / factorial(2) +
                        particle.getDerivative5X() * Math.pow(deltaT,3) / factorial(3);
                double Rp2y = particle.getAccelerationY() + particle.getDerivative3Y() * deltaT +
                        particle.getDerivative4X() * Math.pow(deltaT,2) / factorial(2) +
                        particle.getDerivative5X() * Math.pow(deltaT,3) / factorial(3);

                /* rp3(t + dt) = r3(t) + r4(t) * (dt) + r5(t) * (dt)²/2!  */
                double Rp3 = particle.getDerivative3X() + particle.getDerivative4X() * deltaT +
                        particle.getDerivative5X() * Math.pow(deltaT,2) / factorial(2);
                double Rp3y = particle.getDerivative3Y() + particle.getDerivative4Y() * deltaT +
                        particle.getDerivative5Y() * Math.pow(deltaT,2) / factorial(2);

                /* rp4(t + dt) = r4(t) + r5(t) * (dt)  */
                double Rp4 = particle.getDerivative4X() + particle.getDerivative5X() * deltaT;
                double Rp4y = particle.getDerivative4Y() + particle.getDerivative5Y() * deltaT;


                /* rp5(t + dt) = r5(t)  */
                double Rp5 = particle.getDerivative5X();
                double Rp5y = particle.getDerivative5Y();

                Particle auxParticle = new Particle(particle.getRadius());
                auxParticle.setProperties(Rp0, Rp0y, Rp1, Rp1y);
                auxParticle.setId(particle.getId());
                auxParticle.setMass(particle.getMass());

                double newAx = algorithm.getDerivatives().der2(auxParticle, particles);
                double newAy = algorithm.getDerivativesY().der2(auxParticle, particles);
                double newApx = Rp2;
                double newApy = Rp2y;

                double deltaAx = newAx - newApx;
                double deltaAy = newAy - newApy;
                /* dR2 = da * (dt)²/2!  */
                double deltaR2x = deltaAx * Math.pow(deltaT,2) / factorial(2);
                double deltaR2y = deltaAy * Math.pow(deltaT,2) / factorial(2);

                double alfa0 = 3.0/16.0;
                double alfa1 = 251.0/360.0;
                double alfa2 = 1.0;
                double alfa3 = 11.0/18.0;
                double alfa4 = 1.0/6.0;
                double alfa5 = 1.0/60.0;

                double Rc0 = Rp0 + (alfa0 * deltaR2x * factorial(0) / Math.pow(deltaT,0));
                double Rc1 = Rp1 + (alfa1 * deltaR2x * factorial(1) / Math.pow(deltaT,1));
                double Rc2 = Rp2 + (alfa2 * deltaR2x * factorial(2) / Math.pow(deltaT,2));
                double Rc3 = Rp3 + (alfa3 * deltaR2x * factorial(3) / Math.pow(deltaT,3));
                double Rc4 = Rp4 + (alfa4 * deltaR2x * factorial(4) / Math.pow(deltaT,4));
                double Rc5 = Rp5 + (alfa5 * deltaR2x * factorial(5) / Math.pow(deltaT,5));

                double Rc0y = Rp0y + (alfa0 * deltaR2y * factorial(0) / Math.pow(deltaT,0));
                double Rc1y = Rp1y + (alfa1 * deltaR2y * factorial(1) / Math.pow(deltaT,1));
                double Rc2y = Rp2y + (alfa2 * deltaR2y * factorial(2) / Math.pow(deltaT,2));
                double Rc3y = Rp3y + (alfa3 * deltaR2y * factorial(3) / Math.pow(deltaT,3));
                double Rc4y = Rp4y + (alfa4 * deltaR2y * factorial(4) / Math.pow(deltaT,4));
                double Rc5y = Rp5y + (alfa5 * deltaR2y * factorial(5) / Math.pow(deltaT,5));

                particle.setProperties(Rc0, Rc0y, Rc1, Rc1y);
                particle.setAcceleration(Rc2, Rc2y);
                particle.setDerivative3(Rc3, Rc3y);
                particle.setDerivative4(Rc4, Rc4y);
                particle.setDerivative5(Rc5, Rc5y);

            }

            private int factorial(int n){
                return n <= 1 ? 1 : n * factorial(n-1);
            }

            @Override
            public String toString() {
                return "GearLJ";
            }
        };
    }

    public static AlgorithmMethod getVerletLJ () {
        return new AlgorithmMethod() {
            @Override
            void method(List<Particle> particles, BaseAlgorithm algorithm) {
                double particleDistance;
                double forceCutDistance = Configuration.getInstance().getLJInteractionDistance();
                double boardWidth = Configuration.getInstance().getLJWidth();
                double boardHeight = Configuration.getInstance().getLjHeight();
                double hole = Configuration.getInstance().getLJHole();
                double kineticEnergy = 0;
                double PotencialEnergy = 0;

                for (Particle p1 : particles){
                    for (Particle p2: particles){
                        if (!p1.equals(p2)){
                            particleDistance = p1.getDistance(p2);
                            if (particleDistance <= forceCutDistance){
                                setParticleForce(p1, particleDistance, p2);
                            }
                        }
                    }

                    // VerticalWalls
                    Particle leftWall = new Particle(0);
                    leftWall.setProperties(0, p1.getY(),0,0);
                    particleDistance = p1.getDistance(leftWall);
                    if (particleDistance <= forceCutDistance){
                        setParticleForce(p1, particleDistance, leftWall);
                    }

                    Particle rightWall = new Particle(0);
                    rightWall.setProperties(boardWidth, p1.getY(), 0,0);
                    particleDistance = p1.getDistance(rightWall);
                    if (particleDistance <= forceCutDistance){
                        setParticleForce(p1, particleDistance, rightWall);
                    }

                    // Horizontal Walls
                    Particle topWall = new Particle(0);
                    topWall.setProperties(p1.getX(), boardHeight, 0,0);
                    particleDistance = p1.getDistance(topWall);
                    if (particleDistance <= forceCutDistance){
                        setParticleForce(p1, particleDistance, topWall);
                    }

                    // BOTTOM WALL
                    Particle bottomWall = new Particle(0);
                    bottomWall.setProperties(p1.getX(), 0, 0,0);
                    particleDistance = p1.getDistance(bottomWall);
                    if (particleDistance <= forceCutDistance){
                        setParticleForce(p1, particleDistance, bottomWall);
                    }

                    // Middle Wall
                    double y;

                    /*
                            |
                            |

                            .

                            |
                            |
                     */
                    /* Si estoy a la altura del agujero de la mitad */
                    if (p1.getY() < (boardHeight/2.0 + hole/2.0) && p1.getY() > (boardHeight/2.0 - hole/2.0)){
                        Particle dist1Wall = new Particle(0);
                        dist1Wall.setProperties(boardWidth/2, (boardHeight/2.0) + (hole/2.0),0,0);
                        particleDistance = p1.getDistance(dist1Wall);
                        if (particleDistance <= forceCutDistance) {
                            setParticleForce(p1,particleDistance, dist1Wall);
                        }

                        Particle dist2Wall = new Particle(0);
                        dist2Wall.setProperties(boardWidth/2, (boardHeight/2.0) - (hole/2.0),0,0);
                        particleDistance = p1.getDistance(dist2Wall);
                        if (particleDistance <= forceCutDistance) {
                            setParticleForce(p1, particleDistance, dist2Wall);
                        }
                    } else {
                        y = p1.getY();
                        Particle middleWall = new Particle(0);
                        middleWall.setProperties(boardWidth/2.0, y,0,0);
                        particleDistance = p1.getDistance(middleWall);
                        if (particleDistance <= forceCutDistance){
                            setParticleForce(p1,particleDistance,middleWall);
                        }
                    }

                    /* Si todavia no tengo la posicion anterior es que no use Euler */
                    if (p1.getPreviousX() == 0){
                        setPreviousPositionWithEuler(p1);
                    } else {
                        verlet(p1);
                    }

                    kineticEnergy += 0.5 * p1.getMass() * (p1.getVelX() * p1.getVelX() + p1.getVelY() * p1.getVelY());
                }
            }

            @Override
            public String toString() {
                return "VerletLJ";
            }

            private void verlet(Particle p1) {
                double rx = p1.getX();
                double ry = p1.getY();
                double newX = (2 * rx) - p1.getPreviousX() + ((Math.pow(deltaT,2) * p1.getForceX())/p1.getMass());
                double newY = (2 * ry) - p1.getPreviousY() + ((Math.pow(deltaT,2) * p1.getForceY())/p1.getMass());
                double newVx = (newX - p1.getPreviousX())/(2 * deltaT);
                double newVy = (newY - p1.getPreviousY())/(2 * deltaT);
                p1.setProperties(newX, newY, newVx, newVy);
                p1.setPreviousPosition(rx, ry);
                p1.setForce(new Point2D.Double(0,0));

            }

            private void setPreviousPositionWithEuler(Particle p1) {
                double posX = p1.getX() - deltaT * p1.getVelX();
                double posY = p1.getY() - deltaT * p1.getVelY();
                posX += Math.pow(deltaT, 2) * p1.getForceX() / (2 * p1.getMass());
                posY += Math.pow(deltaT, 2) * p1.getForceY() / (2 * p1.getMass());
                p1.setPreviousPosition(posX, posY);
            }

            private void setParticleForce(Particle p1, double particleDistance, Particle p2) {
                double x2 = p2.getX();
                double y2 = p2.getY();

                double force = calculateLJForce(particleDistance);
                Point2D e = calculatePolarity(p1, x2, y2);
                double fx = force * e.getX();
                double fy = force * e.getY();

                Point2D currForce = p1.getForce();
                p1.setForce(new Point2D.Double(currForce.getX() + fx, currForce.getY() + fy));

            }

            private double calculateLJForce(double distance) {
                double epsilon = Configuration.getInstance().getLJEpsilon();
                double rm = Configuration.getInstance().getLJRm();
                return (12 * epsilon/rm) * (Math.pow(rm/distance, 13) - Math.pow(rm/distance,7));
            }

            private Point2D calculatePolarity(Particle p1, double x, double y) {
                double ex = (p1.getX() - x) / Math.sqrt(Math.pow((p1.getX() - x), 2) + (Math.pow((p1.getY() - y),2)));
                double ey = (p1.getY() - y) / Math.sqrt(Math.pow((p1.getX() - x), 2) + (Math.pow((p1.getY() - y),2)));
                return new Point2D.Double(ex,ey);
            }
        };
    }

    private static List<Particle> interactionParticles(Particle particle, List<Particle> particles) {
    	List<Particle> reducedList = new ArrayList<Particle>();
    	double interactionDistance = Configuration.getInstance().getLJInteractionDistance();
    	
    	for (Particle particle2 : particles) {
    		if (!particle.equals(particle2) && particle.getDistance(particle2) < interactionDistance) {
    			reducedList.add(particle2);
    		}
    	}
    	
    	return reducedList;
    }
}