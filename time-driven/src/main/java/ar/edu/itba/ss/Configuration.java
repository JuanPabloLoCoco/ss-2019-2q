package ar.edu.itba.ss;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

class Configuration {
	private final static String projectDir = System.getProperty("user.dir");
	final static String staticInfoDir = projectDir + "/resources/staticInfo.txt";
	final static String dynamicInfoDir = projectDir + "/resources/dynamicInfo.txt";
	final static String outputDir = projectDir + "/results/";
	final static String analyticsFileName = "analyticsFile.csv";

	private Properties properties;
	private InputStream inStream = null;

	private static Configuration instance;

	/** Seed */
	private long seed;

	private double dt1;

	private double dt2;

	/** Method y Problem*/

	private int problem;

	private int method;

	/** Oscilator **/
	private double osciMass;

	private double osciK;

	private double osciGamma;

	private double osciFinalTime;

	private double osciPosIni;

	/** Lennard Jones */
	private double simulationRadio;

	private double LJRadius;

	private double LJEpsilon;

	private double LJMass;

	private double LJInteractionDistance;

	private double LjHeight;

	private double LJWidth;

	private double LJHole;

	private int LJTimeFactor;

	private int LJn;

	private double LJVelIni;

	private double LJSigma;

	private double LJRm;

	private double LJEpsilonError;

	private Configuration(){
		properties = new Properties();
		try {
			String configInfoDir = projectDir + "/resources/config.properties";
			inStream = new FileInputStream(configInfoDir);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		loadConfiguration();
	}

	static Configuration getInstance(){
		if (instance == null){
			instance = new Configuration();
		}
		return instance;
	}

	private void loadConfiguration() {

		try {
			properties.load(inStream);
		} catch (IOException e){
			e.printStackTrace();
		}

		seed = Long.parseLong(properties.getProperty("seed"));

		dt2 = Double.parseDouble(properties.getProperty("dt2"));
		dt1 = Double.parseDouble(properties.getProperty("dt1"));

		problem = Integer.parseInt(properties.getProperty("problem"));
		method = Integer.parseInt(properties.getProperty("method"));

		osciMass = Double.parseDouble(properties.getProperty("osci.mass"));
		osciK = Double.parseDouble(properties.getProperty("osci.k"));
		osciGamma = Double.parseDouble(properties.getProperty("osci.gamma"));
		osciPosIni = Double.parseDouble(properties.getProperty("osci.posIni"));
		osciFinalTime = Double.parseDouble(properties.getProperty("osci.finalTime"));

		simulationRadio = Double.parseDouble(properties.getProperty("LJ.simulationRadio"));
		LjHeight = Double.parseDouble(properties.getProperty("LJ.height"));
		LJInteractionDistance = Double.parseDouble(properties.getProperty("LJ.interactionDistance"));
		LJEpsilon = Double.parseDouble(properties.getProperty("LJ.epsilon"));
		LJHole = Double.parseDouble(properties.getProperty("LJ.hole"));
		LJTimeFactor = Integer.parseInt(properties.getProperty("LJ.timeFactor"));
		LJWidth = Double.parseDouble(properties.getProperty("LJ.width"));
		LJRadius = Double.parseDouble(properties.getProperty("LJ.radius"));
		LJMass = Double.parseDouble(properties.getProperty("LJ.mass"));
		LJVelIni = Double.parseDouble(properties.getProperty("LJ.velIni"));
		LJn = Integer.parseInt(properties.getProperty("LJ.n"));
		LJRm = Double.parseDouble(properties.getProperty("LJ.Rm"));

		LJEpsilonError = Double.parseDouble(properties.getProperty("LJ.EpsilonError"));

		double fraction = (double)1 / (double) 6;
		double factor = Math.pow(2, fraction);
		LJSigma = LJRm / factor;

	}

	long getSeed() {
		return seed;
	}

	public double getOsciMass() {
		return osciMass;
	}

	public double getOsciK() {
		return osciK;
	}

	public double getOsciGamma() {
		return osciGamma;
	}

	public double getOsciFinalTime() {
		return osciFinalTime;
	}

	public double getOsciPosIni() {
		return osciPosIni;
	}

	public double getLJRadius() {
		return LJRadius;
	}

	public double getLJEpsilon() {
		return LJEpsilon;
	}

	public double getLJMass() {
		return LJMass;
	}

	public double getLJInteractionDistance() {
		return LJInteractionDistance;
	}

	public double getLjHeight() {
		return LjHeight;
	}

	public double getLJWidth() {
		return LJWidth;
	}

	public double getLJHole() {
		return LJHole;
	}

	public int getLJTimeFactor() {
		return LJTimeFactor;
	}

	public double getdt1() {
		return dt1;
	}

	public double getdt2() {
		return dt2;
	}

	public int getLJn() {
		return LJn;
	}

	public double getLJVelIni() {
		return LJVelIni;
	}

	public double getSimulationRadio() {
		return simulationRadio;
	}

	public int getProblem() {
		return problem;
	}

	public int getMethod() {
		return method;
	}

	public double getLJSigma() {
		return LJSigma;
	}

	public double getLJRm() {
		return LJRm;
	}

	public double getLJEpsilonError() {
		return LJEpsilonError;
	}

	@Override
	public String toString() {
		return "Configuration{" +
				"seed=" + seed +
				", dt1=" + dt1 +
				", dt2=" + dt2 +
				", problem=" + problem +
				", method=" + method +
				", osciMass=" + osciMass +
				", osciK=" + osciK +
				", osciGamma=" + osciGamma +
				", osciFinalTime=" + osciFinalTime +
				", osciPosIni=" + osciPosIni +
				", simulationRadio=" + simulationRadio +
				", LJRadius=" + LJRadius +
				", LJEpsilon=" + LJEpsilon +
				", LJMass=" + LJMass +
				", LJDistance=" + LJInteractionDistance +
				", LjHeight=" + LjHeight +
				", LJWidth=" + LJWidth +
				", LJHole=" + LJHole +
				", LJTimeFactor=" + LJTimeFactor +
				", LJn=" + LJn +
				", LJVelIni=" + LJVelIni +
				'}';
	}
}