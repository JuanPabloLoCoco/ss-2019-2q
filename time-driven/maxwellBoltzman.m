function [ans] = maxwellBoltzman (x, a)
  b = sqrt(2 / pi);
  c = (x * x)* exp(-(x*x)/(2 * a*a));
  ans = b * (c / (a * a * a));
end