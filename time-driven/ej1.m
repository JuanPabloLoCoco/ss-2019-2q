## Ej 1. 

## 1.2

## VERLET 
verlet = 0;

## 0.01 10^-2
verlet = verlet + 1;
fileName = '.\results\ej1\verlet_dt0.01.csv';
A = importdata(fileName,',',1);
datos = A.data;
countDatos = size(datos)(1,1);
for x = 1:countDatos
  val(x,1) = (oscilatorFormula(datos(x,1)) - datos(x,2))^2;
endfor
error(verlet, 1) = datos(1,1);
error(verlet, 2) = sum(val(:,1)) / countDatos;
clear('val')

## 0.005
verlet = verlet + 1;
fileName = '.\results\ej1\verlet_dt0.005.csv';
A = importdata(fileName,',',1);
datos = A.data;
countDatos = size(datos)(1,1);
for x = 1:countDatos
  val(x,1) = (oscilatorFormula(datos(x,1)) - datos(x,2))^2;
endfor
error(verlet, 1) = datos(1,1);
error(verlet, 2) = sum(val(:,1)) / countDatos;
clear('val')

## 0.001
verlet = verlet + 1;
fileName = '.\results\ej1\verlet_dt0.001.csv';
A = importdata(fileName,',',1);
datos = A.data;
countDatos = size(datos)(1,1);
for x = 1:countDatos
  val(x,1) = (oscilatorFormula(datos(x,1)) - datos(x,2))^2;
endfor
error(verlet, 1) = datos(1,1);
error(verlet, 2) = sum(val(:,1)) / countDatos;
clear('val')

## 0.0005
verlet = verlet + 1;
fileName = '.\results\ej1\verlet_dt5.0E-4.csv';
A = importdata(fileName,',',1);
datos = A.data;
countDatos = size(datos)(1,1);
for x = 1:countDatos
  val(x,1) = (oscilatorFormula(datos(x,1)) - datos(x,2))^2;
endfor
error(verlet, 1) = datos(1,1);
error(verlet, 2) = sum(val(:,1)) / countDatos;
clear('val')


## 0.0001
verlet = verlet + 1;
fileName = '.\results\ej1\verlet_dt1.0E-4.csv';
A = importdata(fileName,',',1);
datos = A.data;
countDatos = size(datos)(1,1);
for x = 1:countDatos
  val(x,1) = (oscilatorFormula(datos(x,1)) - datos(x,2))^2;
endfor
error(verlet, 1) = datos(1,1);
error(verlet, 2) = sum(val(:,1)) / countDatos;
clear('val')

############################################################

## Gear 
gear = 0;

## 0.01 10^-2
gear = gear + 1;
fileName = '.\results\ej1\GearOscillator_dt0.01.csv';
A = importdata(fileName,',',1);
datos = A.data;
countDatos = size(datos)(1,1);
for x = 1:countDatos
  val(x,1) = (oscilatorFormula(datos(x,1)) - datos(x,2))^2;
endfor
error(gear, 3) = sum(val(:,1)) / countDatos;
clear('val')

## 0.005
gear = gear + 1;
fileName = '.\results\ej1\GearOscillator_dt0.005.csv';
A = importdata(fileName,',',1);
datos = A.data;
countDatos = size(datos)(1,1);
for x = 1:countDatos
  val(x,1) = (oscilatorFormula(datos(x,1)) - datos(x,2))^2;
endfor
error(gear, 3) = sum(val(:,1)) / countDatos;
clear('val')

## 0.001
gear = gear + 1;
fileName = '.\results\ej1\GearOscillator_dt0.001.csv';
A = importdata(fileName,',',1);
datos = A.data;
countDatos = size(datos)(1,1);
for x = 1:countDatos
  val(x,1) = (oscilatorFormula(datos(x,1)) - datos(x,2))^2;
endfor
error(gear, 3) = sum(val(:,1)) / countDatos;
clear('val')

## 0.0005
gear = gear + 1;
fileName = '.\results\ej1\GearOscillator_dt5.0E-4.csv';
A = importdata(fileName,',',1);
datos = A.data;
countDatos = size(datos)(1,1);
for x = 1:countDatos
  val(x,1) = (oscilatorFormula(datos(x,1)) - datos(x,2))^2;
endfor
error(gear, 3) = sum(val(:,1)) / countDatos;
clear('val')


## 0.0001
gear = gear + 1;
fileName = '.\results\ej1\GearOscillator_dt1.0E-4.csv';
A = importdata(fileName,',',1);
datos = A.data;
countDatos = size(datos)(1,1);
for x = 1:countDatos
  val(x,1) = (oscilatorFormula(datos(x,1)) - datos(x,2))^2;
endfor
error(gear, 3) = sum(val(:,1)) / countDatos;
clear('val')

#------------------------------------------------------------

## Beeman 
beeman = 0;

## 0.01 10^-2
beeman = beeman + 1;
fileName = '.\results\ej1\beeman_dt0.01.csv';
A = importdata(fileName,',',1);
datos = A.data;
countDatos = size(datos)(1,1);
for x = 1:countDatos
  val(x,1) = (oscilatorFormula(datos(x,1)) - datos(x,2))^2;
endfor
error(beeman, 4) = sum(val(:,1)) / countDatos;
clear('val')

## 0.005
beeman = beeman + 1;
fileName = '.\results\ej1\beeman_dt0.005.csv';
A = importdata(fileName,',',1);
datos = A.data;
countDatos = size(datos)(1,1);
for x = 1:countDatos
  val(x,1) = (oscilatorFormula(datos(x,1)) - datos(x,2))^2;
endfor
error(beeman, 4) = sum(val(:,1)) / countDatos;
clear('val')

## 0.001
beeman = beeman + 1;
fileName = '.\results\ej1\beeman_dt0.001.csv';
A = importdata(fileName,',',1);
datos = A.data;
countDatos = size(datos)(1,1);
for x = 1:countDatos
  val(x,1) = (oscilatorFormula(datos(x,1)) - datos(x,2))^2;
endfor
error(beeman, 4) = sum(val(:,1)) / countDatos;
clear('val')

## 0.0005
beeman = beeman + 1;
fileName = '.\results\ej1\beeman_dt5.0E-4.csv';
A = importdata(fileName,',',1);
datos = A.data;
countDatos = size(datos)(1,1);
for x = 1:countDatos
  val(x,1) = (oscilatorFormula(datos(x,1)) - datos(x,2))^2;
endfor
error(beeman, 4) = sum(val(:,1)) / countDatos;
clear('val')


## 0.0001
beeman = beeman + 1;
fileName = '.\results\ej1\beeman_dt1.0E-4.csv';
A = importdata(fileName,',',1);
datos = A.data;
countDatos = size(datos)(1,1);
for x = 1:countDatos
  val(x,1) = (oscilatorFormula(datos(x,1)) - datos(x,2))^2;
endfor
error(beeman, 4) = sum(val(:,1)) / countDatos;
clear('val')

# ---------------------------------------------------------------

loglog(error(:,1),error(:,2), 'o', 'Color', 'r', 'linewidth', 3)
hold on
loglog(error(:,1),error(:,3), 'o', 'Color', 'g', 'linewidth', 3)
loglog(error(:,1),error(:,4), 'o', 'Color', 'b', 'linewidth', 3)


#plot( datos(:,1), datos(:,2), 'Color', 'r')
#hold on
#plot( datos(:,1), val(:,2))


## loglog(x,y)
%{
err = sum(val) / countDatos;


## FORMTING ---->set (gca, "yticklabel", num2str (get (gca, "ytick"), '%.1e|'))

## 1.3
## Comparacion de dt. 

dt = datos(2,1) - datos(1,1);
%}
