## 1

## 0.01 10^-2
clear;
fileName = '.\results\ej1\GearOscillator_dt0.01.csv';
A = importdata(fileName,',',1);
datos = A.data;
countDatos = size(datos)(1,1);
for x = 1:countDatos
  val(x,1) = datos(x,1);
  val(x,2) = oscilatorFormula(datos(x,1));
  val(x,3) = datos(x,2);
endfor

## 0.01 10^-2
fileName = '.\results\ej1\beeman_dt0.01.csv';
A = importdata(fileName,',',1);
datos = A.data;
countDatos = size(datos)(1,1);
for x = 1:countDatos
  val(x,4) = datos(x,2);
endfor

## 0.01 10^-2 Verlet
fileName = '.\results\ej1\verlet_dt0.01.csv';
A = importdata(fileName,',',1);
datos = A.data;
countDatos = size(datos)(1,1);
for x = 1:countDatos
  val(x,5) = datos(x,2);
endfor

figure(1);
plot(val(:,1), val(:,2), 'color', 'magenta')
hold on
plot(val(:,1), val(:,5),'color', 'r')

plot(val(:,1), val(:,3),'color', 'g')
plot(val(:,1), val(:,4),'color', 'b')
hold off

figure(2);
plot(val(:,1), val(:,2), 'color', 'black')


