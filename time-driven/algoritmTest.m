## 0.01 10^-2
fileName = '.\results\beeman_dt0.005.csv';
A = importdata(fileName,',',1);
datos = A.data;
countDatos = size(datos)(1,1);
for x = 1: countDatos
  val(x,1) = (oscilatorFormula(datos(x,1)) - datos(x,2))^2;
  val(x,2) = oscilatorFormula(datos(x,1));
endfor

plot(datos(:,1), datos(:,2), 'o', 'color', 'r')
hold on
plot(datos(:,1), val(:,2), 'o')
hold off

clear('val')
