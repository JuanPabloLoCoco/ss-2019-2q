## ej2.2

fileName = '.\results\ej2\Ej2VerletLJ_N200_Seed1250_hole50.0dt1_1.0E-4.csv';
A = importdata(fileName,',',1);
datos = A.data;
countDatos = size(datos)(1,1);
particleCount = datos(1,2);

for x = 1:countDatos
  val(x,1) = datos(x,2)/particleCount;
endfor

plot(datos(:,1),val(:,1),'o','color', 'r')

