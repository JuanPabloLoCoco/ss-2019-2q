## Ej4
clear

fileName = '.\results\ej2\Ej4VerletLJ_N200_Seed1250_hole50.0dt1_1.0E-4.csv';
A = importdata(fileName,',',1);
datos = A.data;
countDatos = size(datos)(1,1);
particleCount = datos(1,2);

figure(1);
[velCount, velCenters] = hist(datos(:,2),200);
bin = velCenters(2) - velCenters(1);
values = velCount/(countDatos*bin);
bar(velCenters, values);

index = 0;
minErrorA = 5;

for a = 5:0.001:7
  index = index + 1;
  errors(index, 1) = 0;
  for x = 1:200
    errors(index,1) = errors(index,1) + (maxwellBoltzman(velCenters(x),a) - values(x)) * (maxwellBoltzman(velCenters(x),a) - values(x));
  endfor
  if (index == 1)
    minError = errors(index,1);
    minErrorA = a
  endif
  if (errors(index, 1) < minError)
    minError = errors(index, 1);
    minErrorIndex = index;
    minErrorA = a;
  endif
  if (a == 6)
    errors(index,1)
  endif
endfor

display('Min error = ')
minError
display('Min error A= ')
minErrorA

for q = 1:200
  toPlot(q,1) = maxwellBoltzman(velCenters(q), minErrorA);
endfor


figure(3);
plot(5:0.001:7, errors, 'o')

figure(4);
plot(velCenters, values, 'o')
hold on
plot(velCenters, toPlot(:,1) ,'color','r', 'linewidth',3)

hold off

