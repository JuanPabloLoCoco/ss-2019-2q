function [pos] = oscilatorFormula(time)
 gamma = 100;
 m = 70;
 k = 10^4;
 var = gamma/(2*m);
 a = (-1)*(var)* time;
 c = (k/m);
 d = (var)^2;
 e = (c - d)^(0.5);
 b = cos(e*time); 
 pos = exp(a) * b;
end 
