package ar.edu.itba.ss;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Algorithm {
	private Board board;
	private Map<Particle,List<Particle>> neighborParticles;
	private double rc;
    private List<Particle> particles;


	public Algorithm(double r, double l, double rc, List<Particle> particles, boolean periodic) {
		this(l, optimumDim(r,rc,l,particles.size()), rc, particles, periodic);
	}

	public Algorithm(double l, int m, double rc, List<Particle> particles, boolean periodic) {
		board = new Board(m,particles,l,periodic);
		this.rc = rc;
		this.particles = particles;
		
		neighborParticles = new HashMap<Particle,List<Particle>>();
		for (Particle particle : particles) {
			neighborParticles.put(particle,new ArrayList<Particle>());
		}
	}
	public Map<Particle, List<Particle>> getNeighborParticles() {
		return neighborParticles;
	}

	public static int optimumDim(double r, double rc, double l, int n) {
		return (int) (l/(rc+2*r));
	}
	
	public Map<Particle,List<Particle>> run() {
		resetNeighborParticles();
		
		for (Cell[] row : board.getBoard()) {
			for (Cell cell : row) {
				for (Particle particle : cell.getElements()) {
					for (Cell neighborCell : cell.getNeighborCells()) {
						for (Particle neighborParticle : neighborCell.getElements()) {
							checkDistance(particle,neighborParticle);
						}
					}
					for (Particle neighborParticle : cell.getElements()){
						if (!particle.equals(neighborParticle)){
							checkDistance(particle, neighborParticle);
						}
					}
				}
			}
		}
		
		return neighborParticles;
	}
	
	private void checkDistance(Particle particle1, Particle particle2) {
		if (particle1.getDistance(particle2) <= rc) {
			neighborParticles.get(particle1).add(particle2);
			neighborParticles.get(particle2).add(particle1);
		}
	}
	
	public Map<Particle,List<Particle>> bruteForce() {
		resetNeighborParticles();
		
		int i = 0;
		
		for (Particle particle : particles) {
			for (Particle other : particles.subList(i + 1, particles.size())) {
				checkDistance(particle,other);
			}
			i++;
		}
		
		return neighborParticles;
	}
	
	private void resetNeighborParticles() {
		for (Map.Entry<Particle,List<Particle>> entry : neighborParticles.entrySet()) {
			entry.getValue().clear();
		}
	}
	
	public String getOutput() {
		StringBuilder stringBuilder = new StringBuilder();
		
		for (Map.Entry<Particle,List<Particle>> entry : neighborParticles.entrySet()) {
			stringBuilder.append(entry.getKey());
			List<Particle> neighbors = entry.getValue();
			
			if (!neighbors.isEmpty()) {
				stringBuilder.append(", (");
				stringBuilder.append("Id: " + neighbors.get(0).getId());
				
				for (Particle neighbor : neighbors.subList(1,neighbors.size())) {
					stringBuilder.append(", Id: " + neighbor.getId());
				}
				stringBuilder.append(")");
			}
			stringBuilder.append('\n');
		}
		stringBuilder.setLength(stringBuilder.length()-1); // Remove last \n
		
		return stringBuilder.toString();
	}

	public static void generateFile(String particleData, int index, String path){
		try {
			Files.write(Paths.get(path + "/results" + index + ".txt"), particleData.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String generateParticleData(Particle particle, List<Particle> neighbours, List<Particle> AllParticles){
		StringBuilder builder = new StringBuilder()
				.append(AllParticles.size())
				.append("\r\n")
				.append("//ID\t X\t Y\t Radius\t R\t G\t B\t\r\n");

		for (Particle current: AllParticles){
			builder.append(current.getId())
					.append(" ")
					.append(current.getX())
					.append(" ")
					.append(current.getY())
					.append(" ")
					.append(current.getRadius())
					.append(" ");
			if (current.equals(particle)){
				builder.append("1 0 0\r\n");
			} else if (neighbours.contains(current)){
				builder.append("0 1 0\r\n");
			} else {
				builder.append("1 1 1\r\n");
			}
		}
		return builder.toString();
	}
}