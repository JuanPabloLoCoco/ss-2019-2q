package ar.edu.itba.ss;

import java.awt.geom.Point2D;
import java.util.List;
import java.util.Objects;

public class Particle {
	/** Default radius that should be equal or higher than any given ratio */
	private static double DEFAULT_R;
	/** time unit to recalculate distance between periods of time */
	protected static double timeUnit = 0d;
	private static boolean isPeriodic = false;
	private static CimReader configurations = null;

	private static int NEW_ID = 1;
	private static int RATIO_ID = 0;
	private static int COLOR_ID = 1;
	private static int X_ID = 0, Y_ID = 1, VEL_X_ID = 2, VEL_Y_ID = 3;
	private static String DEFAULT_COLOR = "#FFF";
	private int id = 0;
	private Point2D position = new Point2D.Double();
	private Point2D velocity = new Point2D.Double();
	private double radius;
	private String color;
	
	public Particle(String[] details) {
		this(Double.parseDouble(details[RATIO_ID]),details[COLOR_ID]);
	}
	
	public Particle(double radius) {
		this(radius,DEFAULT_COLOR); 
	}
	
	public Particle(double radius, String color) {
		if (radius > DEFAULT_R) {
			throw new IllegalArgumentException("Proposed Particle Radius too big.");
		}
		this.radius = radius;
		this.color = color;
	}

	public void setId(){
	    if (id == 0){
	        this.id = NEW_ID;
	        NEW_ID++;
        }
    }

    public static void setPeriodic(boolean periodic){
        isPeriodic = periodic;
	}

	public static void setCimReader (CimReader cimReader){
		configurations = cimReader;
	}

	public static void setDefaultRadius(double R) {
		DEFAULT_R = R;
	}
	
	public int getId() {
		return id;
	}
	
	public double getRadius() {
		return radius;
	}
	
	public String getColor() {
		return color;
	}

	public Point2D getPosition() {
		return position;
	}
	
	public double getY() {
		return position.getY();
	}
	
	public double getX() {
		return position.getX();
	}
	
	/**
	 * @param cl cell length
	 * @return y normalized index
	 */
	public int getYIndex(double cl) {
		return (int) (getY()/cl);
	}
	
	/**
	 * @param cl cell length
	 * @return x normalized index
	 */
	public int getXIndex(double cl) {
		return (int) (getX()/cl);
	}

	public Particle setProperties(String[] properties) {
		return setProperties(Double.parseDouble(properties[X_ID]),Double.parseDouble(properties[Y_ID]),
				Double.parseDouble(properties[VEL_X_ID]),Double.parseDouble(properties[VEL_Y_ID]));
	}
	
	public Particle setProperties(double x, double y, double velX, double velY) {
		position.setLocation(x,y);
		velocity.setLocation(velX,velY);
		return this;
	}

	private double getDistance(double x1, double y1, double x2, double y2){
		double x = x1 - x2;
		double y = y1 - y2;
		return Math.sqrt(x * x + y * y);
	}

	private double getPeriodicDistance(Particle other){
		double distance = 0;
		boolean first = true;
		double l = configurations.getL();
		for(int x = -1; x < 2 ; x++){
			for (int y = -1; y < 2; y++){
				if (first){
					first = false;
					distance = getDistance(position.getX(),
							position.getY(),
							other.position.getX() + x * l,
							other.position.getY() + y * l);
				} else {
					distance = Math.min(distance, getDistance(position.getX(),
							position.getY(),
							other.position.getX() + x * l,
							other.position.getY() + y * l));
				}
			}
		}
		return  distance;
	}

	public double getDistance(Particle other) {
		double distance = 0;
		if (isPeriodic){
			distance = getPeriodicDistance(other);
		} else {
			/** Distance between centers */
			distance = position.distance(other.position);
		}

		/** Distance between centers minus the radius of the particles */
		return distance - radius - other.radius;
	}

	public void recalculateDistance() {

	}

	@Override
	public String toString() {
		return "Id: " + id + " (Radius: " + radius + ", Position x: " + position.getX() + ", y: " + position.getY() + ")";
	}
	
	public String getStaticRepresentation() {
		return radius + " " + color;
	}

	public String getDynamicRepresentation() {
		return position.getX() + " " + position.getY() + " " + velocity.getX() + " " + velocity.getY();
	}
	
	public static boolean isValidParticle(Particle newParticle, List<Particle> currentParticles) {
		for (Particle currentParticle : currentParticles) {
			if (currentParticle.getDistance(newParticle) < 0) {
				return false;
			}
		}
		return true;
	}
	
	public static Particle generateRandom(double l, boolean periodic) {
		double r = Configuration.RAND_PARTICLE_FIX_SIZE ? DEFAULT_R : MyRandom.getInstance().nextDouble(DEFAULT_R);
		return generateRandom(l,r,isPeriodic);
	}
	
	public static Particle generateRandom(double l, double r, boolean periodic) {
		double xPos, yPos;
		
		if (isPeriodic) {
			xPos = MyRandom.getInstance().nextDouble(l);
			yPos = MyRandom.getInstance().nextDouble(l);
		} else { // Add margins so particle doesn't get out of bounds
			xPos = MyRandom.getInstance().nextDouble(r, l - r);
			yPos = MyRandom.getInstance().nextDouble(r, l - r);
		}
		xPos = Math.round(xPos*100)/(double)100;
		yPos = Math.round(yPos*100)/(double)100;
		
		Particle particle = new Particle(r);
		
		particle.setProperties(xPos,yPos,0,0);
		
		return particle;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Particle particle = (Particle) o;
		return id == particle.id;
	}

	@Override
	public int hashCode() {
		return id;
	}


}