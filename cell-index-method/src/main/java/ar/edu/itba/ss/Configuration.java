package ar.edu.itba.ss;

import java.util.Random;

public class Configuration {
	private final static String projectDir = System.getProperty("user.dir");
	public final static String staticInfoDir = projectDir + "/resources/staticInfo.txt";
	public final static String dynamicInfoDir = projectDir + "/resources/dynamicInfo.txt";
	public final static String outputDir = projectDir + "/results/";
	public final static Long seed = 0L;
	public final static Long randomSeed = new Random().nextLong();
	public final static boolean RAND_PARTICLE_FIX_SIZE = true;
}