package ar.edu.itba.ss;

import java.io.IOException;

public class App {
    public static void main( String[] args ) throws IOException {
        //MyRandom.setSeed(Configuration.seed);

        Long seed = Configuration.getInstance().getSeed();

        MyRandom.setSeed(seed);

        CimReader cimReader = new CimReader();

        /*
        if (cimReader.getM() > Algorithm.optimumDim(cimReader.getR(),cimReader.getRc(),cimReader.getL(),cimReader.getN())) {
        	throw new IllegalStateException();
        }
        */

        Algorithm algorithm = new Algorithm(
                cimReader.getL(),cimReader.getM(),cimReader.getRc(),cimReader.getparticles(),cimReader.getP());

        System.out.println("Corriendo simulacion...");
        System.out.println("L " + cimReader.getL() + " (Longitud del lado del área de simulación)");
        System.out.println("M " + cimReader.getM() + " (Longitud del lado de la celda)");
        System.out.println("Rc " + cimReader.getRc() + " (Distancia entre particulas)");
        System.out.println("P " + (cimReader.getP() ? " Activado" : "Desactivado") + "(Condiciones de contorno periodicas del tablero)");
        System.out.println("Particulas (Un total de " + cimReader.getparticles().size() + " particulas que interactuan)");

        long start = System.currentTimeMillis();

        algorithm.run();
        
        System.out.println("Resultado");

        // System.out.println(algorithm.getOutput());
        /*
        Map<Particle, List<Particle>> ans = algorithm.getNeighborParticles();
        for (Particle particle: ans.keySet()){
            String particleData = algorithm.generateParticleData(particle, ans.get(particle), cimReader.getparticles());
            Algorithm.generateFile(particleData, particle.getId(),Configuration.outputDir);
        }
         */


        long end = System.currentTimeMillis();

        System.out.println("Tiempo de duracion de la prueba: " + (end-start));
        System.out.println(Configuration.outputDir);

    }
}