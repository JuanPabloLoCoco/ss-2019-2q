package ar.edu.itba.ss;

import java.util.ArrayList;
import java.util.List;

public class Cell {
	/** To improve efficiency only half of neighbors are visited (prevents having repeated neighbor checks) */
    private List<Cell> neighborCells = new ArrayList<Cell>();
    /** Particles currently placed on the cell */
    private List<Particle> elements = new ArrayList<Particle>();
    
    public List<Cell> getNeighborCells() {
    	return neighborCells;
    }
    
    public List<Particle> getElements() {
    	return elements;
    }
    
    public void addElement(Particle particle) {
    	elements.add(particle);
    }
    
    /**
     * @param x x coordinate of Cell
     * @param y y coordinate of Cell
     * @param m board dimension
     * @param board Board
     */
    public void initNeighborsPeriodic(int y, int x, int m, Cell[][] board) {
    	int neighborY, neighborX;
        /** Upper Top Position - One position up (y - 1) */
    	neighborY = lowPeriodicNeighborPos(y,m);
        neighborX = x;
    	neighborCells.add(board[neighborY][neighborX]);
        /** Upper Right Position - One position right (x + 1) */
    	neighborX = upPeriodicNeighborPos(x,m);
    	neighborCells.add(board[neighborY][neighborX]);
        /** Right Position - One position down (y + 1 = original y) */
    	neighborY = y;
    	neighborCells.add(board[neighborY][neighborX]);
    	/** Bottom Right Position - One position down (y + 1) */
    	neighborY = upPeriodicNeighborPos(y,m);
    	neighborCells.add(board[neighborY][neighborX]);
    }
    
    /**
     * @param x x coordinate of Cell
     * @param y y coordinate of Cell
     * @param m board dimension
     * @param board Board
     */
    public void initNeighborsNonPeriodic(int y, int x, int m, Cell[][] board) {
    	int neighborY, neighborX;
        /** Upper Positions (y - 1) */
    	if ((neighborY = y - 1) >= 0) {
            /** Upper Top Position */
            neighborX = x;
        	neighborCells.add(board[neighborY][neighborX]);
            /** Upper Right Position - One position right (x + 1) */
        	if ((neighborX = x + 1) < m) {
            	neighborCells.add(board[neighborY][neighborX]);
        	}
    	}
        /** Right Positions (x + 1) */
    	if ((neighborX = x + 1) < m) {
            /** Right Mid Position */
            neighborY = y;
        	neighborCells.add(board[neighborY][neighborX]);
            /** Right Bottom Position - One position down (y + 1) */
        	if ((neighborY = y + 1) < m) {
            	neighborCells.add(board[neighborY][neighborX]);
        	}
    	}
    }
    
    /**
     * @param pos position (x or y)
     * @param m board dimension
     * @return fixed position based on periodic rules
     */
    private int upPeriodicNeighborPos(int pos, int m) {
    	pos = pos + 1;
    	if (pos > m - 1) {
    		pos = 0;
    	}
    	return pos;
    }
    
    /**
     * @param pos position (x or y)
     * @param m board dimension
     * @return fixed position based on periodic rules
     */
    private int lowPeriodicNeighborPos(int pos, int m) {
    	pos = pos - 1;
    	if (pos < 0) {
    		pos = m - 1;
    	}
    	return pos;
    }
    
    public void reset() {
        elements.clear();
    }
}