package ar.edu.itba.ss;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

class Configuration {
	private final static String projectDir = System.getProperty("user.dir");
	final static String staticInfoDir = projectDir + "/resources/staticInfo.txt";
	final static String dynamicInfoDir = projectDir + "/resources/dynamicInfo.txt";
	final static String outputDir = projectDir + "/results/";
	final static String analyticsFileName = "analyticsFile.csv";
	final static boolean RAND_PARTICLE_FIX_SIZE = true;

	private Properties properties;
	private InputStream inStream = null;

	private static Configuration instance;

	/** Seed */
	private long seed;
	/** Eta */
	private double eta;

	private double va_error;

	private int iteration_limit;

	private Configuration(){
		properties = new Properties();
		try {
			String configInfoDir = projectDir + "/resources/config.properties";
			inStream = new FileInputStream(configInfoDir);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		loadConfiguration();
	}

	static Configuration getInstance(){
		if (instance == null){
			instance = new Configuration();
		}
		return instance;
	}

	private void loadConfiguration() {

		try {
			properties.load(inStream);
		} catch (IOException e){
			e.printStackTrace();
		}

		seed = Long.parseLong(properties.getProperty("seed"));

		eta = Double.parseDouble(properties.getProperty("eta"));

		va_error = Double.parseDouble(properties.getProperty("va_error"));

		iteration_limit = Integer.parseInt(properties.getProperty("iteration_limit"));
	}

	long getSeed() {
		return seed;
	}

	double getEta() {
		return eta;
	}

	double getVa_error() {
		return va_error;
	}

	public int getIteration_limit() {
		return iteration_limit;
	}
}