package ar.edu.itba.ss;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Algorithm {
	private final static double TIME_INTERVAL = 1;
	private Board board;
	private Map<Particle,List<Particle>> neighborParticles;
	private double rc;
	private double l;
    private List<Particle> particles;
    private double eta;

	public Algorithm(double r, double l, double rc, List<Particle> particles, boolean periodic) {
		this(l, optimumDim(r,rc,l,particles.size()), rc, particles, periodic);
	}

	public Algorithm(double l, int m, double rc, List<Particle> particles, boolean periodic) {
		board = new Board(m,particles,l,periodic);
		this.rc = rc;
		this.l = l;
		this.particles = particles;
		this.eta = Configuration.getInstance().getEta();

		neighborParticles = new HashMap<Particle,List<Particle>>();
		for (Particle particle : particles) {
			neighborParticles.put(particle,new ArrayList<Particle>());
		}
	}

	public Map<Particle, List<Particle>> getNeighborParticles() {
		return neighborParticles;
	}

	public static int optimumDim(double r, double rc, double l, int n) {
		return (int) (l/(rc+2*r));
	}
	
	public double run() {
		double va_error = Configuration.getInstance().getVa_error();

		double va_current;
		double va_next = getVa();
		int time = 0;
		int iteration_limit = Configuration.getInstance().getIteration_limit();

		do {
			System.out.println("va : " + va_next);
			// Mandar a archivo las posiciones de la particula en el tiempo n.
			generateTimeFile(generateTimeData(particles, time), Configuration.outputDir, time);
			generateVaFile(time, va_next);
			va_current = va_next;
			cellIndexMethod();
			recalculateParticles();
			va_next = getVa();
			time++;
			//} while (Math.abs(va_current - va_next) > va_error);
		} while (time <= iteration_limit);
			// Mandar a archivo va, N y eta.
		appendSimulationData(va_next);
		return va_next;
	}
	
	public Map<Particle,List<Particle>> cellIndexMethod() {
		resetNeighborParticles();
		
		for (Cell[] row : board.getBoard()) {
			for (Cell cell : row) {
				for (Particle particle : cell.getElements()) {
					for (Cell neighborCell : cell.getNeighborCells()) {
						for (Particle neighborParticle : neighborCell.getElements()) {
							checkDistance(particle,neighborParticle);
						}
					}
					for (Particle neighborParticle : cell.getElements()){
						if (!particle.equals(neighborParticle)){
							checkDistance(particle, neighborParticle);
						}
					}
				}
			}
		}
		
		return neighborParticles;
	}
	
	private void checkDistance(Particle particle1, Particle particle2) {
		if (particle1.getDistance(particle2) <= rc) {
			neighborParticles.get(particle1).add(particle2);
			neighborParticles.get(particle2).add(particle1);
		}
	}
	
	public Map<Particle,List<Particle>> bruteForce() {
		resetNeighborParticles();
		
		int i = 0;
		
		for (Particle particle : particles) {
			for (Particle other : particles.subList(i + 1, particles.size())) {
				checkDistance(particle,other);
			}
			i++;
		}
		
		return neighborParticles;
	}
	
	private void resetNeighborParticles() {
		for (Map.Entry<Particle,List<Particle>> entry : neighborParticles.entrySet()) {
			entry.getValue().clear();
		}
	}
	
	public double getVa() {
		double va, vxSum = 0, vySum = 0;
		
		for (Particle particle : particles) {
			vxSum += particle.getVelX();
			vySum += particle.getVelY();
		}
		va = Math.hypot(vxSum,vySum);
		va = va/(particles.size() * Particle.velocity);
		return va;
	}
	
	public void recalculateParticles() {
		Map<Particle,Double> newAngles = new HashMap<Particle,Double>();
		
		for (Map.Entry<Particle,List<Particle>> particularNeighbors : neighborParticles.entrySet()) {
			double newAngle = getAvgAngle(particularNeighbors.getKey(),particularNeighbors.getValue());
			newAngle += getDeltaAngle();
			newAngles.put(particularNeighbors.getKey(),newAngle);
		}
		
		for (Particle particle : particles) {
			particle.advance(newAngles.get(particle),TIME_INTERVAL,l);
		}
	}
	
	public double getAvgAngle(Particle particle, List<Particle> neighbors) {
		double angle = particle.getAngle(), totalSin = Math.sin(angle), totalCos = Math.cos(angle);
		
		for (Particle neighbor : neighbors) {
			angle = neighbor.getAngle();
			totalSin += Math.sin(angle);
			totalCos += Math.cos(angle);
		}
		
		return Math.atan2(totalSin,totalCos);
	}
	
	private double getDeltaAngle() {
		double lim = eta/2;
		return MyRandom.getInstance().nextDouble(-lim,lim);
	}

	public String getOutput() {
		StringBuilder stringBuilder = new StringBuilder();
		
		for (Map.Entry<Particle,List<Particle>> entry : neighborParticles.entrySet()) {
			stringBuilder.append(entry.getKey());
			List<Particle> neighbors = entry.getValue();
			
			if (!neighbors.isEmpty()) {
				stringBuilder.append(", (");
				stringBuilder.append("Id: " + neighbors.get(0).getId());
				
				for (Particle neighbor : neighbors.subList(1,neighbors.size())) {
					stringBuilder.append(", Id: " + neighbor.getId());
				}
				stringBuilder.append(")");
			}
			stringBuilder.append('\n');
		}
		stringBuilder.setLength(stringBuilder.length()-1); // Remove last \n
		
		return stringBuilder.toString();
	}

	public static void generateFile(String particleData, int index, String path){
		try {
			Files.write(Paths.get(path + "/results" + index + ".txt"), particleData.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void generateTimeFile (String timeData, String path, int time){
		try {
			Files.write(Paths.get(path + "/time_" + time + ".txt"),timeData.getBytes());
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	public String generateParticleData(Particle particle, List<Particle> neighbours, List<Particle> AllParticles){
		StringBuilder builder = new StringBuilder()
				.append(AllParticles.size())
				.append("\r\n")
				.append("//ID\t X\t Y\t Radius\t R\t G\t B\t\r\n");

		for (Particle current: AllParticles){
			builder.append(current.getId())
					.append(" ")
					.append(current.getX())
					.append(" ")
					.append(current.getY())
					.append(" ")
					.append(current.getRadius())
					.append(" ");
			if (current.equals(particle)){
				builder.append("1 0 0\r\n");
			} else if (neighbours.contains(current)){
				builder.append("0 1 0\r\n");
			} else {
				builder.append("1 1 1\r\n");
			}
		}
		return builder.toString();
	}

	private String generateTimeData(List<Particle> particleList, int time){
		StringBuilder builder = new StringBuilder()
				.append(particleList.size())
				.append("\r\n")
				.append("//ID\t X\t Y\t Vx\t Vy\t Radius\t R\t G\t B\t\r\n");
				//.append("//ID\t X\t Y\t Radius\t R\t G\t B\t\r\n");
		for (Particle current: particleList){
			builder.append(current.getId())
					.append(" ")
					.append(current.getX())
					.append(" ")
					.append(current.getY())
					.append(" ")
					.append(current.getVelX())
					.append(" ")
					.append(current.getVelY())
					.append(" ")
					.append(current.getRadius())
					.append(" ")
					.append(current.getColorByAngle())
					.append("\r\n");
			// Ovito mapea R G B entre [0, 1]
		}
		return builder.toString();
	}

	private void appendSimulationData(double va){
		StringBuilder builder = new StringBuilder()
			.append(particles.size() / (l * l))
			.append(",")
			.append(l)
			.append(",")
			.append(particles.size())
			.append(",")
			.append(Configuration.getInstance().getEta())
			.append(",")
			.append(va)
			.append(",")
			.append(Configuration.getInstance().getVa_error())
			.append("\r\n");
		//.append("//N \t Eta\t va\t\r\n")
		String filePath =  Configuration.outputDir + "/analytics/" + Configuration.analyticsFileName;
		Path path = Paths.get(filePath);

		try {
			if (!Files.exists(path)){
				Files.write(path,getAnalyticsHeader().getBytes(),StandardOpenOption.CREATE);
			}
			Files.write(path,builder.toString().getBytes(),StandardOpenOption.APPEND);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String getAnalyticsHeader(){
		return "rho,l,n,eta,va,va_error\r\n";
	}

	private void generateVaFile(int time, double va){
		StringBuilder builder = new StringBuilder()
				.append(time)
				.append(",")
				.append(va)
				.append("\r\n");

		StringBuilder fileNameBuilder = new StringBuilder()
				.append("s_")
				.append(Configuration.getInstance().getSeed())
				.append("n_")
				.append(particles.size())
				.append("l_")
				.append(l)
				.append("eta_")
				.append(Configuration.getInstance().getEta())
				.append(".csv");

		String filePath =  Configuration.outputDir + "/analytics/" + fileNameBuilder.toString();
		Path path = Paths.get(filePath);

		String vaHeader = "time,va\r\n";

		try {
			if (!Files.exists(path)){
				Files.write(path,vaHeader.getBytes(),StandardOpenOption.CREATE);
			}
			Files.write(path,builder.toString().getBytes(),StandardOpenOption.APPEND);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}