package ar.edu.itba.ss;

import java.util.ArrayList;
import java.util.List;

public class ParticleFactory {
    private static ParticleFactory instance;

    public static ParticleFactory getInstance(){
        if (instance == null){
            instance = new ParticleFactory();
        }
        return instance;
    }

    public static Particle generateRandom(double l, double r, boolean periodic) {
        double xPos, yPos;
        if (periodic) {
            xPos = MyRandom.getInstance().nextDouble(l);
            yPos = MyRandom.getInstance().nextDouble(l);
        } else { // Add margins so particle doesn't get out of bounds
            xPos = MyRandom.getInstance().nextDouble(r, l - r);
            yPos = MyRandom.getInstance().nextDouble(r, l - r);
        }
        xPos = Math.round(xPos*100)/(double)100;
        yPos = Math.round(yPos*100)/(double)100;

        Particle particle = new Particle(r);
        particle.setProperties(xPos,yPos,0,0);
        return particle;
    }

    public static Particle generateRandom(double l, boolean periodic) {
        double r = 0;
        return generateRandom(l,r,periodic);
    }

    public static Particle generateRandom(double l){
        return generateRandom(l, 0, true);
    }

    public static List<Particle> generateParticleList(int N, double l){
        List<Particle> particles = new ArrayList<>();
        for (int i = 0; i < N; i++){
            particles.add(generateRandom(l));
        }
        return particles;
    }
}
