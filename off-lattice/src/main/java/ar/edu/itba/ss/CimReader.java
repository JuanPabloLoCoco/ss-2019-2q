package ar.edu.itba.ss;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class CimReader extends InfoReader {
	private final String PERIODIC_TEXT = "PERIODIC";
	/** Periodic condition - True if on */
	private boolean P;
	/** Number of particles */
	private int N;
	/** Length of board */
	private double L;
	/** Dimension of board */
	private int M;
	/** Interaction Radius */
	private double Rc;
	/** Default Particle Radius */
	private double R;
	private List<Particle> particles;
	
	public CimReader() throws IOException {
		super();
	}
	
	public boolean getP() {
		return P;
	}
	
	public int getN() {
		return N;
	}
	
	public double getL() {
		return L;
	}
	
	public int getM() {
		return M;
	}
	
	public double getRc() {
		return Rc;
	}
	
	public double getR() {
		return R;
	}
	
	public List<Particle> getparticles() {
		return particles;
	}
	
	@Override
	public void generateData() throws IOException {
		P = staticInfo.readLine().equals(PERIODIC_TEXT);
		N = Integer.parseInt(staticInfo.readLine());
		L = Double.parseDouble(staticInfo.readLine());
		M = Integer.parseInt(staticInfo.readLine());
		Rc = Double.parseDouble(staticInfo.readLine());

		Particle.setDefaultRadius(R = Double.parseDouble(staticInfo.readLine()));
		Particle.setCimReader(this);
		Particle.setPeriodic(P);

		particles = new ArrayList<Particle>();
		
		dynamicInfo.readLine(); // Extract time line
		
		String staticLine = staticInfo.readLine(), dynamicLine = dynamicInfo.readLine();
		int i = 0;
		while (i < N && staticLine != null && dynamicLine != null) {

			Particle newParticle = new Particle(staticLine.split(" ")).setProperties(dynamicLine.split(" "));
			
			if (Particle.isValidParticle(newParticle,particles)) {
				newParticle.setId();
			    particles.add(newParticle);
				i++;
			}
			
			staticLine = staticInfo.readLine();
			dynamicLine = dynamicInfo.readLine();
		}

		while (i < N) {
			Particle newParticle = Particle.generateRandom(L,P);
			
			while (!Particle.isValidParticle(newParticle,particles)) {
				newParticle = Particle.generateRandom(L,P);
			}
			newParticle.setId();
			System.out.println(newParticle.toString());
			try {
			    Files.write(Paths.get(Configuration.staticInfoDir), (newParticle.getStaticRepresentation() + "\n").getBytes(), StandardOpenOption.APPEND);
			    Files.write(Paths.get(Configuration.dynamicInfoDir), (newParticle.getDynamicRepresentation() + "\n").getBytes(), StandardOpenOption.APPEND);
			}catch (IOException e) {}
			
			particles.add(newParticle);
			i++;
		}
	}

    @Override
    public String toString() {
        return "CimReader{" +
                "P=" + P +
                ", N=" + N +
                ", L=" + L +
                ", M=" + M +
                ", Rc=" + Rc +
                ", R=" + R +
                '}';
    }
}