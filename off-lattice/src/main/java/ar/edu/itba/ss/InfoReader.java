package ar.edu.itba.ss;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public abstract class InfoReader {
	protected BufferedReader staticInfo;
	protected BufferedReader dynamicInfo;
	
	public InfoReader() throws IOException {
		try {
			staticInfo = new BufferedReader(new FileReader(Configuration.staticInfoDir));
			dynamicInfo = new BufferedReader(new FileReader(Configuration.dynamicInfoDir));
			generateData();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} finally {
			staticInfo.close();
			dynamicInfo.close();
		}
	}
	
	public abstract void generateData() throws IOException;
}