package ar.edu.itba.ss;

import java.util.Random;

public class MyRandom extends Random {
    private static Long class_seed = 0L;
    private static MyRandom instance;

    public static MyRandom getInstance() {
        if (instance == null){
            instance = new MyRandom();
        }
        return instance;
    }
    private MyRandom() {
        super(class_seed);
    }
    public static void setSeed(Long seed) {
        class_seed = seed;
    }
    
    public double nextDouble(double bound) {
    	return bound * nextDouble();
    }
    
    public double nextDouble(double minBound, double maxBound) {
    	return minBound + (maxBound - minBound) * nextDouble();
    }
}