 A = importdata('.\results\analytics\analyticsFile.csv',',',1)
 disp(A.textdata);
 Datos = A.data
 
 for rho = 1:5
  linea(rho,1) = Datos((rho - 1) * 5 + 1,1);
  for seed = 1:5
   % Anoto el eta
    puntos(seed,1) = Datos((rho - 1) * 5 + seed, 5); 
  endfor
    linea(rho,2) = mean(puntos);
    linea(rho,3) = std(puntos);
 endfor
plot(linea(:,1),linea(:,2),'o','Color','b','LineWidth',2);
hold on
errorbar(linea(:,1), linea(:,2),linea(:,3));
title('Va en funcion de \rho', 'FontSize',20);
%legend('N = 40','Location', 'NorthEast');
xlabel('\rho','FontSize',20);
ylabel('Va','FontSize',20);
hold off
 %errorbar(Datos(:,4), Datos(:,5),Datos(:,6));
 %plot(Datos(:,4),Datos(:,5),'Color','r','LineWidth',2)
 %title('Va en funcion de eta', 'FontSize',20);
 %legend(A.colheaders(3),'Location', 'NorthEast');
 %xlabel('\eta','FontSize',20);
 %ylabel(A.colheaders(4),'FontSize',20);
 
 