A = importdata('.\results\analytics\s_1234n_400l_10.0eta_1.0.csv',',',1)
seed1 = A.data;

B = importdata('.\results\analytics\s_1235n_400l_10.0eta_1.0.csv',',',1)
seed2 = B.data;

C = importdata('.\results\analytics\s_1236n_400l_10.0eta_1.0.csv',',',1)
seed3 = C.data;

D = importdata('.\results\analytics\s_1237n_400l_10.0eta_1.0.csv',',',1)
seed4 = D.data;

E = importdata('.\results\analytics\s_1238n_400l_10.0eta_1.0.csv',',',1)
seed5 = E.data;


plot(seed1(:,1),seed1(:,2),'Color','r','LineWidth',2);
hold on
plot(seed2(:,1),seed2(:,2),'Color','g','LineWidth',2);
plot(seed3(:,1),seed3(:,2),'Color','b','LineWidth',2);
title('Va en funcion de tiempo', 'FontSize',20);
xlabel('t','FontSize',20);
ylabel('Va','FontSize',20);
hold off


