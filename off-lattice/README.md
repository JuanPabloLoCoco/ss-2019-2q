# ss-2019-2q

resources/staticInfo.txt format:

Periodic state ("PERIODIC" if periodic. Something else if not).
N:  Number of particles
L:  Length of board
M:  Dimension of board
Rc: Default Particle Radius
R:  Interaction Radius
... (Particles Info (Radius <= Default and Properties) from 1 to N)

resources/config.properties
seed: seed
iteration_limit: count of iterations
eta: eta limit

# Compilacion
```bash
$ mvn package
$ cd target
$ chmod +x off-latice-1.0.jar
```
copie el jar al directorio off-lattice

```bash
$ java -jar off-latice-1.0.jar
```
Para arrancar a correr el programa es necesario que el archivo dinamico solo contenga "t0" y 
el archivo estatico solo los parametros definidos